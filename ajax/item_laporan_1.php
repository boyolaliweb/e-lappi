<?php
include ("../core/db.config.php");
$tahun = $_POST['tahun'];
//$server = "http://".$_SERVER['SERVER_NAME']."/";
$server = "http://localhost/elaporan_rev2.2/";
?>

<?php
$id = "12"; //bulan apbd
$tahun_apbd = "2016"; //tahun apbd
?>
<link href="<?php echo $server; ?>media/css/boot.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $server; ?>media/css/steel.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 10]>      
    <link href="media/css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->           
<!--[if lte IE 7]>
    <script type='text/javascript' src='media/js/plugins/other/lte-ie7.js'></script>
<![endif]-->    
<script type='text/javascript' src='<?php echo $server; ?>media/js/jquery.js'></script>

<script src="<?php echo $server; ?>media/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>media/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>media/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo $server; ?>media/amcharts/serial.js" type="text/javascript"></script>
<table cellpadding="0" border="1" cellspacing="0" width="100%" style="display:none">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">SATUAN KERJA</th>
            <th rowspan="2">JML KEG</th>
            <th rowspan="2">JUMLAH ANGGARAN</th>
            <th colspan="4">REALISASI PENYERAPAN DANA</th>
            <th colspan="2">PROGRES FISIK</th>
            <th rowspan="2">KET.</th>
        </tr>
        <tr>
            <th>PANJAR/SP2D</th>
            <th>%</th>
            <th>SPJ</th>
            <th>%</th>
            <th>TARGET</th>
            <th>REALISASI</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        $sql = mysqli_query($db, "select distinct(id_skpd) from t_apbd where bulan='" . $id . "' and tahun='" . $tahun_apbd . "'  and status_update='0'");

        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
//make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_apbd = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_apbd = 0;
        $sum_panjar_persen = 0;
        $sum_panjar_apbd_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_apbd = 0;
        $sum_realisasi_persen = 0;
        $sum_realisasi_apbd_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;
        $no = 0;
        $no_target = 0;
        $no_real = 0;
        $no_panjar = 0;
        $no_realisasi = 0;

        echo '<tr>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>';

        while ($rowProgram = mysqli_fetch_array($sql)) {
            $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $rowProgram['id_skpd'] . "' limit 1"));
            $sum_kegiatan = mysqli_num_rows(mysqli_query($db, "select id from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));
            $anggaran_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_apbd) anggaran from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $panjar_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_apbd) panjar from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $realisasi_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_apbd) anggaran from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $target_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));
            $real_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) target from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            //target
            $persen_target = ($target_apbd['target'] != 0) ? $target_apbd['target'] / $sum_kegiatan : 0;
            $persen_real = ($real_apbd['target'] != 0) ? $real_apbd['target'] / $sum_kegiatan : 0;
            echo '
			<tr>
				<td align="center"><b>' . ($noBid++) . '</b></td>
				<td><b>' . $nama_skpd['nama'] . '</b></td>
				<td align="right">' . number_format($sum_kegiatan, 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_apbd['anggaran'], 0, ",", ".") . '</td>
			
				<td align="right">' . number_format($panjar_apbd['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format(($panjar_apbd['panjar'] != 0) ? $panjar_apbd['panjar'] * 100 / $anggaran_apbd['anggaran'] : 0, 2, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_apbd['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format(($realisasi_apbd['anggaran'] != 0) ? $realisasi_apbd['anggaran'] * 100 / $anggaran_apbd['anggaran'] : 0, 2, ",", ".") . '</td>
				<td align="right">' . number_format($persen_target, 2, ",", ".") . '</td>
				<td align="right">' . number_format($persen_real, 2, ",", ".") . '</td>
				<td>&nbsp;</td>
			</tr>
			';
            //$sqlKegiatan = mysqli_query($db,"select * from t_apbd where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "'");

            $sqlSKPD = mysqli_query($db, "select * from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun_apbd . "' and status_update='0'");
            //if ($target_apbd['target'] != 0) {
            $no_target = $no_target + 1;
            //}
            //if ($real_apbd['target'] != 0) {
            $no_real = $no_real + 1;
            //}
            //if ($panjar_apbd['panjar'] != 0) {
            $no_panjar = $no_panjar + 1;
            //}
            //if ($realisasi_apbd['anggaran'] != 0) {
            $no_realisasi = $no_realisasi + 1;
            //}
            $baris = $baris + $sum_kegiatan;
            $sum_anggaran_apbd = $sum_anggaran_apbd + $anggaran_apbd['anggaran'];

            $sum_panjar_apbd = $sum_panjar_apbd + $panjar_apbd['panjar'];
            $sum_panjar_persen = ($sum_panjar_persen + (($panjar_apbd['panjar'] != 0) ? $panjar_apbd['panjar'] * 100 / $anggaran_apbd['anggaran'] : 0));
            $sum_realisasi_apbd = $sum_realisasi_apbd + $realisasi_apbd['anggaran'];
            $sum_realisasi_persen = ($sum_realisasi_persen + (($realisasi_apbd['anggaran'] != 0) ? $realisasi_apbd['anggaran'] * 100 / $anggaran_apbd['anggaran'] : 0));
            $sum_persen_target = $sum_persen_target + $persen_target;
            $sum_persen_real = $sum_persen_real + $persen_real;
        }
        echo '<tr>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			</tr>';
        echo '
            <tr>
                    <td>&nbsp;</td>
                    <td align="center"><b>JUMLAH</b></td>
                    <td align="right"><b>' . number_format($baris, 0, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format($sum_anggaran_apbd, 0, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format($sum_panjar_apbd, 0, ",", ".") . '</b></td>			
                    <td align="right"><b>' . number_format(($sum_panjar_persen != 0) ? $sum_panjar_apbd / $sum_anggaran_apbd * 100 : 0, 2, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format($sum_realisasi_apbd, 0, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format(($sum_realisasi_persen != 0) ? $sum_realisasi_apbd / $sum_anggaran_apbd * 100 : 0, 2, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format(($sum_persen_target != 0) ? $sum_persen_target / $no_target : 0, 2, ",", ".") . '</b></td>
                    <td align="right"><b>' . number_format(($sum_persen_real != 0) ? $sum_persen_real / $no_real : 0, 2, ",", ".") . '</b></td>
                    <td align="right">&nbsp;</td>

            </tr>
            ';
        ?>
    </tbody>
</table>
<!-- Styles -->
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }										
</style>

<!-- Chart code -->
<script>
    AmCharts.makeChart("chartdiv", {
        "theme": "light",
        "type": "serial",
        "dataProvider": [
            {
                "dana": "DAK",
                "anggaran": <?php echo $jumlah_dak; ?>,
                "spj": <?php echo $jumlah_realisasi_dak; ?>
            }
        ],
        "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Jumlah",
            }],
        "startDuration": 1,
        "graphs": [
            {
                "balloonText": "Realisasi Anggaran [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "spj",
                "type": "column",
                "valueField": "spj"
            },
            {
                "balloonText": "Anggaran [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "anggaran",
                "type": "column",
                "valueField": "anggaran"
            }
        ],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 60,
        "angle": 30,
        "categoryField": "dana",
        "categoryAxis": {
            "gridPosition": "start"
        },
        "export": {
            "enabled": true
        }
    });

</script>

<div style="text-align:center;">
    <h5>REKAPITULASI DANA APBD KABUPATEN<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?></h5>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">

            <div>
                <p>Periode </p>
                <table>
                    <tr>
                        <td>
                            Tahun&nbsp;&nbsp;
                        <td>
                            <select name="tahun" id="tahunx" class="form-control">
                                <?php
                                $sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
                                $sqlTahunx->data_seek(0);
                                echo '<option value="0">-- Pilih Tahun --</option>';
                                while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
                                    //$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
                                    echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Bulan&nbsp;&nbsp;
                        </td>
                        <td>
                            <select name="bulan" id="bulan" class="form-control">>
                            </select>
                        </td>
                        <td>
                            <span id="loading_image" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading <img src="media/img/loader.gif"/></span>
                        </td>
                    </tr>
                </table>                                                  
            </div> 		
            <hr>
            <div>&nbsp;</div>

        </div>
    </div>
</div>
<!-- HTML -->
<div id="chartdiv"></div>     

<div class="well">
    <?php
    $id_apbn = 12;
    if (isset($_GET['years']) && $_GET['years'] != "") {
        $tahun = $_GET['years']; //tahun by variabel years di URL
    } else {
        $tahun = date("Y"); //tahun grafik aktif    
    }
    ?>
    <div style="text-align:center;">
        <h5>REKAPITULASI DANA APBN DAN APBD PROVINSI<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?></h5>
    </div>


    <?php
//hitung jumlah kegiatan
    $q_hitung_dak = mysqli_query($db, "select id from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_dak = $q_hitung_dak->num_rows;

    $q_hitung_dbhct = mysqli_query($db, "select id from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_dbhcht = $q_hitung_dbhct->num_rows;

    $q_hitung_bankeu_kab = mysqli_query($db, "select id from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_bankeu_kab = $q_hitung_bankeu_kab->num_rows;

    $q_hitung_bankeu_desa = mysqli_query($db, "select id from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_bankeu_desa = $q_hitung_bankeu_desa->num_rows;

    $q_hitung_dekon = mysqli_query($db, "select id from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_dekon = $q_hitung_dekon->num_rows;

    $q_hitung_tp = mysqli_query($db, "select id from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_tp = $q_hitung_tp->num_rows;

    $q_hitung_dub = mysqli_query($db, "select id from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
    $hitung_dub = $q_hitung_bankeu_desa->num_rows;

    $perda_1 = $hitung_dak + $hitung_dbhcht;
    $perda_2 = $hitung_bankeu_kab;

    $non_1 = $hitung_dekon + $hitung_tp + $hitung_dub;
    $non_2 = $hitung_bankeu_desa;

    $sum_perda = $perda_1 + $perda_2;
    $sum_non = $non_1 + $non_2;
    $no_target = 0;
    $no_real = 0;
//kolom jml kegiatan beres
//anggaran
    $anggaran_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dak) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dbhcht) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dekon) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_tp) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dub) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $anggaran_perda_apbn = $anggaran_dak['total'] + $anggaran_dbhcht['total'];
    $anggaran_perda_apbd = $anggaran_bankeu_kab['total'];

    $anggaran_non_apbn = $anggaran_dekon['total'] + $anggaran_tp['total'] + $anggaran_dub['total'];
    $anggaran_non_apbd = $anggaran_bankeu_desa['total'];

    $sum_anggaran_perda = $anggaran_perda_apbn + $anggaran_perda_apbd;
    $sum_anggaran_non = $anggaran_non_apbn + $anggaran_non_apbd;
//anggaran beres
//pendamping
    $pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $pendamping_perda_apbn = $pendamping_dak['total'] + $pendamping_dbhcht['total'];
    $pendamping_perda_apbd = $pendamping_bankeu_kab['total'];

    $pendamping_non_apbn = $pendamping_dekon['total'] + $pendamping_tp['total'] + $pendamping_dub['total'];
    $pendamping_non_apbd = $pendamping_bankeu_desa['total'];

    $sum_pendamping_perda = $pendamping_perda_apbn + $pendamping_perda_apbd;
    $sum_pendamping_non = $pendamping_non_apbn + $pendamping_non_apbd;
//pendamping beres
//JUMLAH
    $jumlah_dak = $anggaran_dak['total'] + $pendamping_dak['total'];
    $jumlah_dbhcht = $anggaran_dbhcht['total'] + $pendamping_dbhcht['total'];
    $jumlah_bankeu_kab = $anggaran_bankeu_kab['total'] + $pendamping_bankeu_kab['total'];
    $jumlah_bankeu_desa = $anggaran_bankeu_desa['total'] + $pendamping_bankeu_desa['total'];
    $jumlah_dekon = $anggaran_dekon['total'] + $pendamping_dekon['total'];
    $jumlah_tp = $anggaran_tp['total'] + $pendamping_tp['total'];
    $jumlah_dub = $anggaran_dub['total'] + $pendamping_dub['total'];

    $jumlah_perda_apbn = $anggaran_perda_apbn + $pendamping_perda_apbn;
    $jumlah_perda_apbd = $anggaran_perda_apbd + $pendamping_perda_apbd;
    $jumlah_non_apbn = $anggaran_non_apbn + $pendamping_non_apbn;
    $jumlah_non_apbd = $anggaran_non_apbd + $pendamping_non_apbd;
    $jumlah_sum_perda = $sum_anggaran_perda + $sum_pendamping_perda;
    $jumlah_sum_non = $sum_anggaran_non + $sum_pendamping_non;
//JUMLAH beres
//realisasi spj
    $realisasi_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dak) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dbhcht) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dekon) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_tp) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dub) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_perda_apbn = $realisasi_dak['total'] + $realisasi_dbhcht['total'];
    $realisasi_perda_apbd = $realisasi_bankeu_kab['total'];

    $realisasi_non_apbn = $realisasi_dekon['total'] + $realisasi_tp['total'] + $realisasi_dub['total'];
    $realisasi_non_apbd = $realisasi_bankeu_desa['total'];

    $sum_realisasi_perda = $realisasi_perda_apbn + $realisasi_perda_apbd;
    $sum_realisasi_non = $realisasi_non_apbn + $realisasi_non_apbd;
//realisasi spj beres
//pendamping spj
    $realisasi_pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendampingg_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $realisasi_pendamping_perda_apbn = $realisasi_pendamping_dak['total'] + $realisasi_pendamping_dbhcht['total'];
    $realisasi_pendamping_perda_apbd = $realisasi_pendamping_bankeu_kab['total'];

    $realisasi_pendamping_non_apbn = $realisasi_pendamping_dekon['total'] + $realisasi_pendampingg_tp['total'] + $realisasi_pendamping_dub['total'];
    $realisasi_pendamping_non_apbd = $realisasi_pendamping_bankeu_desa['total'];

    $sum_realisasi_pendamping_perda = $realisasi_pendamping_perda_apbn + $realisasi_pendamping_perda_apbd;
    $sum_realisasi_pendamping_non = $realisasi_pendamping_non_apbn + $realisasi_pendamping_non_apbd;
//pendamping spj beres
//JUMLAH SPJ
    $jumlah_realisasi_dak = $realisasi_dak['total'] + $realisasi_pendamping_dak['total'];
    $jumlah_realisasi_dbhcht = $realisasi_dbhcht['total'] + $realisasi_pendamping_dbhcht['total'];
    $jumlah_realisasi_bankeu_kab = $realisasi_bankeu_kab['total'] + $realisasi_pendamping_bankeu_kab['total'];
    $jumlah_realisasi_bankeu_desa = $realisasi_bankeu_desa['total'] + $realisasi_pendamping_bankeu_desa['total'];
    $jumlah_realisasi_dekon = $realisasi_dekon['total'] + $realisasi_pendamping_dekon['total'];
    $jumlah_realisasi_tp = $realisasi_tp['total'] + $realisasi_pendampingg_tp['total'];
    $jumlah_realisasi_dub = $realisasi_dub['total'] + $realisasi_pendamping_dub['total'];

    $jumlah_realisasi_perda_apbn = $realisasi_perda_apbn + $realisasi_pendamping_perda_apbn;
    $jumlah_realisasi_perda_apbd = $realisasi_perda_apbd + $realisasi_pendamping_perda_apbd;
    $jumlah_realisasi_non_apbn = $realisasi_non_apbn + $realisasi_pendamping_non_apbn;
    $jumlah_realisasi_non_apbd = $realisasi_non_apbd + $realisasi_pendamping_non_apbd;
    $jumlah_realisasi_sum_perda = $sum_realisasi_perda + $sum_realisasi_pendamping_perda;
    $jumlah_realisasi_sum_non = $sum_realisasi_non + $sum_realisasi_pendamping_non;
//JUMLAH spj beres
//Persen SPJ
    $persen_dak = ($jumlah_realisasi_dak != 0) ? $jumlah_realisasi_dak * 100 / $jumlah_dak : 0;
    $persen_dbhcht = ($jumlah_realisasi_dbhcht != 0) ? $jumlah_realisasi_dbhcht * 100 / $jumlah_dbhcht : 0;
    $persen_bankeu_kab = ($jumlah_realisasi_bankeu_kab != 0) ? $jumlah_realisasi_bankeu_kab * 100 / $jumlah_bankeu_kab : 0;
    $persen_bankeu_desa = ($jumlah_realisasi_bankeu_desa != 0) ? $jumlah_realisasi_bankeu_desa * 100 / $jumlah_bankeu_desa : 0;
    $persen_dekon = ($jumlah_realisasi_dekon != 0) ? $jumlah_realisasi_dekon * 100 / $jumlah_dekon : 0;
    $persen_tp = ($jumlah_realisasi_tp != 0) ? $jumlah_realisasi_tp * 100 / $jumlah_tp : 0;
    $persen_dub = ($jumlah_realisasi_dub != 0) ? $jumlah_realisasi_dub * 100 / $jumlah_dub : 0;

    $persen_perda_apbn = ($jumlah_realisasi_perda_apbn != 0) ? $jumlah_realisasi_perda_apbn * 100 / $jumlah_perda_apbn : 0;
    $persen_perda_apbd = ($jumlah_realisasi_perda_apbd != 0) ? $jumlah_realisasi_perda_apbd * 100 / $jumlah_perda_apbd : 0;
    $persen_non_apbn = ($jumlah_realisasi_non_apbn != 0) ? $jumlah_realisasi_non_apbn * 100 / $jumlah_non_apbn : 0;
    $persen_non_apbd = ($jumlah_realisasi_non_apbd != 0) ? $jumlah_realisasi_non_apbd * 100 / $jumlah_non_apbd : 0;
    $persen_sum_perda = ($jumlah_realisasi_sum_perda != 0) ? $jumlah_realisasi_sum_perda * 100 / $jumlah_sum_perda : 0;
    $persen_sum_non = ($jumlah_realisasi_sum_non != 0) ? $jumlah_realisasi_sum_non * 100 / $jumlah_sum_non : 0;
//Persen spj beres
//target & realisasi

    $target_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_dak = ($target_dak['target'] != 0 && $hitung_dak != 0) ? $target_dak['target'] / $hitung_dak : 0;
    $target_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_dbhcht = ($target_dbhcht['target'] != 0 && $hitung_dbhcht != 0) ? $target_dbhcht['target'] / $hitung_dbhcht : 0;
    $target_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_bankeu_kab = ($target_bankeu_kab['target'] != 0 && $hitung_bankeu_kab != 0) ? $target_bankeu_kab['target'] / $hitung_bankeu_kab : 0;
    $target_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_bankeu_desa = ($target_bankeu_desa['target'] != 0 && $hitung_bankeu_desa != 0) ? $target_bankeu_desa['target'] / $hitung_bankeu_desa : 0;
    $target_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_dub = ($target_dub['target'] != 0 && $hitung_dub != 0) ? $target_dub['target'] / $hitung_dub : 0;
    $target_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_tp = ($target_tp['target'] != 0 && $hitung_tp != 0) ? $target_tp['target'] / $hitung_tp : 0;
    $target_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $target_dekon = ($target_dekon['target'] != 0 && $hitung_dekon != 0) ? $target_dekon['target'] / $hitung_dekon : 0;

//realisasi
    $real_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_dak = ($real_dak['reali'] != 0) ? $real_dak['reali'] / $hitung_dak : 0;
    $real_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_dbhcht = ($real_dbhcht['reali'] != 0) ? $real_dbhcht['reali'] / $hitung_dbhcht : 0;
    $real_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_bankeu_kab = ($real_bankeu_kab['reali'] != 0) ? $real_bankeu_kab['reali'] / $hitung_bankeu_kab : 0;
    $real_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_bankeu_desa = ($real_bankeu_desa['reali'] != 0) ? $real_bankeu_desa['reali'] / $hitung_bankeu_desa : 0;

    $real_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_dub = ($real_dub['reali'] != 0 && $hitung_dub != 0) ? $real_dub['reali'] / $hitung_dub : 0;
    $real_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_tp = ($real_tp['reali'] != 0 && $hitung_tp != 0) ? $real_tp['reali'] / $hitung_tp : 0;
    $real_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
    $real_dekon = ($real_dekon['reali'] != 0 && $hitung_dekon != 0) ? $real_dekon['reali'] / $hitung_dekon : 0;
    ?>


</div>

<div class="well">
    <select name="tahunan" id="tahunan" class="form-control" onchange="if (this.value)
                                                                window.location.href = this.value">
                <?php
                $sqlTahun = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
                $sqlTahun->data_seek(0);
                while ($rowTahun = $sqlTahun->fetch_assoc()) {
                    $select = ($rowTahun['tahun'] == $tahun) ? "selected" : "";
                    echo '<option value="?years=' . $rowTahun['tahun'] . '" ' . $select . '>Laporan Tahun Anggaran ' . $rowTahun['tahun'] . '</option>';
                }
                ?>
    </select>
</div>                                    

</div>
</div>

<!-- Styles -->
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }										
</style>

<!-- Chart code -->
<script>
    AmCharts.makeChart("chartdiv", {
        "theme": "light",
        "type": "serial",
        "dataProvider": [
            {
                "dana": "DAK",
                "anggaran": <?php echo $jumlah_dak; ?>,
                "spj": <?php echo $jumlah_realisasi_dak; ?>
            },
            {
                "dana": "DBHCHT",
                "anggaran": <?php echo $jumlah_dbhcht; ?>,
                "spj": <?php echo $jumlah_realisasi_dbhcht; ?>
            },
            {
                "dana": "Bankeu Kab",
                "anggaran": <?php echo $jumlah_bankeu_kab; ?>,
                "spj": <?php echo $jumlah_realisasi_bankeu_kab; ?>
            },
            {
                "dana": "Dekon",
                "anggaran": <?php echo $jumlah_dekon; ?>,
                "spj": <?php echo $jumlah_realisasi_dekon; ?>
            },
            {
                "dana": "TP",
                "anggaran": <?php echo $jumlah_tp; ?>,
                "spj": <?php echo $jumlah_realisasi_tp; ?>
            },
            {
                "dana": "DUB",
                "anggaran": <?php echo $jumlah_dub; ?>,
                "spj": <?php echo $jumlah_realisasi_dub; ?>
            },
            {
                "dana": "Bankeu Desa",
                "anggaran": <?php echo $jumlah_bankeu_desa; ?>,
                "spj": <?php echo $jumlah_realisasi_bankeu_desa; ?>
            }
        ],
        "valueAxes": [{
                "stackType": "3d",
                "unit": "",
                "position": "left",
                "title": "Jumlah",
            }],
        "startDuration": 1,
        "graphs": [
            {
                "balloonText": "Realisasi Anggaran [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "spj",
                "type": "column",
                "valueField": "spj"
            },
            {
                "balloonText": "Anggaran [[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "anggaran",
                "type": "column",
                "valueField": "anggaran"
            }
        ],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 60,
        "angle": 30,
        "categoryField": "dana",
        "categoryAxis": {
            "gridPosition": "start"
        },
        "export": {
            "enabled": true
        }
    });

</script>

<!-- HTML -->
<div id="chartdiv"></div> 
