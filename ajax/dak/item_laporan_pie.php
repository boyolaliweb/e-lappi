<?php
include ("../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_GET['bln'];
$tahun = $_GET['th'];
?>

        <?php
        //$sql = mysqli_query($db, "select distinct(bidang) from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $sql = $db->query("SELECT DISTINCT (a.bidang)
                            FROM t_dak a
                            INNER JOIN m_bidang b ON a.bidang = b.bidang where a.bulan='".$id."'
                            and a.tahun='".$tahun."' and status_update='0' order by b.kode_urusan asc,
                            b.kode_bidang asc");
        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_dak = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_dak = 0;
        $sum_panjar_dak_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_dak = 0;
        $sum_realisasi_dak_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;

        while ($rowBid = mysqli_fetch_array($sql)) {
         

            $sqlSKPD = mysqli_query($db, "select distinct(id_skpd) from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");

            $baris = 0;
            while ($row = mysqli_fetch_array($sqlSKPD)) {

                $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $row['id_skpd'] . "'"));
                $kegiatan_skpd = mysqli_num_rows(mysqli_query($db, "select kegiatan from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                //hitung anggaran Rp
                $anggaran_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_dak = $anggaran_dak['anggaran'];
                $anggaran_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_pendamping = $anggaran_pendamping['anggaran'];
                $anggaran_jumlah = $anggaran_dak + $anggaran_pendamping;
                $panjar_dak = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_dak = $panjar_dak['anggaran'];
                $panjar_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_pendamping = $panjar_pendamping['anggaran'];
                $realisasi_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_dak = $realisasi_dak['anggaran'];
                $realisasi_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and  bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_pendamping = $realisasi_pendamping['anggaran'];
                //realisasi dan target
                $target_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $real_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $persen_target = ($target_dak['target'] != 0) ? $target_dak['target'] / $kegiatan_skpd : 0;
                $persen_real = ($real_dak['reali'] != 0) ? $real_dak['reali'] / $kegiatan_skpd : 0;
               

                $baris = $baris + 1;


                $sum_kegiatan = $sum_kegiatan + $kegiatan_skpd;
                $sum_anggaran_dak = $sum_anggaran_dak + $anggaran_dak;
                $sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping;
                $sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
                $sum_panjar_dak = $sum_panjar_dak + $panjar_dak;
                $sum_panjar_dak_persen = $sum_panjar_dak_persen + (($panjar_dak != 0) ? $panjar_dak * 100 / $anggaran_dak : 0);
                $sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping;
                $sum_panjar_pendamping_persen = $sum_panjar_pendamping_persen + (($panjar_pendamping != 0) ? $panjar_pendamping * 100 / $anggaran_pendamping : 0);
                $sum_realisasi_dak = $sum_realisasi_dak + $realisasi_dak;
                $sum_realisasi_dak_persen = $sum_realisasi_dak_persen + (($realisasi_dak != 0) ? $realisasi_dak * 100 / $anggaran_dak : 0);
                $sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping;
                $sum_realisasi_pendamping_persen = $sum_realisasi_pendamping_persen + (($realisasi_pendamping != 0) ? $realisasi_pendamping * 100 / $anggaran_pendamping : 0);
                $sum_persen_target = $sum_persen_target + $persen_target;
                $sum_persen_real = $sum_persen_real + $persen_real;
            }
            $jumlah_baris = $jumlah_baris + $baris;
        }
      
        ?>
        
<?php 
$fisik = ($sum_persen_real != 0) ? $sum_persen_real / $jumlah_baris : 0;
$fisik = number_format($fisik,2);
$total = 100 - $fisik;
?>
[
{
    "country": "Belum Realisasi",
    "value": <?php echo $total;?>
}, {
    "country": "Realisasi Fisik",
    "value": <?php echo $fisik;?>
}
]
