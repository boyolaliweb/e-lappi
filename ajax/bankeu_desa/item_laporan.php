<?php
include ("../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_GET['bln'];
$tahun = $_GET['th'];
//$skpd = $_POST['skpd'];
//$id = 12;
//$tahun = 2016;
?>
        <?php
        $sql = mysqli_query($db, "select distinct(id_skpd) from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");

        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_bankeu_desa = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_bankeu_desa = 0;
        $sum_panjar_bankeu_desa_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_bankeu_desa = 0;
        $sum_panjar_jumlah = 0;
        $sum_realisasi_jumlah = 0;
        $sum_realisasi_bankeu_desa_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;
        $no = 0;
        $no_target = 0;
        $no_real = 0;

        while ($rowProgram = mysqli_fetch_array($sql)) {
            $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $rowProgram['id_skpd'] . "' limit 1"));
            $sum_kegiatan = mysqli_num_rows(mysqli_query($db, "select id from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_desa) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_jumlah = $anggaran_bankeu_desa['anggaran'] + $anggaran_pendamping['anggaran'];

            $panjar_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_bankeu_desa) panjar from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $panjar_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_pendamping) panjar from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $panjar_jumlah = $panjar_bankeu_desa['panjar'] + $panjar_pendamping['panjar'];

            $realisasi_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_desa) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $realisasi_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $realisasi_jumlah = $realisasi_bankeu_desa['anggaran'] + $realisasi_pendamping['anggaran'];

            $target_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $real_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) target from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            //target
            $persen_target = ($target_bankeu_desa['target'] != 0) ? $target_bankeu_desa['target'] / $sum_kegiatan : 0;
            $persen_real = ($real_bankeu_desa['target'] != 0) ? $real_bankeu_desa['target'] / $sum_kegiatan : 0;
           
            //$sqlKegiatan = mysqli_query($db,"select * from t_bankeu_desa where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "' and status_update='0'");

            $sqlSKPD = mysqli_query($db, "select * from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            if ($target_bankeu_desa['target'] != 0) {
                $no_target = $no_target + 1;
            }
            if ($real_bankeu_desa['target'] != 0) {
                $no_real = $no_real + 1;
            }
            $baris = $baris + $sum_kegiatan;
            $sum_anggaran_bankeu_desa = $sum_anggaran_bankeu_desa + $anggaran_bankeu_desa['anggaran'];
            $sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping['anggaran'];
            $sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
            $sum_panjar_bankeu_desa = $sum_panjar_bankeu_desa + $panjar_bankeu_desa['panjar'];
            $sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping['panjar'];
            $sum_panjar_jumlah = $sum_panjar_jumlah + $panjar_jumlah;
            $sum_realisasi_bankeu_desa = $sum_realisasi_bankeu_desa + $realisasi_bankeu_desa['anggaran'];
            $sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping['anggaran'];
            $sum_realisasi_jumlah = $sum_realisasi_jumlah + $realisasi_jumlah;
            $sum_persen_target = $sum_persen_target + $persen_target;
            $sum_persen_real = $sum_persen_real + $persen_real;
        }
      
        ?>

       
[{
    "dana": "Jumlah Anggaran (Rp)",
    "anggaran": <?php echo $sum_anggaran_bankeu_desa;?>
    ,"color":"#80ff00"
},
{
    "dana": "SPJ (Rp)",
    "anggaran": <?php echo $sum_realisasi_bankeu_desa;?>
    ,"color":"#bfff00"
}]
