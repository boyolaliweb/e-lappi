<?php
include ("../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_GET['bln'];
$tahun = $_GET['th'];
?>

        <?php
        $sql = mysqli_query($db, "select distinct(program) from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'  and program<>''");

        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_tp = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_tp = 0;
        $sum_panjar_jumlah = 0;
        $sum_realisasi_jumlah = 0;
        $sum_panjar_tp_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_tp = 0;
        $sum_realisasi_tp_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;
        $no_target = 0;
        $no_real = 0;

        while ($rowProgram = mysqli_fetch_array($sql)) {
          
            //$sqlKegiatan = mysqli_query($db,"select * from t_tp where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "' and status_update='0'");

            $sqlSKPD = mysqli_query($db, "select distinct(id_skpd) from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");

            $baris = 0;

            while ($row = mysqli_fetch_array($sqlSKPD)) {

                $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $row['id_skpd'] . "'"));
                $kegiatan_skpd = mysqli_num_rows(mysqli_query($db, "select kegiatan from t_tp where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                //hitung anggaran Rp
                $anggaran_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_tp = $anggaran_tp['anggaran'];
                $anggaran_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_pendamping = $anggaran_pendamping['anggaran'];
                $anggaran_jumlah = $anggaran_tp + $anggaran_pendamping;
                $panjar_tp = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_tp = $panjar_tp['anggaran'];
                $panjar_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_pendamping = $panjar_pendamping['anggaran'];
                $panjar_jumlah = $panjar_tp + $panjar_pendamping;
                $realisasi_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_tp = $realisasi_tp['anggaran'];
                $realisasi_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and  bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_pendamping = $realisasi_pendamping['anggaran'];
                $realisasi_jumlah = $realisasi_tp + $realisasi_pendamping;
                //realisasi dan target
                $target_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_tp where program='" . $rowProgram['program'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $real_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_tp where program='" . $rowProgram['program'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $persen_target = ($target_tp['target'] != 0) ? $target_tp['target'] / $kegiatan_skpd : 0;
                $persen_real = ($real_tp['reali'] != 0) ? $real_tp['reali'] / $kegiatan_skpd : 0;
              

                $sum_kegiatan = $sum_kegiatan + $kegiatan_skpd;
                $sum_anggaran_tp = $sum_anggaran_tp + $anggaran_tp;
                $sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping;
                $sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
                $sum_panjar_tp = $sum_panjar_tp + $panjar_tp;
                $sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping;
                $sum_panjar_jumlah = $sum_panjar_jumlah + $panjar_jumlah;
                $sum_realisasi_tp = $sum_realisasi_tp + $realisasi_tp;
                $sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping;
                $sum_realisasi_jumlah = $sum_realisasi_jumlah + $realisasi_jumlah;
                $sum_persen_target = $sum_persen_target + $persen_target;
                $sum_persen_real = $sum_persen_real + $persen_real;
            }
            $jumlah_baris = $jumlah_baris + $baris;
            if ($sum_persen_target != 0) {
                $no_target = $no_target + 1;
            }
            if ($sum_persen_real != 0) {
                $no_real = $no_real + 1;
            }
        }
     ?>

<?php 
$fisik = (($sum_persen_real != 0) ? $sum_persen_real / $no_real : 0);
$fisik = number_format($fisik,2);
$total = 100 - $fisik;
?>
[
{
    "country": "Belum Realisasi",
    "value": <?php echo $total;?>
}, {
    "country": "Realisasi Fisik",
    "value": <?php echo $fisik;?>
}
]
