<?php
include ("../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_GET['bln'];
$tahun = $_GET['th'];
//$skpd = $_POST['skpd'];
//$id = 12;
//$tahun = 2016;
?>

       <?php
        $sql = mysqli_query($db, "select distinct(id_skpd) from t_apbd where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");

        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_apbd = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_apbd = 0;
        $sum_panjar_persen = 0;
        $sum_panjar_apbd_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_apbd = 0;
        $sum_realisasi_persen = 0;
        $sum_realisasi_apbd_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;
        $no = 0;
        $no_target = 0;
        $no_real = 0;
        $no_panjar = 0;
        $no_realisasi = 0;

        while ($rowProgram = mysqli_fetch_array($sql)) {
            $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $rowProgram['id_skpd'] . "' limit 1"));
            $sum_kegiatan = mysqli_num_rows(mysqli_query($db, "select id from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));
            $anggaran_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_apbd) anggaran from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $panjar_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_apbd) panjar from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $realisasi_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_apbd) anggaran from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            $target_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));
            $real_apbd = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) target from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' and (program<>'' and kegiatan<>'')"));

            /* 
                and (program<>'' and kegiatan<>'') => tambahan untuk filter perbedaan dana rekap dengan laporan
                rev2.2 (Maret 2016)
            */
            //target
            $persen_target = ($target_apbd['target'] != 0) ? $target_apbd['target'] / $sum_kegiatan : 0;
            $persen_real = ($real_apbd['target'] != 0) ? $real_apbd['target'] / $sum_kegiatan : 0;
            $noBid++;
            //$sqlKegiatan = mysqli_query($db,"select * from t_apbd where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "'");

            $sqlSKPD = mysqli_query($db, "select * from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            //if ($target_apbd['target'] != 0) {
                $no_target = $no_target + 1;
            //}
            //if ($real_apbd['target'] != 0) {
                $no_real = $no_real + 1;
            //}
            //if ($panjar_apbd['panjar'] != 0) {
                $no_panjar = $no_panjar + 1;
            //}
            //if ($realisasi_apbd['anggaran'] != 0) {
                $no_realisasi = $no_realisasi + 1;
            //}
            $baris = $baris + $sum_kegiatan;
            $sum_anggaran_apbd = $sum_anggaran_apbd + $anggaran_apbd['anggaran'];

            $sum_panjar_apbd = $sum_panjar_apbd + $panjar_apbd['panjar'];
            $sum_panjar_persen = ($sum_panjar_persen + (($panjar_apbd['panjar'] != 0) ? $panjar_apbd['panjar'] * 100 / $anggaran_apbd['anggaran'] : 0));
            $sum_realisasi_apbd = $sum_realisasi_apbd + $realisasi_apbd['anggaran'];
            $sum_realisasi_persen = ($sum_realisasi_persen + (($realisasi_apbd['anggaran'] != 0) ? $realisasi_apbd['anggaran'] * 100 / $anggaran_apbd['anggaran'] : 0));
            $sum_persen_target = $sum_persen_target + $persen_target;
            $sum_persen_real = $sum_persen_real + $persen_real;
        }
     
        ?>
<?php 
$fisik = (($sum_persen_real != 0) ? $sum_persen_real / $no_real : 0);
$fisik = number_format($fisik,2);
$total = 100 - $fisik;
?>
[
{
    "country": "Belum Realisasi",
    "value": <?php echo $total;?>
}, {
    "country": "Realisasi Fisik",
    "value": <?php echo $fisik;?>
}
]
