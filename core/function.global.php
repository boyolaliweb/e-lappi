<?php



if (!defined('BASEPATH'))

    exit('Nothing found here');

$tanggal = date("d/m/Y");



function tanggalan($datawaktu) {

    $waktu = explode('-', $datawaktu);

    if ($waktu[1] == '01') {  //jika 01 maka januari

        $bulan = 'Januari';

    } elseif ($waktu[1] == '02') {

        $bulan = 'Februari';

    } elseif ($waktu[1] == '03') {

        $bulan = 'Maret';

    } elseif ($waktu[1] == '04') {

        $bulan = 'April';

    } elseif ($waktu[1] == '05') {

        $bulan = 'Mei';

    } elseif ($waktu[1] == '06') {

        $bulan = 'Juni';

    } elseif ($waktu[1] == '07') {

        $bulan = 'Juli';

    } elseif ($waktu[1] == '08') {

        $bulan = 'Agustus';

    } elseif ($waktu[1] == '09') {

        $bulan = 'September';

    } elseif ($waktu[1] == '10') {

        $bulan = 'Oktober';

    } elseif ($waktu[1] == '11') {

        $bulan = 'November';

    } elseif ($waktu[1] == '12') {

        $bulan = 'Desember';

    } else {

        $bulan = '00';

    }

    return $waktu[2] . ' ' . $bulan . ' ' . $waktu[0];

}



function alert_success() {

    echo "<script type='text/javascript'>alert(\"Berhasil melakukan proses\");</script>";

}



function alert_error() {

    echo "<script type='text/javascript'>alert(\"Gagal melakukan proses\");</script>";

}



function redirect($url) {

    echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL=' . $url . '">';

}



function redirect_time($url, $time) {

    echo '<META HTTP-EQUIV=Refresh CONTENT="' . $time . '; URL=' . $url . '">';

}



function base_url($base = site_url) {

    echo $base;

}



function base_uri($base = site_url) {

    return $base;

}



function url($url, $text) {

    echo "<a href='" . $url . "'>" . $text . "</a>";

}



function url_new($url, $text) {

    echo "<a href='" . $url . "' target='_blank'>" . $text . "</a>";

}



function tanggal_jam() {

    $month = date("m");

    $bulan = bulan($month);

    $value = date("d") . " " . $bulan . " " . date("Y h:i:s");

    return $value;

}



function tanggal() {

    $month = date("m");

    $bulan = bulan($month);

    $value = date("d") . " " . $bulan . " " . date("Y");

    return $value;

}



function tanggal_db() {

    $value = date("Y-m-d");

    return $value;

}



function tanggal_jam_db() {

    $value = date("Y-m-d h:i:s");

    return $value;

}



function jam_db() {

    $value = date("G:i:s");

    return $value;

}



function tanggal_en() {

    $value = date("d F Y");

    return $value;

}



function tanggal_jam_en() {

    $value = date("d F Y h:i:s");

    return $value;

}



function browser_client() {

    $value = getenv("HTTP_USER_AGENT");

    return $value;

}



function ip_client() {

    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {

        if (array_key_exists($key, $_SERVER) === true) {

            foreach (explode(',', $_SERVER[$key]) as $ip) {

                if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {

                    return $ip;

                }

            }

        }

    }

}



function bulan($bulan) {

    switch ($bulan) {

        case '01': $bulan = "Januari";

            break;

        case '02': $bulan = "Februari";

            break;

        case '03': $bulan = "Maret";

            break;

        case '04': $bulan = "April";

            break;

        case '05': $bulan = "Mei";

            break;

        case '06': $bulan = "Juni";

            break;

        case '07': $bulan = "Juli";

            break;

        case '08': $bulan = "Agustgus";

            break;

        case '09': $bulan = "September";

            break;

        case '10': $bulan = "Oktober";

            break;

        case '11': $bulan = "Nopember";

            break;

        case '12': $bulan = "Desember";

            break;

    }

    return $bulan;

}



function do_alert($msg) {

    echo '<script type="text/javascript">alert("' . $msg . '"); </script>';

}



function kode($len) {

    $random = substr(number_format(time() * rand(), 0, '', ''), 0, $len);

    return $random;

}



function truncate($str, $len) {

    $tail = max(0, $len - 10);

    $trunk = substr($str, 0, $tail);

    $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len - $tail))));

    return $trunk;

}



function rupiah($angka) {

    return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));

}



// A function to return the Roman Numeral, given an integer

function romawi($num) {

    // Make sure that we only use the integer portion of the value

    $n = intval($num);

    $result = '';



    // Declare a lookup array that we will use to traverse the number:

    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,

        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,

        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);



    foreach ($lookup as $roman => $value) {

        // Determine the number of matches

        $matches = intval($n / $value);



        // Store that many characters

        $result .= str_repeat($roman, $matches);



        // Substract that from the number

        $n = $n % $value;

    }



    // The Roman numeral should be built, return it

    return $result;

}



function kode_dak($inisial = 'DAK') {

    //$num = mysql_num_rows($db->query("select id from t_dak"));

    global $db;

    $q_num = $db->query("select id from t_dak order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_apbd($inisial = 'APBD') {

    //$num = mysql_num_rows($db->query("select id from t_apbd"));

    global $db;

    $q_num = $db->query("select id from t_apbd order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_bankeu_kab($inisial = 'KAB') {

    //$num = mysql_num_rows($db->query("select id from t_bankeu_kab"));

    global $db;

    $q_num = $db->query("select id from t_bankeu_kab order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_bankeu_desa($inisial = 'DESA') {

    //$num = mysql_num_rows($db->query("select id from t_bankeu_desa"));

    global $db;

    $q_num = $db->query("select id from t_bankeu_desa order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_dbhcht($inisial = 'DBHCHT') {

    //$num = mysql_num_rows($db->query("select id from t_dbhcht"));

    global $db;

    $q_num = $db->query("select id from t_dbhcht order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_dekon($inisial = 'DEKON') {

    //$num = mysql_num_rows($db->query("select id from t_dekon"));

    global $db;

    $q_num = $db->query("select id from t_dekon order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];

    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_dub($inisial = 'DUB') {

    //$num = mysql_num_rows($db->query("select id from t_dub"));

    global $db;

    $q_num = $db->query("select id from t_dub order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];



    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



function kode_tp($inisial = 'TP') {

    //$num = mysql_num_rows($db->query("select id from t_tp"));

    global $db;

    $q_num = $db->query("select id from t_tp order by id desc limit 1");

    $num1 = $q_num->fetch_assoc();

    $num = $num1['id'];

    $panjang = 30;

    $angka = strval($num + 1);

    $tmp = '';

    for ($i = 1; $i <= ($panjang - 0 - strlen($inisial) - strlen($angka)); $i++) {

        $tmp = $tmp . "0";

    }

    return $inisial . $tmp . $angka . "A";

}



?>

