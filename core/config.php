<?php

//if (!defined('BASEPATH')) exit('Nothing found here');
/*
 * File ini berisi variabel setting yang diperlukan dalam program
 * $config['judul'] -> title dari website, letaknya ada di head
 */
$config['judul'] = "JUDUL_WEBSITE";
/* edit baris di bawah ini untuk menentukan base url */
$base = "";

define("site_url", $base);

function tanggal_indo($tgl) {
    $tanggal = substr($tgl, 8, 2);
    $bulan = getBulan(substr($tgl, 5, 2));
    $tahun = substr($tgl, 0, 4);
    return $tanggal . ' ' . $bulan . ' ' . $tahun;
}

function getBulan($bln) {
    switch ($bln) {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
}

?>
