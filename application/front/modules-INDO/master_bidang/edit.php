<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Bidang <small>HALAMAN UNTUK MENGATUR DATA MASTER BIDANG</small></h1>  
</div>
<?php
if (isset($_GET['id'])) {
    $sqlOpen = $db->query("select * from m_bidang where id='" . $_GET['id'] . "' limit 1");
    $sqlOpen->data_seek(0);
    $default = $sqlOpen->fetch_assoc();
} else {
    redirect("master-bidang");
}
?>
<form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Ubah Data Bidang</h2>
                </div>              
                <div class="data-fluid">
                    <div class="row-form">
                        <div class="span3">Urusan</div>
                        <div class="span9">
                            <select name="urusan" id="urusan" onChange="showKode()">
                                <option value="">--pilih urusan--</option>
                                <?php
                                $sqlUrusan = $db->query("select * from m_urusan");
                                $sqlUrusan->data_seek(0);
                                while ($rowUrusan = $sqlUrusan->fetch_assoc()) {
                                    $select = ($rowUrusan['urusan'] == $default['urusan']) ? "selected" : "";
                                    echo '<option value="' . $rowUrusan['urusan'] . '" ' . $select . '>' . $rowUrusan['urusan'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Kode Bidang</div>
                        <div class="span9"><input type="text" name="kode" required="true" id="kode" value="<?php echo $default['kode_urusan'] . '.' . $default['kode_bidang']; ?>"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span3">Bidang</div>
                        <div class="span9"><input type="text" name="bidang" required="true" value="<?php echo $default['bidang']; ?>"></div>
                    </div>

                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>


<?php
//processing submit data
if (isset($_POST['submit'])) {
    $kode = explode('.', $_POST['kode']);
    $kode_urusan = $kode[0];
    $kode_bidang = $kode[1];
    $sql = $db->query("update m_bidang set
                    kode_urusan = '" . $kode_urusan . "',
                    kode_bidang = '" . $kode_bidang . "',
                    urusan = '" . $_POST['urusan'] . "',
                    bidang = '" . $_POST['bidang'] . "'
                    where id ='" . $_POST['id'] . "'
            ");
    if ($sql) {
        alert_success();
        redirect("master-bidang");
    }
}
?>