<?php
include ("../../../../core/db.config.php");
$sql_status = mysqli_fetch_array(mysqli_query($db, "select status from m_entry where id='1' limit 1"));
$STATUS = $sql_status['status'];

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
$level = $_POST['level'];
$per_skpd = $_POST['per_skpd'];
?>
<center>
    <h5>LAPORAN KEGIATAN YANG BERSUMBER DARI DANA URUSAN BERSAMA (DUB)<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE <?php echo strtoupper(bulan($id)); ?> </h5>
</center>
<div class="span4 pull-right">
    <input type="text" id="search" placeholder="Pencarian">
    <br><br>
</div>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">No</th>
            <th rowspan="2">PROGRAM/KEGIATAN</th>
            <th colspan="3">ANGGARAN (Rp.)</th>
            <th rowspan="2">Anggaran Kas</th>
            <th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
            <th colspan="2">Progres Fisik</th>
            <th rowspan="2">KET.</th>
            <th rowspan="2">Sisa Anggaran Kas</th>
            
        </tr>
        <tr>
            <th>DUB</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>DUB</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>DUB</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>Target (%)</th>
            <th>Real (%)</th>
        </tr>
        <tr>
            <th></th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
            <th>16</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
            $sql = mysqli_query($db, "select distinct(id_skpd) from t_dub where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        } else {
            if ($per_skpd == "all") { //login as administrator
                $sql = mysqli_query($db, "select distinct(id_skpd) from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            } else {
                $sql = mysqli_query($db, "select distinct(id_skpd) from t_dub where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            }
        }
        $noBid = 1;
        //make looping to get data
        while ($rowBid = mysqli_fetch_array($sql)) {
            $row_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1"));
            echo '
							<tr>
								<td></td>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $row_skpd['nama'] . '</b></td>
								<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
								<td></td><td></td><td></td><td></td><td></td>
							</tr>
						';
            //$sql_program = mysqli_query($db, "select distinct(program) from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            $sql_program = $db->query("SELECT DISTINCT (a.program)
                            FROM t_dub a
                            INNER JOIN m_program b ON a.program = b.program where a.id_skpd='" . $rowBid['id_skpd'] . "' and a.bulan='".$id."'
                            and a.tahun='".$tahun."' and status_update='0' order by b.kode_urusan asc,
                            b.kode_bidang asc,b.kode_program asc");
            $jum3 = 0;
            $jum4 = 0;
            $jum5 = 0;
            $jum6 = 0;
            $jum7 = 0;
            $jum8 = 0;
            $jum9 = 0;
            $jum10 = 0;
            $jum11 = 0;
            $jum12 = 0;
            $jum13 = 0;
            $jum15 = 0;
            //progres real
            $real = 0;
            $target = 0;

            while ($row_program = mysqli_fetch_array($sql_program)) {
                echo '
                                    <tr>
                                            <td>&nbsp;</td><td>&nbsp;</td>
                                            <td><b>' . $row_program['program'] . '</b></td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                    </tr>
                                ';
                if (isset($_SESSION['id_skpd'])) {
                    $sqlKegiatan = mysqli_query($db, "select * from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "' and status_update='0'");
                } else {
                    if ($per_skpd == "all") { //login as administrator
                        $sqlKegiatan = mysqli_query($db, "select * from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "' and status_update='0'");
                    } else {
                        $sqlKegiatan = mysqli_query($db, "select * from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "' and status_update='0'");
                    }
                }
                $no = 1;

                while ($row = mysqli_fetch_array($sqlKegiatan)) {
                    $jum3 = $jum3 + $row['anggaran_dub'];
                    $jum4 = $jum4 + $row['anggaran_pendamping'];
                    $jum5 = $jum5 + $row['anggaran_jumlah'];
                    $jum6 = $jum6 + $row['panjar_dub'];
                    $jum7 = $jum7 + $row['panjar_pendamping'];
                    $jum8 = $jum8 + $row['panjar_jumlah'];
                    $jum9 = $jum9 + $row['realisasi_dub'];
                    $jum10 = $jum10 + $row['realisasi_pendamping'];
                    $jum11 = $jum11 + $row['realisasi_jumlah'];
                    $jum12 = $jum12 + $row['progres_target'];
                    $jum13 = $jum13 + $row['progres_real'];
                    $jum15 = $jum15 + $row['anggaran_kas'];
                     $real = $real + 1;
                    $target = $target + 1;
                    $anggaran_dub = ($row['anggaran_dub'] == 0) ? "-" : number_format($row['anggaran_dub'], 0, ",", ".");
                    $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                    $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                    $panjar_dub = ($row['panjar_dub'] == 0) ? "-" : number_format($row['panjar_dub'], 0, ",", ".");
                    $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                    $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                    $realisasi_dub = ($row['realisasi_dub'] == 0) ? "-" : number_format($row['realisasi_dub'], 0, ",", ".");
                    $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                    $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                    $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 2, ",", ".");
                    $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 2, ",", ".");
                    $anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");

                     $sisa_anggaran_kas = number_format($row['anggaran_kas'] - $row['realisasi_jumlah'],0, ",", ".");
                    if ($skpd > 0) {
                        $ubah = "";
                        if ($row['final'] == 0 && $STATUS == "open") {
                            $icon = 'pencil';
                            $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['kode'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dub-ok&id=' . $row['id'] . '" onclick="return confirm(\'Apakah yakin sudah final? Klik Tombol OK untuk melanjutkan proses. Untuk merubah data yang sudah final, silahkan hubungi administrator. Terima kasih\')">
													<span class="icon-ok"></span> Tandai Final
													</a></li>';
                        } else {
                            $icon = 'ok';
                            $tombol = '<li><a href="#"><i class="icon-ban-circle"></i> Hubungi administrator untuk ubah</a></li>';
                        }
                    } else
                    if ($skpd == 0) {
                        if ($level == "view") {
                            $ubah = "";
                            $icon = "";
                            $tombol = "";
                        } else
                        if ($level == "administrator") {
                            $ubah = "Ubah";
                            if ($row['final'] == 0) {
                                $icon = 'pencil';
                                $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['kode'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>';
                            } else {
                                $icon = 'ok';
                                $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['kode'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dub-revisi&id=' . $row['id'] . '" onclick="return confirm(\'Data ini akan direvisi?\')">
													<span class="icon-retweet"></span> Revisi Data Laporan
													</a></li>';
                            }
                        }
                    }
                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                    $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                    $subkegiatan = ($row['subkegiatan']) ? " " . $row['subkegiatan'] . "<br>" : "";
                    echo '
								<tr>
									<td>
										<div class="btn-group">
                                            &nbsp;<button class="btn btn-link dropdown-toggle" data-toggle="dropdown" style="color:blue;"><span class="icon-' . $icon . '"></span> ' . $ubah . '</button>&nbsp;
                                            <ul class="dropdown-menu">
                                                ' . $tombol . '
                                            </ul>
                                        </div>
                                        </td>
                                        <td align="center">' . $no++ . '</td>
                                        <td>' . $row['kegiatan'] . $subkegiatan . $lokasi . '</td>
                                        <td align="right">' . $anggaran_dub . '</td>
                                        <td align="right">' . $anggaran_pendamping . '</td>
                                        <td align="right">' . $anggaran_jumlah . '</td>
                                            <td align="right">' . $anggaran_kas . '</td>
                                        <td align="right">' . $panjar_dub . '</td>
                                        <td align="right">' . $panjar_pendamping . '</td>
                                        <td align="right">' . $panjar_jumlah . '</td>
                                        <td align="right">' . $realisasi_dub . '</td>
                                        <td align="right">' . $realisasi_pendamping . '</td>
                                        <td align="right">' . $realisasi_jumlah . '</td>
                                        <td align="right">' . $progres_target . '</td>
                                        <td align="right">' . $progres_real . '</td>
                                        <td>' . $masalah . '</td>							
                                        <td align="right">' . $sisa_anggaran_kas . '</td>
                                </tr>
                                ';
                }
            }

            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
            $jum12 = ($jum12 == 0) ? "-" : number_format(($jum12 / $target), 2, ",", "."); //target
            $jum13 = ($jum13 == 0) ? "-" : number_format(($jum13 / $real), 2, ",", "."); //real
            $jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
            echo '
                    <tr>
                            <td></td><td></td>
                            <td align="center"><b>Jumlah</b></td>
                            <td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
                            <td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum15 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
                            <td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
                            <td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
                            <td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
                            <td align="right"><b>' . $jum13 . '</b></td><td align="right"></td><td align="right"></td>

                    </tr>
            ';
        }
        ?>
    </tbody>
</table>

<script>
    $(document).ready(function() {
        var $rows = $('#table tr');
        $('#search').keyup(function() {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
    });
</script>