<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master SKPD <small>HALAMAN UNTUK MENGATUR DATA MASTER SKPD</small></h1>  
</div>
<?php
if (isset($_GET['id'])) {
    $sqlOpen = $db->query("select * from m_skpd where id='" . $_GET['id'] . "' limit 1");
    $sqlOpen->data_seek(0);
    $default = $sqlOpen->fetch_assoc();
} else {
    redirect("master-skpd");
}
?>
<form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Ubah Data SKPD</h2>
                </div>              
                <div class="data-fluid">
                    <div class="row-form">
                        <div class="span3">Jenis SKPD</div>
                        <div class="span9">
                            <select name="jenis_skpd">
                                <option value="SKPD" <?php echo $option = ($default['jenis'] == "SKPD") ? "selected" : ""; ?>>SKPD</option>
                                <option value="Kecamatan" <?php echo $option = ($default['jenis'] == "Kecamatan") ? "selected" : ""; ?>>Kecamatan</option>
                                <option value="Kelurahan" <?php echo $option = ($default['jenis'] == "Kelurahan") ? "selected" : ""; ?>>Kelurahan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Kode SKPD</div>
                        <div class="span9"><input type="text" name="kode" required="true" value="<?php echo $default['kode']; ?>"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span3">Nama SKPD:</div>
                        <div class="span9"><input type="text" name="nama" required="true" value="<?php echo $default['nama']; ?>"></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Kepala SKPD</div>
                        <div class="span9"><input type="text" name="kepala" value="<?php echo $default['kepala']; ?>"></div>               
                    </div>
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Data Login</h2>
                </div>                                        
                <div class="data-fluid">
                </div>  <div class="row-form">
                    <div class="span3">Username</div>
                    <div class="span9">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user icon-white"></i></span>
                            <input type="text" name="username" style="width: 331px;" required="true" value="<?php echo $default['username']; ?>">                               
                        </div>                  
                    </div>
                </div>  <div class="row-form">
                    <div class="span3">Password</div>
                    <div class="span9">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock icon-white"></i></span>
                            <input type="password" name="password" style="width: 331px;" required="true" value="<?php echo $default['password']; ?>">                               
                        </div>                  
                    </div>
                </div>   
            </div>
        </div>
    </div>
</form>
</div>

<?php
//processing submit data
if (isset($_POST['submit'])) {
    $sql = $db->query("update m_skpd set
                    kode = '" . $_POST['kode'] . "',
                    nama = '" . $_POST['nama'] . "',
                    kepala = '" . $_POST['kepala'] . "',
                    jenis = '" . $_POST['jenis_skpd'] . "',
                    username = '" . $_POST['username'] . "',
                    password = '" . $_POST['password'] . "'
                    where id ='" . $_POST['id'] . "'
            ");
    if ($sql) {
        alert_success();
        redirect("master-skpd");
    }
}
?>