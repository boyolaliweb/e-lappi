<?php
//processing submit data
if (isset($_POST['submit'])) {
    if (isset($_POST['kegiatan'])) {
        $kegiatan = explode("_", $_POST['kegiatan']);
        $kode_permendagri = $kegiatan[0];
        $kegiatan = $kegiatan[1];
        $permendagri = explode(".", $kode_permendagri);
        $kode_urusan = $permendagri[0];
        $kode_bidang = $permendagri[1];
        $kode_program = $permendagri[2];
        $kode_kegiatan = $permendagri[3];
        //$get_kode = mysqli_fetch_array($db->query("select kode from t_apbd where id='".$_POST['id']."'"));
        //$kode = $get_kode['kode'];
        //$kode++;
        //$sql = $db->query("insert into `t_apbd` (kode,bagian,subbagian,bidang,program,kegiatan,lokasi,anggaran_apbd,panjar_apbd,realisasi_apbd,progres_target,progres_real,permasalahan,keterangan,id_skpd,bulan,tahun,final,anggaran_kas) values ('".$kode."','".$_POST['bagian']."','".$_POST['subbagian']."','','".$_POST['program']."','".$_POST['kegiatan']."','".$_POST['lokasi']."','".$_POST['anggaran_apbd']."','".$_POST['panjar_apbd']."','".$_POST['realisasi_apbd']."','".$_POST['progres_target']."','".$_POST['progres_real']."','".$_POST['permasalahan']."','".$_POST['keterangan']."','".$_POST['skpd']."','".$_POST['bulan']."','".$_POST['tahun']."','0','".$_POST['anggaran_kas']."')");

        //update status
        //$sql = $db->query("update `t_apbd` set status_update='1' where id='".$_POST['id']."'");
        $get_bidang = mysqli_fetch_array($db->query("select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1"));
        $get_program = mysqli_fetch_array($db->query("select program from m_program where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' limit 1"));
        $get_kegiatan = mysqli_fetch_array($db->query("select kegiatan from m_kegiatan where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' and kode_kegiatan='" . $kode_kegiatan . "' limit 1"));

        $sql = $db->query("update `t_apbd` set
					bagian='',
					subbagian='" . $_POST['subbagian'] . "',
					bidang='',
					program='" . $get_program['program'] . "',
					kegiatan='" . $get_kegiatan['kegiatan'] . "',
                                            subkegiatan = '".$_POST['subkegiatan']."',
					lokasi='" . $_POST['lokasi'] . "',
					anggaran_apbd='" . $_POST['anggaran_apbd'] . "',
					panjar_apbd='" . $_POST['panjar_apbd'] . "',
					realisasi_apbd='" . $_POST['realisasi_apbd'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					keterangan='" . $_POST['keterangan'] . "',
					kode_bidang='" . $kode_bidang . "',
					kode_program='" . $kode_program . "',
					kode_kegiatan='" . $kode_kegiatan . "',
					id_skpd='" . $_POST['skpd'] . "',
					anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");

        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-apbd", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-apbd", "2");
        }
    }else{

        $sql = $db->query("update `t_apbd` set
					bagian='',
					bidang='',
                                        subkegiatan = '".$_POST['subkegiatan']."',
					lokasi='" . $_POST['lokasi'] . "',
					anggaran_apbd='" . $_POST['anggaran_apbd'] . "',
					panjar_apbd='" . $_POST['panjar_apbd'] . "',
					realisasi_apbd='" . $_POST['realisasi_apbd'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					keterangan='" . $_POST['keterangan'] . "',
					id_skpd='" . $_POST['skpd'] . "',
					anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");

        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-apbd", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-apbd", "2");
        }
    }
}
?>