<script type="text/javascript">
    $(function() {
        $('#bulan').change(function(event) {
            //alert($('#bulan').val());
            urls = 'application/front/modules/laporan_dak/item_bulan.php';
            $.post(urls, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data) {
                $('#tableWeb').html(data);
            }
            );

            urls2 = 'application/front/modules/laporan_dak/item_bulan_print.php';
            $.post(urls2, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
        $('#triwulan').change(function(event) {
            urls = 'application/front/modules/laporan_dak/item_triwulan.php';
            $.post(urls, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data) {
                $('#tableWeb').html(data);
            }
            );
            urls2 = 'application/front/modules/laporan_dak/item_triwulan_print.php';
            $.post(urls2, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
    });
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Anggaran Kas :: Dana Alokasi Khusus (DAK) <small>ANGGARAN KAS BERSUMBER DARI DANA ALOKASI KHUSUS</small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2> Dana Alokasi Khusus </h2>
				
                <div class="printed">
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="icon-print icon-white"></span> Cetak &nbsp;<span class="ico-caret-down"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="print('tableExcel')"><span class="ico-caret-right"></span> Bappeda</a></li>
                            <li><a href="#" onclick="print('tableExcel2')"><span class="ico-caret-right"></span> DPPKAD</a></li>
                            <li><a href="#" onclick="print('tableExcel3')"><span class="ico-caret-right"></span> Bagian Pembangunan Setda</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="icon-book icon-white"></span> Export to Excel&nbsp;&nbsp;<span class="ico-caret-down"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="excel"><span class="ico-caret-right"></span> Bappeda</a></li>
                            <li><a href="#" id="excel2"><span class="ico-caret-right"></span> DPPKAD</a></li>
                            <li><a href="#" id="excel3"><span class="ico-caret-right"></span> Bagian Pembangunan Setda</a></li>
                        </ul>
                    </div>
                </div>
                                              
            </div>         
			<!--
            <div>
                <h3>Filtering </h3>
                <table>
                    <tr>
                        <td>
                            Bulanan: 
                        </td>
                        <td>
                            <select name="bulan" id="bulan">
                                <option value="">--Pilih Bulan--</option>
                                <option value="01">Januari</option>
                                <option value="01">Februari</option>
                                <option value="02">Maret</option>
                                <option value="03">April</option>
                                <option value="04">Mei</option>
                                <option value="05">Juni</option>
                                <option value="06">Juli</option>
                                <option value="07">Agustus</option>
                                <option value="08">September</option>
                                <option value="09">Oktober</option>
                                <option value="10">Nopember</option>
                                <option value="11">Desember</option>
                            </select>
                        </td>
                    </tr>
                   
					<tr>
                        <td>Triwulan: </td>
                        <td>
                            <select name="triwulan" id="triwulan">
                                <option value="">--Pilih Triwulan--</option>
                                <option value="1">Januari-Maret</option>
                                <option value="2">April-Juni</option>
                                <option value="3">Juli-September</option>
                                <option value="4">Oktober-Desember</option>
                            </select>
                        </td>
                    </tr>
					
                </table>
                </ul>   
				
            </div>     
			-->
            <div>&nbsp;</div>
            <div class="data-fluid" id="tableExcel">
                <center>
                    <h3>LAPORAN ANGGARAN KAS BERSUMBER DARI DANA ALOKASI KHUSUS (DAK)<BR>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $_SESSION['tahun']; ?><br>( BAPPEDA )</h3>
                </center>
                <table cellpadding="0" border="1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">BIDANG/PROGRAM/KEGIATAN</th>
                            <th colspan="3">ANGGARAN (Rp.)</th>
                            <th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
                            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
                            <th colspan="2">Progres Fisik</th>
                            <th rowspan="2">KET.</th>
							<th rowspan="2">Anggaran Kas</th>
                        </tr>
                        <tr>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>Target (%)</th>
                            <th>Real (%)</th>
                        </tr>
                        <tr>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
							<th>15</th>
                        </tr>
                    </thead>
                    <tbody id="table">
                        <?php
                        if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
                            $sql = mysqli_query($db,"select distinct(bidang) from t_dak where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        } else {
                            $sql = mysqli_query($db,"select distinct(bidang) from t_dak where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        }
                        $noBid = 1;
                        //make looping to get data
                        while ($rowBid = mysqli_fetch_array($sql)) {
                            echo '
							<tr>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $rowBid['bidang'] . '</b></td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                                                <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                                                
							</tr>
						';
                            if (isset($_SESSION['id_skpd'])) {
                                $sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                            } else {
                                $sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                            }
                            $no = 1;
                            $jum3 = 0;
                            $jum4 = 0;
                            $jum5 = 0;
                            $jum6 = 0;
                            $jum7 = 0;
                            $jum8 = 0;
                            $jum9 = 0;
                            $jum10 = 0;
                            $jum11 = 0;
                            $jum12 = 0;
                            $jum13 = 0;
							$jum15 = 0;
                            while ($row = mysqli_fetch_array($sqlKegiatan)) {
                                $jum3 = $jum3 + $row['anggaran_dak'];
                                $jum4 = $jum4 + $row['anggaran_pendamping'];
                                $jum5 = $jum5 + $row['anggaran_jumlah'];
                                $jum6 = $jum6 + $row['panjar_dak'];
                                $jum7 = $jum7 + $row['panjar_pendamping'];
                                $jum8 = $jum8 + $row['panjar_jumlah'];
                                $jum9 = $jum9 + $row['realisasi_dak'];
                                $jum10 = $jum10 + $row['realisasi_pendamping'];
                                $jum11 = $jum11 + $row['realisasi_jumlah'];
                                $jum12 = $jum12 + $row['progres_target'];
                                $jum13 = $jum13 + $row['progres_real'];
                                $anggaran_dak = ($row['anggaran_dak'] == 0) ? "-" : number_format($row['anggaran_dak'], 0, ",", ".");
                                $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                                $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                                $panjar_dak = ($row['panjar_dak'] == 0) ? "-" : number_format($row['panjar_dak'], 0, ",", ".");
                                $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                                $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                                $realisasi_dak = ($row['realisasi_dak'] == 0) ? "-" : number_format($row['realisasi_dak'], 0, ",", ".");
                                $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                                $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                                $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
                                $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
								$anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");
                                $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                                $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                                echo '
								<tr>
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_dak . '</td>
									<td align="right">' . $anggaran_pendamping . '</td>
									<td align="right">' . $anggaran_jumlah . '</td>
									<td align="right">' . $panjar_dak . '</td>
									<td align="right">' . $panjar_pendamping . '</td>
									<td align="right">' . $panjar_jumlah . '</td>
									<td align="right">' . $realisasi_dak . '</td>
									<td align="right">' . $realisasi_pendamping . '</td>
									<td align="right">' . $realisasi_jumlah . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td>' . ($masalah) . '</td>							
									<td align="right">' . $anggaran_kas . '</td>
								</tr>
								';
                            }
                            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
                            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
                            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
                            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
                            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
                            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
                            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
                            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
                            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
                            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
                            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
							$jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
                            echo '
							<tr>
								<td>&nbsp;</td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
								<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
								<td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>
								<td align="right"><b>' . $jum15 . '</b></td>
							</tr>
						';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
            <div class="data-fluid" id="tableWeb">
                <table cellpadding="0" border="1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            
                            <th rowspan="2">No</th>
                            <th rowspan="2">BIDANG/PROGRAM/KEGIATAN</th>
                            <th colspan="3">ANGGARAN (Rp.)</th>
                            <th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
                            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
                            <th colspan="2">Progres Fisik</th>
                            <th rowspan="2">KET.</th>
							<th rowspan="2">Anggaran Kas</th>
                        </tr>
                        <tr>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DAK</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>Target (%)</th>
                            <th>Real (%)</th>
                        </tr>
                        <tr>
                            
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
							<th>15</th>
                        </tr>
                    </thead>
                    <tbody id="table">
                        <?php
                        if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
                            $sql = mysqli_query($db,"select distinct(bidang) from t_dak where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        } else {
                            $sql = mysqli_query($db,"select distinct(bidang) from t_dak where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        }
                        $noBid = 1;
                        //make looping to get data
                        while ($rowBid = mysqli_fetch_array($sql)) {
                            echo '
							<tr>
								
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $rowBid['bidang'] . '</b></td>
								<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
								<td></td><td></td><td></td><td></td>
							</tr>
						';
                            if (isset($_SESSION['id_skpd'])) {
                                $sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                            } else {
                                $sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                            }
                            $no = 1;
                            $jum3 = 0;
                            $jum4 = 0;
                            $jum5 = 0;
                            $jum6 = 0;
                            $jum7 = 0;
                            $jum8 = 0;
                            $jum9 = 0;
                            $jum10 = 0;
                            $jum11 = 0;
                            $jum12 = 0;
                            $jum13 = 0;
							$jum15 = 0; //anggaran_kas
							
							
                            while ($row = mysqli_fetch_array($sqlKegiatan)) {
							$masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                                $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                                $jum3 = $jum3 + $row['anggaran_dak'];
                                $jum4 = $jum4 + $row['anggaran_pendamping'];
                                $jum5 = $jum5 + $row['anggaran_jumlah'];
                                $jum6 = $jum6 + $row['panjar_dak'];
                                $jum7 = $jum7 + $row['panjar_pendamping'];
                                $jum8 = $jum8 + $row['panjar_jumlah'];
                                $jum9 = $jum9 + $row['realisasi_dak'];
                                $jum10 = $jum10 + $row['realisasi_pendamping'];
                                $jum11 = $jum11 + $row['realisasi_jumlah'];
                                $jum12 = $jum12 + $row['progres_target'];
                                $jum13 = $jum13 + $row['progres_real'];
								$jum15 = $jum15 + $row['anggaran_kas']; //anggaran_kas
                                $anggaran_dak = ($row['anggaran_dak'] == 0) ? "-" : number_format($row['anggaran_dak'], 0, ",", ".");
                                $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                                $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                                $panjar_dak = ($row['panjar_dak'] == 0) ? "-" : number_format($row['panjar_dak'], 0, ",", ".");
                                $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                                $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                                $realisasi_dak = ($row['realisasi_dak'] == 0) ? "-" : number_format($row['realisasi_dak'], 0, ",", ".");
                                $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                                $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                                $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
                                $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
								//di bawah untuk anggaran kas
								$anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");
								
                                //di bawah ini cek status final dari laporan
                                if ($_SESSION['level'] == "skpd") {
                                    if ($row['final'] == 0) {
                                        $icon = 'pencil';
                                        $tombol = '<li><a href="index.php?val=laporan-dak-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dak-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dak-ok&id=' . $row['id'] . '" onclick="return confirm(\'Apakah yakin sudah final? Klik Tombol OK untuk melanjutkan proses. Untuk merubah data yang sudah final, silahkan hubungi administrator. Terima kasih\')">
													<span class="icon-ok"></span> Tandai Final
													</a></li>';
                                    } else {
                                        $icon = 'ok';
                                        $tombol = '<li><a href="#"><i class="icon-ban-circle"></i> Hubungi administrator untuk ubah</a></li>';
                                    }
                                } else
                                if ($_SESSION['level'] == "administrator") {
                                    if ($row['final'] == 0) {
                                        $icon = 'pencil';
                                        $tombol = '<li><a href="index.php?val=laporan-dak-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dak-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>';
                                    } else {
                                        $icon = 'ok';
                                        $tombol = '<li><a href="index.php?val=laporan-dak-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dak-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dak-revisi&id=' . $row['id'] . '" onclick="return confirm(\'Data ini akan direvisi?\')">
													<span class="icon-retweet"></span> Revisi Data Laporan
													</a></li>';
                                    }
                                }

                                $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                                echo '
								<tr>
									
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_dak . '</td>
									<td align="right">' . $anggaran_pendamping . '</td>
									<td align="right">' . $anggaran_jumlah . '</td>
									<td align="right">' . $panjar_dak . '</td>
									<td align="right">' . $panjar_pendamping . '</td>
									<td align="right">' . $panjar_jumlah . '</td>
									<td align="right">' . $realisasi_dak . '</td>
									<td align="right">' . $realisasi_pendamping . '</td>
									<td align="right">' . $realisasi_jumlah . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td align="right">' . $masalah . '</td>
									
									<td align="right">' . $anggaran_kas . '</td>
								</tr>
								';
                            }
                            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
                            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
                            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
                            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
                            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
                            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
                            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
                            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
                            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
                            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
                            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
							$jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
                            echo '
							<tr>
								<td></td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
								<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
								<td align="right"><b>' . $jum13 . '</b></td><td align="right"></td>
								<td align="right"><b>' . $jum15 . '</b></td>
                                                                    
							</tr>
						';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>

<div class="data-fluid" id="tableExcel2" style="display:none;">
	<center>
		<h3>LAPORAN ANGGARAN KAS BERSUMBER DARI DANA ALOKASI KHUSUS (DAK)<BR>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $_SESSION['tahun']; ?><br>
		( DPPKAD )</h3>
	</center>
	<table cellpadding="0" border="1" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">BIDANG/PROGRAM/KEGIATAN</th>
				<th colspan="3">ANGGARAN (Rp.)</th>
				<th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
				<th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
				<th colspan="2">Progres Fisik</th>
				<th rowspan="2">KET.</th>
				<th rowspan="2">Anggaran Kas</th>
			</tr>
			<tr>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>Target (%)</th>
				<th>Real (%)</th>
			</tr>
			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
			</tr>
		</thead>
		<tbody id="table">
			<?php
			if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
				$sql = mysqli_query($db,"select distinct(bidang) from t_dak where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
			} else {
				$sql = mysqli_query($db,"select distinct(bidang) from t_dak where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
			}
			$noBid = 1;
			//make looping to get data
			while ($rowBid = mysqli_fetch_array($sql)) {
				echo '
				<tr>
					<td align="center"><b>' . romawi($noBid++) . '</b></td>
					<td><b>' . $rowBid['bidang'] . '</b></td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
													<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
													
				</tr>
			';
				if (isset($_SESSION['id_skpd'])) {
					$sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
				} else {
					$sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
				}
				$no = 1;
				$jum3 = 0;
				$jum4 = 0;
				$jum5 = 0;
				$jum6 = 0;
				$jum7 = 0;
				$jum8 = 0;
				$jum9 = 0;
				$jum10 = 0;
				$jum11 = 0;
				$jum12 = 0;
				$jum13 = 0;
				$jum15 = 0;
				while ($row = mysqli_fetch_array($sqlKegiatan)) {
					$jum3 = $jum3 + $row['anggaran_dak'];
					$jum4 = $jum4 + $row['anggaran_pendamping'];
					$jum5 = $jum5 + $row['anggaran_jumlah'];
					$jum6 = $jum6 + $row['panjar_dak'];
					$jum7 = $jum7 + $row['panjar_pendamping'];
					$jum8 = $jum8 + $row['panjar_jumlah'];
					$jum9 = $jum9 + $row['realisasi_dak'];
					$jum10 = $jum10 + $row['realisasi_pendamping'];
					$jum11 = $jum11 + $row['realisasi_jumlah'];
					$jum12 = $jum12 + $row['progres_target'];
					$jum13 = $jum13 + $row['progres_real'];
					$anggaran_dak = ($row['anggaran_dak'] == 0) ? "-" : number_format($row['anggaran_dak'], 0, ",", ".");
					$anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
					$anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
					$panjar_dak = ($row['panjar_dak'] == 0) ? "-" : number_format($row['panjar_dak'], 0, ",", ".");
					$panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
					$panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
					$realisasi_dak = ($row['realisasi_dak'] == 0) ? "-" : number_format($row['realisasi_dak'], 0, ",", ".");
					$realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
					$realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
					$progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
					$progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
					$anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");
					$masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
					$lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
					echo '
					<tr>
						<td align="center">' . $no++ . '</td>
						<td>' . $row['kegiatan'] . $lokasi . '</td>
						<td align="right">' . $anggaran_dak . '</td>
						<td align="right">' . $anggaran_pendamping . '</td>
						<td align="right">' . $anggaran_jumlah . '</td>
						<td align="right">' . $panjar_dak . '</td>
						<td align="right">' . $panjar_pendamping . '</td>
						<td align="right">' . $panjar_jumlah . '</td>
						<td align="right">' . $realisasi_dak . '</td>
						<td align="right">' . $realisasi_pendamping . '</td>
						<td align="right">' . $realisasi_jumlah . '</td>
						<td align="right">' . $progres_target . '</td>
						<td align="right">' . $progres_real . '</td>
						<td>' . ($masalah) . '</td>							
						<td align="right">' . $anggaran_kas . '</td>
					</tr>
					';
				}
				$jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
				$jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
				$jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
				$jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
				$jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
				$jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
				$jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
				$jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
				$jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
				$jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
				$jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
				$jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
				echo '
				<tr>
					<td>&nbsp;</td>
					<td align="center"><b>Jumlah</b></td>
					<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
					<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
					<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
					<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
					<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
					<td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>
					<td align="right"><b>' . $jum15 . '</b></td>
				</tr>
			';
			}
			?>
		</tbody>
	</table>
</div> 

<div class="data-fluid" id="tableExcel3" style="display:none;">
	<center>
		<h3>LAPORAN ANGGARAN KAS BERSUMBER DARI DANA ALOKASI KHUSUS (DAK)<BR>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $_SESSION['tahun']; ?><br>( BAGIAN PEMBANGUNAN SETDA )</h3>
	</center>
	<table cellpadding="0" border="1" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">BIDANG/PROGRAM/KEGIATAN</th>
				<th colspan="3">ANGGARAN (Rp.)</th>
				<th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
				<th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
				<th colspan="2">Progres Fisik</th>
				<th rowspan="2">KET.</th>
				<th rowspan="2">Anggaran Kas</th>
			</tr>
			<tr>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>DAK</th>
				<th>Dana Pendamping</th>
				<th>JUMLAH</th>
				<th>Target (%)</th>
				<th>Real (%)</th>
			</tr>
			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
			</tr>
		</thead>
		<tbody id="table">
			<?php
			if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
				$sql = mysqli_query($db,"select distinct(bidang) from t_dak where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
			} else {
				$sql = mysqli_query($db,"select distinct(bidang) from t_dak where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
			}
			$noBid = 1;
			//make looping to get data
			while ($rowBid = mysqli_fetch_array($sql)) {
				echo '
				<tr>
					<td align="center"><b>' . romawi($noBid++) . '</b></td>
					<td><b>' . $rowBid['bidang'] . '</b></td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
													<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
													
				</tr>
			';
				if (isset($_SESSION['id_skpd'])) {
					$sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
				} else {
					$sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
				}
				$no = 1;
				$jum3 = 0;
				$jum4 = 0;
				$jum5 = 0;
				$jum6 = 0;
				$jum7 = 0;
				$jum8 = 0;
				$jum9 = 0;
				$jum10 = 0;
				$jum11 = 0;
				$jum12 = 0;
				$jum13 = 0;
				$jum15 = 0;
				while ($row = mysqli_fetch_array($sqlKegiatan)) {
					$jum3 = $jum3 + $row['anggaran_dak'];
					$jum4 = $jum4 + $row['anggaran_pendamping'];
					$jum5 = $jum5 + $row['anggaran_jumlah'];
					$jum6 = $jum6 + $row['panjar_dak'];
					$jum7 = $jum7 + $row['panjar_pendamping'];
					$jum8 = $jum8 + $row['panjar_jumlah'];
					$jum9 = $jum9 + $row['realisasi_dak'];
					$jum10 = $jum10 + $row['realisasi_pendamping'];
					$jum11 = $jum11 + $row['realisasi_jumlah'];
					$jum12 = $jum12 + $row['progres_target'];
					$jum13 = $jum13 + $row['progres_real'];
					$anggaran_dak = ($row['anggaran_dak'] == 0) ? "-" : number_format($row['anggaran_dak'], 0, ",", ".");
					$anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
					$anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
					$panjar_dak = ($row['panjar_dak'] == 0) ? "-" : number_format($row['panjar_dak'], 0, ",", ".");
					$panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
					$panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
					$realisasi_dak = ($row['realisasi_dak'] == 0) ? "-" : number_format($row['realisasi_dak'], 0, ",", ".");
					$realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
					$realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
					$progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
					$progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
					$anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");
					$masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
					$lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
					echo '
					<tr>
						<td align="center">' . $no++ . '</td>
						<td>' . $row['kegiatan'] . $lokasi . '</td>
						<td align="right">' . $anggaran_dak . '</td>
						<td align="right">' . $anggaran_pendamping . '</td>
						<td align="right">' . $anggaran_jumlah . '</td>
						<td align="right">' . $panjar_dak . '</td>
						<td align="right">' . $panjar_pendamping . '</td>
						<td align="right">' . $panjar_jumlah . '</td>
						<td align="right">' . $realisasi_dak . '</td>
						<td align="right">' . $realisasi_pendamping . '</td>
						<td align="right">' . $realisasi_jumlah . '</td>
						<td align="right">' . $progres_target . '</td>
						<td align="right">' . $progres_real . '</td>
						<td>' . ($masalah) . '</td>							
						<td align="right">' . $anggaran_kas . '</td>
					</tr>
					';
				}
				$jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
				$jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
				$jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
				$jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
				$jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
				$jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
				$jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
				$jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
				$jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
				$jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
				$jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
				$jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
				echo '
				<tr>
					<td>&nbsp;</td>
					<td align="center"><b>Jumlah</b></td>
					<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
					<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
					<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
					<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
					<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
					<td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>
					<td align="right"><b>' . $jum15 . '</b></td>
				</tr>
			';
			}
			?>
		</tbody>
	</table>
</div> 

