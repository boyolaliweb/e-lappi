<?php
$default = mysqli_fetch_array(mysqli_query($db,"select * from t_bankeu_kab where id='" . $_GET['id'] . "' limit 1"));
?>
<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
    $(function() {
        $('#urusan').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_bidang.php';
            $.post(urls, {id: $('#urusan').val()},
            function(data) {
                $('#bidang').html(data);
                $('#program').html("");
                $('#kegiatan').html("");
            }
            );
        });
        $('#bidang').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_program.php';
            $.post(urls, {id: $('#bidang').val()},
            function(data) {
                $('#program').html(data);
            }
            );
        });
        $('#program').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_kegiatan.php';
            $.post(urls, {id: $('#program').val()},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Ubah Laporan Dana Bankeu Kepada Kab/Kota <small>HALAMAN UNTUK MENGELOLA LAPORAN BANKEU KEPADA KAB/KOTA</small></h1>
</div>
<div class="row-fluid">
	<!-- Back button -->
	<div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>
</div>
<form action="laporan-bankeu_kab-update" method="POST">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = mysqli_fetch_array($sqlSKPD)) {
                                    $select1 = ($rowSPKD['id'] == $default['id_skpd']) ? "selected" : "";
                                    echo '<option value="' . $rowSPKD['id'] . '" ' . $select1 . '>' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--
    <div class="row-form">
        <div class="span3">Urusan</div>
                            <div class="span9">
                                    <select name="urusan" id="urusan">
                                            <option>-- Pilih Urusan --</option>
                    <?php
                    $sqlUrusan = mysqli_query($db,"select distinct(urusan) from m_bidang");
                    while ($rowUrusan = mysqli_fetch_array($sqlUrusan)) {
                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['urusan'] . '</option>';
                    }
                    ?>
                                    </select>
                            </div>
    </div>                    
    <div class="row-form">
        <div class="span3">Bidang Kegiatan</div>
                            <div class="span9">
                                    <select name="bidang" id="bidang" style="width: 100%;" required="true">
                                            <option value="<?php echo $default['bidang']; ?>"><?php echo $default['bidang']; ?></option>
                                    </select>
                            </div>
    </div>
                    -->
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9">
                            <select name="program" id="program" class="select" style="width: 100%;" required="true">	
                                <option value="<?php echo $default['program']; ?>"><?php echo $default['program']; ?></option>
                                <?php
                                $sqlProgram = mysqli_query($db,"select distinct(program) from m_kegiatan");
                                while ($rowProgram = mysqli_fetch_array($sqlProgram)) {
                                    echo '<option value="' . $rowProgram['program'] . '">' . $rowProgram['program'] . '</option>';
                                }
                                ?>						
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9">
                            <select name="kegiatan" id="kegiatan" class="select" style="width: 100%;" required="true">
                                <option value="<?php echo $default['kegiatan']; ?>"><?php echo $default['kegiatan']; ?></option>
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="<?php echo $default['lokasi']; ?>" style="width:100%;">
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="anggaran_bankeu_kab" value="<?php echo $default['anggaran_bankeu_kab']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="anggaran_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="anggaran_pendamping" value="<?php echo $default['anggaran_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="anggaran_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="anggaran_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="panjar_bankeu_kab" value="<?php echo $default['panjar_bankeu_kab']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="panjar_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="panjar_pendamping" value="<?php echo $default['panjar_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="panjar_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="panjar_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="realisasi_bankeu_kab" value="<?php echo $default['realisasi_bankeu_kab']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="realisasi_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="realisasi_pendamping" value="<?php echo $default['realisasi_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="realisasi_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="realisasi_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Target Fisik</h2></div>
                    <div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" value="<?php echo $default['progres_target']; ?>" style="width:60px;" maxlength="3" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="<?php echo $default['progres_real']; ?>" style="width:60px;" maxlength="3" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                        </div>              
                    </div>

					<?php if(isset($_SESSION['level']) && $_SESSION['level'] != "skpd") { ?>
					<!-- kolom anggaran kas -->
					<div class="head"><hr><h2>Anggaran Kas</h2></div>
                    <div class="row-form">
                        <div class="span3">Anggaran Kas</div>
                        <div class="span9">
                            <input type="text" name="anggaran_kas" value="<?php echo $default['anggaran_kas']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
							&nbsp;&nbsp;
                            <span id="anggaran_kas" class="kanan"></span>
                        </div>              
                    </div>
					<?php }else{ ?>
						<input type="hidden" name="anggaran_kas" value="0">
					<?php } ?>
					
                    <div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9">
                            <input type="text" name="permasalahan" value="<?php echo $default['permasalahan']; ?>">
                        </div>              
                    </div>

                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
        bankeu_kab = (parseInt($('input[name="anggaran_bankeu_kab"]').val()));
        pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
        bankeu_kab = (parseInt($('input[name="panjar_bankeu_kab"]').val()));
        pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
        bankeu_kab = (parseInt($('input[name="realisasi_bankeu_kab"]').val()));
        pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }
    function detail() {
        $("#anggaran_bankeu_kab_1").html("Rp. " + koma($('input[name="anggaran_bankeu_kab"]').val()) + " ,-");
        $("#anggaran_pendamping_1").html("Rp. " + koma($('input[name="anggaran_pendamping"]').val()) + " ,-");
        $("#anggaran_jumlah_1").html("Rp. " + koma($('input[name="anggaran_jumlah"]').val()) + " ,-");

        $("#panjar_bankeu_kab_1").html("Rp. " + koma($('input[name="panjar_bankeu_kab"]').val()) + " ,-");
        $("#panjar_pendamping_1").html("Rp. " + koma($('input[name="panjar_pendamping"]').val()) + " ,-");
        $("#panjar_jumlah_1").html("Rp. " + koma($('input[name="panjar_jumlah"]').val()) + " ,-");

        $("#realisasi_bankeu_kab_1").html("Rp. " + koma($('input[name="realisasi_bankeu_kab"]').val()) + " ,-");
        $("#realisasi_pendamping_1").html("Rp. " + koma($('input[name="realisasi_pendamping"]').val()) + " ,-");
        $("#realisasi_jumlah_1").html("Rp. " + koma($('input[name="realisasi_jumlah"]').val()) + " ,-");
		$("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");
    }

    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);

</script>