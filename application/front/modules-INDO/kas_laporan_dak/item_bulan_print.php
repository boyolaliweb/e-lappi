<?php
include ("../../../../core/db.config.php");
$sqlStatus = $db->query("select status from m_entry where id='1' limit 1");
$sqlStatus->data_seek(0);
$sql_status = $sqlStatus->fetch_assoc();
$STATUS = $sql_status['status'];

function bulan($bulan) {
    switch ($bulan) {
        case 1:
            $bulan = "Januari";
            break;
        case 2:
            $bulan = "Februari";
            break;
        case 3:
            $bulan = "Maret";
            break;
        case 4:
            $bulan = "April";
            break;
        case 5:
            $bulan = "Mei";
            break;
        case 6:
            $bulan = "Juni";
            break;
        case 7:
            $bulan = "Juli";
            break;
        case 8:
            $bulan = "Agustus";
            break;
        case 9:
            $bulan = "September";
            break;
        case 10:
            $bulan = "Oktober";
            break;
        case 11:
            $bulan = "Nopember";
            break;
        case 12:
            $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$bulan = $id;
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
$kop = (isset($_POST['kop'])) ? "(" . $_POST['kop'] . ")" : "";
?>
<center>
    <h3>LAPORAN ANGGARAN KAS BERSUMBER DARI DANA ALOKASI KHUSUS (DAK)<BR>KABUPATEN BOYOLALI
        BULAN <?php echo strtoupper(bulan($id)); ?> TAHUN ANGGARAN <?php echo $tahun; ?><br> <?php echo $kop; ?> </h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>

            <th rowspan="2">No</th>
            <th rowspan="2">BIDANG/PROGRAM/KEGIATAN</th>
            <th colspan="3">ANGGARAN (Rp.)</th>
            <th rowspan="2">Anggaran Kas</th>
            <th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
            <th colspan="2">Progres Fisik</th>
            <th rowspan="2">KET.</th>

        </tr>
        <tr>
            <th>DAK</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>DAK</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>DAK</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>Target (%)</th>
            <th>Real (%)</th>
        </tr>
        <tr>

            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
            //$sql = $db->query("select distinct(bidang) from t_dak where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0' order by kode_bidang");
            $sql = $db->query("SELECT DISTINCT (a.bidang)
                            FROM t_dak a
                            INNER JOIN m_bidang b ON a.bidang = b.bidang where a.id_skpd='" . $skpd . "' and a.bulan='" . $id . "'
                            and a.tahun='" . $tahun . "' and status_update='0' order by b.kode_urusan asc,
                            b.kode_bidang asc");
        } else {
            //if ($per_skpd == "all") { //login as administrator
            //$sql = $db->query("select distinct(bidang) from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' order by kode_bidang");
            /*
             * $sql = $db->query("select * from t_dak a inner join m_bidang b on a.bidang=b.bidang"
              . "where a.bulan='" . $id . "' and a.tahun='" . $tahun . "'  and a.status_update='0' "
              . "order by b.kode_urusan asc, b.kode_bidang asc");
             * */
            $sql = $db->query("SELECT DISTINCT (a.bidang)
                            FROM t_dak a
                            INNER JOIN m_bidang b ON a.bidang = b.bidang where a.bulan='" . $id . "'
                            and a.tahun='" . $tahun . "' and status_update='0' order by b.kode_urusan asc,
                            b.kode_bidang asc");
            //} else {
            //$sql = $db->query("select distinct(bidang) from t_dak where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0' order by kode_bidang");
            //   $sql = $db->query("SELECT DISTINCT (a.bidang)
            //       FROM t_dak a
            //       INNER JOIN m_bidang b ON a.bidang = b.bidang where a.id_skpd='" . $per_skpd . "' and a.bulan='" . $id . "'
            //       and a.tahun='" . $tahun . "' and status_update='0' order by b.kode_urusan asc,
            //       b.kode_bidang asc");
            //}
        }
        $noBid = 1;
//make looping to get data
        while ($rowBid = mysqli_fetch_array($sql)) {
            echo '
                    <tr>

                        <td align="center"><b>' . romawi($noBid++) . '</b></td>
                        <td><b>' . $rowBid['bidang'] . '</b></td>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                ';
            if ($skpd > 0) {
                $sqlKegiatan = mysqli_query($db, "select * from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $skpd . "' and bulan='" . $bulan . "' and tahun='" . $tahun . "' and status_update='0'");
            } else {
                $sqlKegiatan = mysqli_query($db, "select * from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $bulan . "' and tahun='" . $tahun . "' and status_update='0'");
            }
            $no = 1;
            $jum3 = 0;
            $jum4 = 0;
            $jum5 = 0;
            $jum6 = 0;
            $jum7 = 0;
            $jum8 = 0;
            $jum9 = 0;
            $jum10 = 0;
            $jum11 = 0;
            $jum12 = 0;
            $jum13 = 0;
            $jum15 = 0; //anggaran_kas
            //progres real
            $real = 0;
            $target = 0;

            while ($row = mysqli_fetch_array($sqlKegiatan)) {
                $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                $jum3 = $jum3 + $row['anggaran_dak'];
                $jum4 = $jum4 + $row['anggaran_pendamping'];
                $jum5 = $jum5 + $row['anggaran_jumlah'];
                $jum6 = $jum6 + $row['panjar_dak'];
                $jum7 = $jum7 + $row['panjar_pendamping'];
                $jum8 = $jum8 + $row['panjar_jumlah'];
                $jum9 = $jum9 + $row['realisasi_dak'];
                $jum10 = $jum10 + $row['realisasi_pendamping'];
                $jum11 = $jum11 + $row['realisasi_jumlah'];
                $jum12 = $jum12 + $row['progres_target'];
                $jum13 = $jum13 + $row['progres_real'];
                $jum15 = $jum15 + $row['anggaran_kas']; //anggaran_kas

                $real = $real + 1;
                $target = $target + 1;


                $anggaran_dak = ($row['anggaran_dak'] == 0) ? ? "0" : number_format($row['anggaran_dak'], 0, ",", ".");
                $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? ? "0" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? ? "0" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                $panjar_dak = ($row['panjar_dak'] == 0) ? ? "0" : number_format($row['panjar_dak'], 0, ",", ".");
                $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? ? "0" : number_format($row['panjar_pendamping'], 0, ",", ".");
                $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? ? "0" : number_format($row['panjar_jumlah'], 0, ",", ".");
                $realisasi_dak = ($row['realisasi_dak'] == 0) ? ? "0" : number_format($row['realisasi_dak'], 0, ",", ".");
                $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? ? "0" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? ? "0" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                $progres_target = ($row['progres_target'] == 0) ? ? "0" : number_format($row['progres_target'], 2, ",", ".");
                $progres_real = ($row['progres_real'] == 0) ? ? "0" : number_format($row['progres_real'], 2, ",", ".");
                //di bawah untuk anggaran kas
                $anggaran_kas = ($row['anggaran_kas'] == 0) ? ? "0" : number_format($row['anggaran_kas'], 0, ",", ".");
                $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                echo '
    <tr>
            <td align="center">' . $no++ . '</td>
            <td>' . $row['kegiatan'] . $lokasi . '</td>
            <td align="right">' . $anggaran_dak . '</td>
            <td align="right">' . $anggaran_pendamping . '</td>
            <td align="right">' . $anggaran_jumlah . '</td>
                <td align="right">' . $anggaran_kas . '</td>
            <td align="right">' . $panjar_dak . '</td>
            <td align="right">' . $panjar_pendamping . '</td>
            <td align="right">' . $panjar_jumlah . '</td>
            <td align="right">' . $realisasi_dak . '</td>
            <td align="right">' . $realisasi_pendamping . '</td>
            <td align="right">' . $realisasi_jumlah . '</td>
            <td align="right">' . $progres_target . '</td>
            <td align="right">' . $progres_real . '</td>
            <td align="right">' . $masalah . '</td>
    </tr>
    ';
            }
            $jum3 = ($jum3 == 0) ? ? "0" : number_format($jum3, 0, ",", ".");
            $jum4 = ($jum4 == 0) ? ? "0" : number_format($jum4, 0, ",", ".");
            $jum5 = ($jum5 == 0) ? ? "0" : number_format($jum5, 0, ",", ".");
            $jum6 = ($jum6 == 0) ? ? "0" : number_format($jum6, 0, ",", ".");
            $jum7 = ($jum7 == 0) ? ? "0" : number_format($jum7, 0, ",", ".");
            $jum8 = ($jum8 == 0) ? ? "0" : number_format($jum8, 0, ",", ".");
            $jum9 = ($jum9 == 0) ? ? "0" : number_format($jum9, 0, ",", ".");
            $jum10 = ($jum10 == 0) ? ? "0" : number_format($jum10, 0, ",", ".");
            $jum11 = ($jum11 == 0) ? ? "0" : number_format($jum11, 0, ",", ".");
            $jum12 = ($jum12 == 0) ? ? "0" : number_format(($jum12 / $target), 2, ",", "."); //target
            $jum13 = ($jum13 == 0) ? ? "0" : number_format(($jum13 / $real), 2, ",", "."); //real
            $jum15 = ($jum15 == 0) ? ? "0" : number_format($jum15, 0, ",", ".");
            echo '
        <tr>
                <td>&nbsp;</td>
                <td align="center"><b>Jumlah</b></td>
                <td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
                <td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum15 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
                <td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
                <td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
                <td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
                <td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>


        </tr>
    ';
        }
        ?>
    </tbody>
</table>