<?php
$msg = "";
//Check to see if the form has been submitted
if (isset($_POST['submit'])):
    //Establish Post form variables
    $username = mysql_real_escape_string($_POST['username']);
    $password = mysql_real_escape_string(($_POST['password']));
    $npassword = mysql_real_escape_string(($_POST['npassword']));
    $rpassword = mysql_real_escape_string(($_POST['rpassword']));

    if ($_POST['tipe'] == "skpd") {
        // Query the database - To find which user we're working with
        $sql = "SELECT * FROM m_skpd WHERE username = '$username' ";
    } else {
        $sql = "SELECT * FROM m_user WHERE username = '$username' ";
    }
    $query = $db->query($sql);
    $numrows = $query->num_rows;

    //Gather database information
    while ($rows = $query->fetch_assoc()):
        $dbusername = $rows['username'];
        $dbpassword = $rows['password'];
    endwhile;

    //Validate The Form
    if (empty($username) || empty($password) || empty($npassword) || empty($rpassword)):
        $msg = "Silahkan lengkapi field";
    elseif ($numrows == 0):
        $msg = "Username tidak ditemukan";
    elseif ($password != $dbpassword):
        $msg = "Password yang anda masukkan salah";
    elseif ($npassword != $rpassword):
        $msg = "Password baru anda tidak cocok";
    elseif ($npassword == $password):
        $msg = "Password baru tidak boleh sama dengan password lama";
    else:

        if ($_POST['tipe'] == "skpd") {
            //$msg = "Your Password has been changed.";
            $db->query("UPDATE m_skpd SET password = '$npassword' WHERE username = '$username'");
            $msg = "Berhasil!";
        } else {
            //$msg = "Your Password has been changed.";
            $db->query("UPDATE m_user SET password = '$npassword' WHERE username = '$username'");
            $msg = "Berhasil!";
        }

    endif;
endif;
?>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Ubah Password <small>HALAMAN UNTUK MENGUBAH PASSWORD PENGGUNA</small></h1>
</div>
<div class="row-fluid">

    <div class="span6">                

        <div class="block">
            <div class="head">                                
                <h2>Ubah Password Pengguna</h2>
                <ul class="buttons">             
                    <li><a href="#" onclick="source('form_default');
                            return false;"><div class="icon"><span class="ico-info"></span></div></a></li>
                </ul>                                  
            </div>                                        
            <div class="data-fluid">
                <form action="" method="post">
                    <input type="hidden" name="tipe" value="<?php echo $_SESSION['level']; ?>">
                    <div class="row-form">
                        <div class="span3">Username:</div>
                        <div class="span9"><input type="text" name="username" value="<?php echo $_SESSION['username']; ?>"/></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Password:</div>
                        <div class="span9"><input type="password" name="password" value="" autocomplete="off"/></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Password Baru:</div>
                        <div class="span9"><input type="password" name="npassword" value=""/></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Ulang Password Baru:</div>
                        <div class="span9"><input type="password" name="rpassword" value=""/></div>
                    </div>
                    <div class="row-form">
                        <div class="span3"></div>
                        <div class="span9" align="right"><button id="submit" name="submit" class="btn btn-primary">Ubah Password</button></div>
                    </div>
                </form>
            </div>
        </div>
        <?php echo $msg; ?>
    </div>
</div>






