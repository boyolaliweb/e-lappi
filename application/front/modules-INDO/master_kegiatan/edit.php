<script type="text/javascript">
	/*function showKode(){
		urusan = document.getElementById("urusan").value;
		$.ajax({
		type: "POST",
		url: "application/front/modules/master_kegiatan/item_urusan.php",
		//url: "item_program.php",
		data: {id: urusan},
		success: function(response){
		$("#kode_urusan").val(response);
		},
		dataType:"html"
		});
		return false;
	}*/
	function showUrusan(){
		$.ajax({
		type: "POST",
		url: "application/front/modules/master_kegiatan/item_urusan.php",
		//url: "item_program.php",
		data: {id: true},
		success: function(response){
			//alert(response);
			$("#select_urusan").html(response);
		},
		dataType:"html"
		});
		return false;
	}
	function showBidang(){
		urusan = document.getElementById("urusan").value;
		$.ajax({
		type: "POST",
		url: "application/front/modules/master_kegiatan/item_bidang.php",
		//url: "item_program.php",
		data: {id: urusan},
		success: function(response){
			//alert(response);
			$("#bidang").html(response);
		},
		dataType:"html"
		});
		return false;
	}
	
	function showProgram(){
		bidang = document.getElementById("select_bidang").value;
		//alert(bidang);
		$.ajax({
		type: "POST",
		url: "application/front/modules/master_kegiatan/item_program.php",
		//url: "item_kegiatan.php",
		data: {id: bidang},
		success: function(response){
			$("#programe").html(response);
		},
		dataType:"html"
		});
		return false;
	}
	
	function showKode(){
		program = document.getElementById("select_program").value;
		//alert(bidang);
		$.ajax({
		type: "POST",
		url: "application/front/modules/master_kegiatan/item_kode.php",
		//url: "item_kegiatan.php",
		data: {id: program},
		success: function(response){
			//alert(response);
			$("#kode_kegiatan").val(response);
		},
		dataType:"html"
		});
		$("#kegiatan").focus();
		return false;
		
	}

</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Kegiatan <small>HALAMAN UNTUK MENGATUR DATA MASTER KEGIATAN</small></h1>  
</div>
<?php
if(isset($_GET['id'])){
	$sqlOpen = $db->query("select * from m_kegiatan where id='".$_GET['id']."' limit 1");
        $sqlOpen->data_seek(0);
	$default = $sqlOpen->fetch_assoc();
        
        //open detail
        $sql_urusan = $db->query("select urusan from m_urusan where kode='" . $default['kode_urusan'] . "' limit 1");
        $sql_urusan->data_seek(0);
        $get_urusan = $sql_urusan->fetch_assoc();
    
        $sql_bidang = $db->query("select bidang from m_bidang where kode_urusan='" . $default['kode_urusan'] . "' and kode_bidang='" . $default['kode_bidang'] . "' limit 1");
        $sql_bidang->data_seek(0);
        $get_bidang = $sql_bidang->fetch_assoc();

        $sql_program = $db->query("select program from m_program where kode_urusan='" . $default['kode_urusan'] . "' and kode_bidang='" . $default['kode_bidang'] . "' and kode_program='" . $default['kode_program'] . "' limit 1");
        $sql_program->data_seek(0);
        $get_program = $sql_program->fetch_assoc();
}else{
	redirect("master-kegiatan");
}
?>
<form action="" method="post">
<input type="hidden" name="id" value="<?php echo $default['id'];?>">
<div class="row-fluid">
    <div class="span8">                
        <div class="block">
            <div class="head">                                
                <h2>Ubah Data Kegiatan</h2>
            </div>              
            <div class="data-fluid">
				<div class="row-form">
                    <div class="span3">Urusan</div>
					<div class="span9" id="select_urusan"><b><?php echo $get_urusan['urusan'];?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="btn btn-warning" onClick="showUrusan();"><i class="ico-edit"></i> Ubah Urusan</span>
					</div>
                </div>
                <div class="row-form">
                    <div class="span3">Bidang</div>
					<div class="span9" id="bidang"><b><?php echo $get_bidang['bidang'];?></b></div>
                </div>
				<div class="row-form">
                    <div class="span3">Program</div>
					<div class="span9" id="programe"><b><?php echo $get_program['program'];?></b></div>
                </div>
                <div class="row-form">
                    <div class="span3">Kode Kegiatan</div>
                    <div class="span9"><input type="text" name="kode" required="true" id="kode_kegiatan" value="<?php echo $default['kode_urusan'].'.'.$default['kode_bidang'].'.'.$default['kode_program'].'.'.$default['kode_kegiatan'];?>"></div>
                </div>                    
                <div class="row-form">
                    <div class="span3">Kegiatan</div>
                    <div class="span9"><input type="text" name="kegiatan" required="true" id="kegiatan" value="<?php echo $default['kegiatan'];?>"></div>
                </div>
				
				<div class="row-form">
                    <div class="span12">
						<input type="submit" name="submit" value="Simpan" class="btn btn-large">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="btn btn-large" onClick="window.location.reload()"><i class="ico-edit"></i> Batal Ubah</span>
					</div>               
				</div>
			</div>  
		</div>
	</div>
</div>
</form>


<?php
//processing submit data
if(isset($_POST['submit'])){
	$kode = explode(".",$_POST['kode']);
	$kode_urusan = $kode[0];
	$kode_bidang = $kode[1];
	$kode_program = $kode[2];
	$kode_kegiatan = $kode[3];
        
        $sql_urusan = $db->query("select urusan from m_urusan where kode='" . $kode_urusan . "' limit 1");
        $sql_urusan->data_seek(0);
        $get_urusan = $sql_urusan->fetch_assoc();

        $sql_bidang = $db->query("select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1");
        $sql_bidang->data_seek(0);
        $get_bidang = $sql_bidang->fetch_assoc();

        $sql_program = $db->query("select program from m_program where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' limit 1");
        $sql_program->data_seek(0);
        $get_program = $sql_program->fetch_assoc();
        
	$sql = $db->query("update m_kegiatan set
                            kode_urusan = '".$kode_urusan."',
                            kode_bidang = '".$kode_bidang."',
                            kode_program = '".$kode_program."',
                            kode_kegiatan = '".$kode_kegiatan."',
                            bidang = '".$get_bidang['bidang']."',
                            program = '".$get_program['program']."',
                            kegiatan = '".$_POST['kegiatan']."'
                            where id ='".$_POST['id']."'
                    ");
	if($sql){
		alert_success();
		redirect("master-kegiatan");
	}
}
?>