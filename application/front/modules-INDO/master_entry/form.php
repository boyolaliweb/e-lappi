<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Status Entry <small>HALAMAN UNTUK MENGATUR DATA MASTER STATUS ENTRY</small></h1>  
</div>

<div class="row-fluid">
    <div class="span6">                
        <div class="block">
            <div class="head">                                
                <p>Silahkan Pilih untuk memperbolehkan atau tutup SKPD melakukan entry laporan</p>
            </div>              
            <div class="data-fluid">
                <div class="row-form">
                    <div class="span3">Status</div>
                    <div class="span9">
                        <div class="btn-group">
                            <button class="btn btn-info"><?php echo $STATUS; ?></button>
                            <button data-toggle="dropdown" class="btn btn-info dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?val=master-entry-action&id=open">Perbolehkan Entry Laporan</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?val=master-entry-action&id=close">Tutup Entry laporan</a></li>
                            </ul>
                        </div>
                    </div>
                </div>                    
            </div>  
        </div>
    </div>

</div>
