<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
    $(function() {
        $('#urusan').change(function(event) {
            urls = 'application/front/modules/laporan_apbd/item_bidang.php';
            $.post(urls, {id: $('#urusan').val()},
            function(data) {
                $('#bidang').html(data);
                $('#program').html("");
                $('#kegiatan').html("");
            }
            );
        });
        $('#bidang').change(function(event) {
            urls = 'application/front/modules/laporan_apbd/item_program.php';
            $.post(urls, {id: $('#bidang').val()},
            function(data) {
                $('#program').html(data);
            }
            );
        });
        $('#program').change(function(event) {
            urls = 'application/front/modules/laporan_apbd/item_kegiatan.php';
            $.post(urls, {id: $('#program').val()},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Laporan Pelaksanaan Kegiatan Belanja Langsung <small>HALAMAN UNTUK MENGELOLA LAPORAN APDB KABUPATEN</small></h1>
</div>
<div class="row-fluid">
	<!-- Back button -->
	<div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>
</div>
<form action="laporan-apbd-simpan" method="POST">
    <input type="hidden" name="bagian" value="">
    <input type="hidden" name="subbagian" value="">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = mysqli_fetch_array($sqlSKPD)) {
                                    echo '<option value="' . $rowSPKD['id'] . '">' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--
    <div class="row-form">
        <div class="span3">Urusan</div>
                            <div class="span9">
                                    <select name="urusan" id="urusan">
                                            <option>-- Pilih Urusan --</option>
                    <?php
                    $sqlUrusan = mysqli_query($db,"select distinct(urusan) from m_bidang");
                    while ($rowUrusan = mysqli_fetch_array($sqlUrusan)) {
                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['urusan'] . '</option>';
                    }
                    ?>
                                    </select>
                            </div>
    </div>                    
    <div class="row-form">
        <div class="span3">Bidang Kegiatan</div>
                            <div class="span9">
                                    <select name="bidang" id="bidang" style="width: 100%;" required="true">
                                    </select>
                            </div>
    </div>
                    -->
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9">
                            <select name="program" id="program" class="select" style="width: 100%;" required="true">		
                                <option>-- Pilih Program --</option>
                                <?php
                                $sqlProgram = mysqli_query($db,"select distinct(program) from m_kegiatan");
                                while ($rowProgram = mysqli_fetch_array($sqlProgram)) {
                                    echo '<option value="' . $rowProgram['program'] . '">' . $rowProgram['program'] . '</option>';
                                }
                                ?>						
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9">
                            <select name="kegiatan" id="kegiatan" class="select" style="width: 100%;" required="true">
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="" style="width:100%;">
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">Jumlah (Rp.)</div>
                        <div class="span9">
                            <input type="text" name="anggaran_apbd" value="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="anggaran_apbd_1" class="kanan"></span>
                        </div>              
                    </div>				
                    <div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
                    <div class="row-form">
                        <div class="span3">Jumlah (Rp.)</div>
                        <div class="span9">
                            <input type="text" name="panjar_apbd" value="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="panjar_apbd_1" class="kanan"></span>
                        </div>              
                    </div>				
                    <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                    <div class="row-form">
                        <div class="span3">Jumlah (Rp.)</div>
                        <div class="span9">
                            <input type="text" name="realisasi_apbd" value="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                            &nbsp;&nbsp;
                            <span id="realisasi_apbd_1" class="kanan"></span>
                        </div>              
                    </div>				
                    <div class="head"><hr><h2>Progres Fisik</h2></div>
                    <div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" value="" style="width:60px;" maxlength="3" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="" style="width:60px;" maxlength="3" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')">
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Keterangan & Permasalahan</h2></div>
                    <div class="row-form">
                        <div class="span3">Masalah</div>
                        <div class="span9">
                            <input type="text" name="permasalahan" value="">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9">
                            <input type="text" name="keterangan" value="">
                        </div>              
                    </div>
                    <hr>
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var apbd = 0;
        var pendamping = 0;
        var jumlah = 0;
        apbd = (parseInt($('input[name="anggaran_apbd"]').val()));
        pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        jumlah = apbd + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var apbd = 0;
        var pendamping = 0;
        var jumlah = 0;
        apbd = (parseInt($('input[name="panjar_apbd"]').val()));
        pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        jumlah = apbd + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var apbd = 0;
        var pendamping = 0;
        var jumlah = 0;
        apbd = (parseInt($('input[name="realisasi_apbd"]').val()));
        pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        jumlah = apbd + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }
     function detail() {
        $("#anggaran_apbd_1").html("Rp. " + koma($('input[name="anggaran_apbd"]').val()) + " ,-");
        $("#panjar_apbd_1").html("Rp. " + koma($('input[name="panjar_apbd"]').val()) + " ,-");
        $("#realisasi_apbd_1").html("Rp. " + koma($('input[name="realisasi_apbd"]').val()) + " ,-");
    }
    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);

</script>