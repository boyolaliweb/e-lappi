<?php if ((isset($_SESSION['level']) && $_SESSION['level'] == "skpd") && (isset($_SESSION['status_entry']) && $_SESSION['status_entry'] == "close")) exit('Entry data telah ditutup'); ?>
<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
    $(function() {
        $('#urusan').change(function(event) {
            urls = 'application/front/modules/laporan_dak/item_bidang.php';
            $.post(urls, {id: $('#urusan').val()},
            function(data) {
                $('#bidang').html(data);
                $('#program').html("");
                $('#kegiatan').html("");
            }
            );
        });
        $('#bidang').change(function(event) {
            bidang = $('#bidang').val();
            //alert(bidang);
            urls = 'application/front/modules/laporan_dak/item_program.php';
            $.post(urls, {id: bidang},
            function(data) {
                $('#program').html(data);
            }
            );
        });
        $('#program').change(function(event) {
            program = $('#select_program').val();
            //alert(program);
            urls = 'application/front/modules/laporan_dak/item_kegiatan.php';
            $.post(urls, {id: program},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
        $('#kegiatan').change(function(event) {
            $("#lokasi").focus();
        });

    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Anggaran Perubahan Dana Alokasi Khusus (DAK) <small>HALAMAN UNTUK MENGELOLA LAPORAN DAK</small></h1>  
</div>
<div class="row-fluid">
    <!-- Back button -->
    <!--<div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>-->
</div>
<form action="laporan-dak-simpan" method="POST" id="form">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Bidang / Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Bulan</div>
                        <div class="span9">
                            <select name="bulan">
                                <?php
                                $bulan = date("m");
                                ?>
                                <option value="01" <?php echo ($bulan == "01") ? "SELECTED" : ""; ?>>Januari</option>
                                <option value="02" <?php echo ($bulan == "02") ? "SELECTED" : ""; ?>>Februari</option>
                                <option value="03" <?php echo ($bulan == "03") ? "SELECTED" : ""; ?>>Maret</option>
                                <option value="04" <?php echo ($bulan == "04") ? "SELECTED" : ""; ?>>April</option>
                                <option value="05" <?php echo ($bulan == "05") ? "SELECTED" : ""; ?>>Mei</option>
                                <option value="06" <?php echo ($bulan == "06") ? "SELECTED" : ""; ?>>Juni</option>
                                <option value="07" <?php echo ($bulan == "07") ? "SELECTED" : ""; ?>>Juli</option>
                                <option value="08" <?php echo ($bulan == "08") ? "SELECTED" : ""; ?>>Agustus</option>
                                <option value="09" <?php echo ($bulan == "09") ? "SELECTED" : ""; ?>>September</option>
                                <option value="10" <?php echo ($bulan == "10") ? "SELECTED" : ""; ?>>Oktober</option>
                                <option value="11" <?php echo ($bulan == "11") ? "SELECTED" : ""; ?>>Nopember</option>
                                <option value="12" <?php echo ($bulan == "12") ? "SELECTED" : ""; ?>>Desember</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = $db->query("select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = $db->query("select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = $sqlSKPD->fetch_assoc()) {
                                    echo '<option value="' . $rowSPKD['id'] . '">' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--
                                    <div class="row-form">
                        <div class="span3">Urusan</div>
                                            <div class="span9">
                                                    <select name="urusan" id="urusan">
                                                            <option>-- Pilih Urusan --</option>
                    <?php
                    $sqlUrusan = $db->query("select * from m_bidang");
                    while ($rowUrusan = $sqlUrusan->fetch_assoc()) {
                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['kode_urusan'] . '.' . $rowUrusan['kode_bidang'] . ' &raquo; ' . $rowUrusan['urusan'] . '</option>';
                    }
                    ?>
                                                    </select>
                                            </div>
                    </div>                    
                    -->
                    <div class="row-form">
                        <div class="span3">Bidang Kegiatan</div>
                        <div class="span9">
                            <select name="bidang" id="bidang" style="width: 100%;" required="true" class="select">
                                <option>-- Pilih Bidang --</option>
                                <?php
                                $sqlBidang = $db->query("select * from m_bidang");
                                while ($rowBidang = $sqlBidang->fetch_assoc()) {
                                    echo '<option value="' . $rowBidang['kode_urusan'] . '_' . $rowBidang['kode_bidang'] . '_' . $rowBidang['bidang'] . '">' . $rowBidang['kode_urusan'] . '.' . $rowBidang['kode_bidang'] . ' &raquo; ' . $rowBidang['bidang'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9" id="program">
                            <!--<select name="program" id="program" style="width: 100%;" required="true">					
                            </select>-->
                            <!--<input type="text" name="program" style="width: 100%;" required="true" data-index="2">-->
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9" id="kegiatan">
                            <!--<select name="kegiatan" id="kegiatan" class="select" style="width: 100%;" required="true">
                            </select>-->
                            <!--<input type="text" name="kegiatan" style="width: 100%;" required="true" data-index="3">-->
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3"><input type="checkbox" name="SubKeg" value="Sub Kegiatan" id="subkegiatanbtn"> Sub Kegiatan</div>
                        <div class="span9" id="subkegiatan">
                            <input type="text" name="subkegiatan" style="width: 100%;" data-index="3">
                            <span> Subkegiatan boleh dikosongkan. Tuliskan detail subkegiatan di atas ini.</span>
                        </div>                 
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="" style="width:100%;" data-index="4" required="true" id="lokasi">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Volume</div>
                        <div class="span9">
                            <input type="text" name="volume" value="" style="width:50%;" data-index="5">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Satuan</div>
                        <div class="span9">
                            <select name="satuan"  style="width:50%;" >
                                <option value="">-- Pilih Satuan --</option>
                                <?php
                                $sql_satuan = $db->query("select * from m_satuan order by id asc");
                                while ($r_satuan = $sql_satuan->fetch_assoc()) {
                                    if ($r_satuan['keterangan'] == "-" || empty($r_satuan['keterangan'])) {
                                        $ket = "";
                                    } else {
                                        $ket = '(' . $r_satuan['keterangan'] . ')';
                                    }
                                    echo '<option value="' . $r_satuan['nama'] . '">' . $r_satuan['nama'] . ' ' . $ket . '</option>';
                                }
                                ?>
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Jumlah Penerima</div>
                        <div class="span9">
                            <input type="text" name="penerima" value="" style="width:50%;" data-index="6">
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">DAK</div>
                        <div class="span9">
                            <input id="angka" type="text" name="anggaran_dak" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="7">
                            &nbsp;&nbsp;
                            <span id="anggaran_dak_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input id="angka" type="text" name="anggaran_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="8">
                            &nbsp;&nbsp;
                            <span id="anggaran_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input id="angka" type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="anggaran_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran Kas</h2></div>
                    <div class="row-form">
                        <div class="span3">Anggaran Kas</div>
                        <div class="span9">
                            <input type="text" name="anggaran_kas" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="9">
                            &nbsp;&nbsp;
                            <span id="anggaran_kas" class="kanan"></span>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
                    <div class="row-form">
                        <div class="span3">DAK</div>
                        <div class="span9">
                            <input id="angka" type="text" name="panjar_dak" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="10">
                            &nbsp;&nbsp;
                            <span id="panjar_dak_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input id="angka" type="text" name="panjar_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="11">
                            &nbsp;&nbsp;
                            <span id="panjar_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input id="angka" type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="panjar_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="head"><hr><h2>PELAKSANAAN KEGIATAN</h2></div>
                    <div class="row-form">
                        <div class="span3">Swakelola</div>
                        <div class="span9">
                            <input id="angka" type="text" name="swakelola" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="12">
                            &nbsp;&nbsp;
                            <span id="swakelola" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kontrak</div>
                        <div class="span9">
                            <input id="angka" type="text" name="kontrak" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="13">
                            &nbsp;&nbsp;
                            <span id="kontrak" class="kanan"></span> 
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                    <div class="row-form">
                        <div class="span3">DAK</div>
                        <div class="span9">
                            <input id="angka" type="text" name="realisasi_dak" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="14">
                            &nbsp;&nbsp;
                            <span id="realisasi_dak_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input id="angka" type="text" name="realisasi_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="15">
                            &nbsp;&nbsp;
                            <span id="realisasi_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input id="angka" type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="realisasi_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Target Fisik</h2></div>
                    <div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" value="" maxlength="6" style="width:60px;" class="decimal" data-index="16">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="" maxlength="6" style="width:60px;"  class="decimal" data-index="17">
                        </div> 
                        <div class="span12">*) Gunakan titik untuk tanda pemisah koma</div>						
                    </div>
                    <div class="head"><hr><h2>Kesesuaian Sasaran dan Lokasi dengan RKPD</h2></div>
                    <div class="row-form">
                        <div class="span3">Sesuai</div>
                        <div class="span9">
                            <select name="sesuai_rkpd" style="width:80px" >
                                <option value=""></option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>              
                    </div>
                    <div class="head"><hr><h2>Kesesuaian Antara DPA SKPD dengan Juknis</h2></div>
                    <div class="row-form">
                        <div class="span3">Sesuai</div>
                        <div class="span9">
                            <select name="sesuai_juknis" style="width:80px" >
                                <option value=""></option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                        </div>              
                    </div>
                    <div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9">
                            <input type="text" name="permasalahan" data-index="18" value="Anggaran Perubahan" readonly="readonly">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kodefikasi Masalah</div>
                        <div class="span9">
                            <select name="kode_masalah"  style="width:100%;">
                                <option>-- Pilih Kodefikasi --</option>
                                <?php
                                $sql_kode = $db->query("select * from m_kode_masalah order by id asc");
                                while ($r_kode = $sql_kode->fetch_assoc()) {
                                    echo '<option value="' . $r_kode['id'] . '">' . $r_kode['id'] . '. ' . $r_kode['masalah'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>              
                    </div>
                    <hr>
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="reset" value="Reset" class="btn btn-large btn-success">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>

    </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;

        if (isNaN(parseInt($('input[name="anggaran_dak"]').val()))) {
            dak = 0;
        } else {
            dak = (parseInt($('input[name="anggaran_dak"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="anggaran_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        } //anggaran 2


        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;

        if (isNaN(parseInt($('input[name="panjar_dak"]').val()))) {
            dak = 0;
        } else {
            dak = (parseInt($('input[name="panjar_dak"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="panjar_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        } //anggaran 2

        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;

        if (isNaN(parseInt($('input[name="realisasi_dak"]').val()))) {
            dak = 0;
        } else {
            dak = (parseInt($('input[name="realisasi_dak"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="realisasi_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        } //anggaran 2

        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }

    //angka ribuan


    function detail() {
        $("#anggaran_dak_1").html("Rp. " + koma($('input[name="anggaran_dak"]').val()) + " ,-");
        $("#anggaran_pendamping_1").html("Rp. " + koma($('input[name="anggaran_pendamping"]').val()) + " ,-");
        $("#anggaran_jumlah_1").html("Rp. " + koma($('input[name="anggaran_jumlah"]').val()) + " ,-");

        $("#panjar_dak_1").html("Rp. " + koma($('input[name="panjar_dak"]').val()) + " ,-");
        $("#panjar_pendamping_1").html("Rp. " + koma($('input[name="panjar_pendamping"]').val()) + " ,-");
        $("#panjar_jumlah_1").html("Rp. " + koma($('input[name="panjar_jumlah"]').val()) + " ,-");

        $("#realisasi_dak_1").html("Rp. " + koma($('input[name="realisasi_dak"]').val()) + " ,-");
        $("#realisasi_pendamping_1").html("Rp. " + koma($('input[name="realisasi_pendamping"]').val()) + " ,-");
        $("#realisasi_jumlah_1").html("Rp. " + koma($('input[name="realisasi_jumlah"]').val()) + " ,-");
        $("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");
        $("#swakelola").html("Rp. " + koma($('input[name="swakelola"]').val()) + " ,-");
        $("#kontrak").html("Rp. " + koma($('input[name="kontrak"]').val()) + " ,-");
    }

    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);

    $('.decimal').keyup(function() {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9\.]/g, '');
            if (val.split('.').length > 2)
                val = val.replace(/\.+$/, "");
        }
        $(this).val(val);
    });

    $('#form').on('keydown', 'input', function(event) {
        if (event.which == 13) {
            event.preventDefault();
            var $this = $(event.target);
            var index = parseFloat($this.attr('data-index'));
            $('[data-index="' + (index + 1).toString() + '"]').focus();
        }
    });
</script>
