<?php
include ("../../../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
$per_skpd = $_POST['per_skpd'];
?>
<center>
    <h3>LAPORAN KEMAJUAN PER TRIWULAN DAK TAHUN <?php echo $tahun; ?>
        <br>TRIWULAN <?php echo romawi($id); ?></h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="3">NO</th>
            <th rowspan="3">JENIS KEGIATAN<br></th>
            <th colspan="6">PERENCANAAN KEGIATAN<br></th>
            <th colspan="2">PELAKSANAAN KEGIATAN<br></th>
            <th colspan="3">REALISASI</th>
            <th colspan="2">Kesesuaian Sasaran dan Lokasi dengan RKPD<br></th>
            <th colspan="2">Kesesuaian Antara DPA SKPD dengan Juknis<br></th>
            <th rowspan="3">Kodefikasi <br>Masalah<br></th>
        </tr>
        <tr>
            <th rowspan="2">Satuan</th>
            <th rowspan="2">Volume</th>
            <th rowspan="2">Jumlah Penerima Manfaat<br></th>
            <th colspan="3">Jumlah</th>
            <th rowspan="2">Swakelola (juta)<br></th>
            <th rowspan="2">Kontrak (Rp. Juta)<br></th>
            <th rowspan="2">Fisik (%)<br></th>
            <th colspan="2">Keuangan</th>
            <th rowspan="2">Ya</th>
            <th rowspan="2">Tidak</th>
            <th rowspan="2">Ya</th>
            <th rowspan="2">Tidak</th>
        </tr>
        <tr>
            <th>DAK<br>(Rp. Juta)<br></th>
            <th>Pendamping<br>(Rp. Juta)<br></th>
            <th>Total Anggaran<br>(Rp. Juta)<br></th>
            <th>(Rp. Juta)<br></th>
            <th>Keuangan<br>(%)<br></th>
        </tr>

        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12A</th>
            <th>12B</th>
            <th>13A</th>
            <th>13B</th>
            <th>14A</th>
            <th>14B</th>
            <th>15</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
            if ($id == 1) {
                $id = "03";
                $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            } else
            if ($id == 2) {
                $id = "06";
                $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            } else
            if ($id == 3) {
                $id = "09";
                $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            } else
            if ($id == 4) {
                $id = "12";
                $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            } else {
                $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='' and tahun='" . $tahun . "' and status_update='0'");
            }
        } else {
            if ($per_skpd == "all") { //login as administrator
                if ($id == 1) {
                    $id = "03";
                    $sql = $db->query("select * from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 2) {
                    $id = "06";
                    $sql = $db->query("select * from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 3) {
                    $id = "09";
                    $sql = $db->query("select * from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 4) {
                    $id = "12";
                    $sql = $db->query("select * from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else {
                    $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='' and tahun='" . $tahun . "' and status_update='0'");
                }
            } else {
                 if ($id == 1) {
                    $id = "03";
                    $sql = $db->query("select * from t_dak where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 2) {
                    $id = "06";
                    $sql = $db->query("select * from t_dak where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 3) {
                    $id = "09";
                    $sql = $db->query("select * from t_dak where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else
                if ($id == 4) {
                    $id = "12";
                    $sql = $db->query("select * from t_dak where id_skpd='" . $per_skpd . "' and bulan='" . $id . "' and tahun='" . $tahun . "'  and status_update='0'");
                } else {
                    $sql = $db->query("select * from t_dak where id_skpd='" . $skpd . "' and bulan='' and tahun='" . $tahun . "' and status_update='0'");
                }
            }
        }
//make looping to get data
        $no = 1;
        $jum3 = 0;
        $jum4 = 0;
        $jum5 = 0;
        $jum6 = 0;
        $jum7 = 0;
        $jum8 = 0;
        $jum9 = 0;
        $jum10 = 0;
        $jum11 = 0;
        $jum12 = 0;
        $jum13 = 0;
        $jum15 = 0;
        //start untuk triwulan
        $jum16 = 0;
        $jum17 = 0;
        $jum18 = 0;
        $jum19 = 0;
        $jum20 = 0;
        //progres real
        $real = 0;
        $target = 0;
        while ($row = $sql->fetch_assoc()) {
            $anggaran_dak = ($row['anggaran_dak'] == 0) ? "0" : number_format($row['anggaran_dak'], 0, ",", ".");
            $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "0" : number_format($row['anggaran_pendamping'], 0, ",", ".");
            $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "0" : number_format($row['anggaran_jumlah'], 0, ",", ".");
            $panjar_dak = ($row['panjar_dak'] == 0) ? "0" : number_format($row['panjar_dak'], 0, ",", ".");
            $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "0" : number_format($row['panjar_pendamping'], 0, ",", ".");
            $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "0" : number_format( $row['panjar_jumlah'], 0, ",", ".");
            $realisasi_dak = ($row['realisasi_dak'] == 0) ? "0" : number_format($row['realisasi_dak'], 0, ",", ".");
            $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "0" : number_format($row['realisasi_pendamping'], 0, ",", ".");
            $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "0" : number_format($row['realisasi_jumlah'], 0, ",", ".");
            $progres_target = ($row['progres_target'] == 0) ? "0" : number_format($row['progres_target'], 2, ",", ".");
            $progres_real = ($row['progres_real'] == 0) ? "0" : number_format($row['progres_real'], 2, ",", ".");
            $anggaran_kas = ($row['anggaran_kas'] == 0) ? "0" : number_format($row['anggaran_kas'], 0, ",", ".");
            //triwulan
            $satuan = ($row['satuan'] == "" || $row['satuan'] == 0) ? "0" : ($row['satuan']);
            $volume = ($row['volume'] == "" || $row['volume'] == 0) ? "0" : ( $row['volume']);
            $penerima = ($row['penerima']);
            $swakelola = ($row['swakelola'] == 0) ? "0" : number_format($row['swakelola'], 0, ",", ".");
            $kontrak = ( $row['kontrak'] == 0) ? "0" : number_format($row['kontrak'], 0, ",", ".");
            $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
            $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
            $subkegiatan = ($row['subkegiatan']) ? " " . $row['subkegiatan'] . "<br>" : "";
            if ($row['sesuai_rkpd'] == "") {
                $sesuai_rkpd_ya = "";
                $sesuai_rkpd_tidak = "";
            } else
            if ($row['sesuai_rkpd'] == "tidak") {
                $sesuai_rkpd_ya = "";
                $sesuai_rkpd_tidak = "Tidak";
            } else
            if ($row['sesuai_rkpd'] == "ya") {
                $sesuai_rkpd_ya = "Ya";
                $sesuai_rkpd_tidak = "";
            }
            //sesuai Juknis
            if ($row['sesuai_juknis'] == "") {
                $sesuai_juknis_ya = "";
                $sesuai_juknis_tidak = "";
            } else
            if ($row['sesuai_juknis'] == "tidak") {
                $sesuai_juknis_ya = "";
                $sesuai_juknis_tidak = "Tidak";
            } else
            if ($row['sesuai_juknis'] == "ya") {
                $sesuai_juknis_ya = "Ya";
                $sesuai_juknis_tidak = "";
            }
            if ($row['kode_masalah'] > 0) {
                $kode_masalah = $row['kode_masalah'];
            } else {
                $kode_masalah = "";
            }
            echo '
                <tr>
                        <td align="center">' . $no++ . '</td>
                        <td>' . $row['kegiatan'] . $subkegiatan . $lokasi . '</td>
                        <td align="right">' . $satuan . '</td>
                        <td align="right">' . $volume . '</td>
                        <td align="right">' . $penerima . '</td>
                        <td align="right">' . $anggaran_dak . '</td>
                        <td align="right">' . $anggaran_pendamping . '</td>
                        <td align="right">' . $anggaran_jumlah . '</td>
                        <td align="right">' . $swakelola . '</td>
                        <td align="right">' . $kontrak . '</td>
                        <td align="right">' . $progres_real . '</td>
                        <td align="right">' . $realisasi_jumlah . '</td>
                        <td align="right">&nbsp;</td>
                        <td align="center">' . $sesuai_rkpd_ya . '</td>
                        <td align="center">' . $sesuai_rkpd_tidak . '</td>
                        <td align="center">' . $sesuai_juknis_ya . '</td>
                        <td align="center">' . $sesuai_juknis_tidak . '</td>
                        <td align="center">' . $kode_masalah . '</td>							
                </tr>
                ';
            /*
              $jum3 = ($jum3 == 0) ? "0" : ($jum3);
              $jum4 = ($jum4 == 0) ? "0" : ($jum4);
              $jum5 = ($jum5 == 0) ? "0" : ($jum5);
              $jum6 = ($jum6 == 0) ? "0" : ($jum6);
              $jum7 = ($jum7 == 0) ? "0" : ($jum7);
              $jum8 = ($jum8 == 0) ? "0" : ($jum8);
              $jum9 = ($jum9 == 0) ? "0" : ($jum9);
              $jum10 = ($jum10 == 0) ? "0" : ($jum10);
              $jum11 = ($jum11 == 0) ? "0" : ($jum11);
              $jum12 = ($jum12 == 0) ? "0" : ($jum12);
              $jum13 = ($jum13 == 0) ? "0" : ($jum13);
              $jum15 = ($jum15 == 0) ? "0" : ($jum15);
              $jum16 = ($jum16 == 0) ? "0" : ($jum16);
              $jum17 = ($jum17 == 0) ? "0" : ($jum17);
              $jum18 = ($jum18 == 0) ? "0" : ($jum18);
             * 
             */
            $jum3 = $jum3 + $row['anggaran_dak'];
            $jum4 = $jum4 + $row['anggaran_pendamping'];
            $jum5 = $jum5 + $row['anggaran_jumlah'];
            $jum6 = $jum6 + $row['panjar_dak'];
            $jum7 = $jum7 + $row['panjar_pendamping'];
            $jum8 = $jum8 + $row['panjar_jumlah'];
            $jum9 = $jum9 + $row['realisasi_dak'];
            $jum10 = $jum10 + $row['realisasi_pendamping'];
            $jum11 = $jum11 + $row['realisasi_jumlah'];
            $jum12 = $jum12 + $row['progres_target'];
            $jum13 = $jum13 + $row['progres_real'];
            $jum15 = $jum15 + $row['anggaran_kas'];
            //triwulan
            $jum16 = $jum16 + $row['swakelola'];
            $jum17 = $jum17 + $row['kontrak'];
            $jum18 = $jum18 + $row['penerima'];

            if ($row['progres_real'] == '0.00' || $row['progres_real'] == '0' || $row['progres_real'] == '') {
                $real = $real;
            } else {
                $real = $real + 1;
            }
            if ($row['progres_target'] == '0.00' || $row['progres_target'] == '0' || $row['progres_target'] == '') {
                $target = $target;
            } else {
                $target = $target + 1;
            }
        }
        $jum3 = ($jum3 == 0) ? "0" : number_format($jum3,0, ",", ".");
        $jum4 = ($jum4 == 0) ? "0" : number_format($jum4,0, ",", ".");
        $jum5 = ($jum5 == 0) ? "0" : number_format($jum5,0, ",", ".");
        $jum6 = ($jum6 == 0) ? "0" : number_format($jum6,0, ",", ".");
        $jum7 = ($jum7 == 0) ? "0" : number_format($jum7,0, ",", ".");
        $jum8 = ($jum8 == 0) ? "0" : number_format($jum8,0, ",", ".");
        $jum9 = ($jum9 == 0) ? "0" : number_format($jum9,0, ",", ".");
        $jum10 = ($jum10 == 0) ? "0" : number_format($jum10,0, ",", ".");
        $jum11 = ($jum11 == 0) ? "0" : number_format($jum11,0, ",", ".");
        $jum12 = ($jum12 == 0) ? "0" : number_format($jum12,0, ",", ".");
        $jum16 = ($jum16 == 0) ? "0" : number_format($jum16,0, ",", ".");
        $jum17 = ($jum17 == 0) ? "0" : number_format($jum17,0, ",", ".");
        $jum18 = ($jum18 == 0) ? "0" : number_format($jum18,0, ",", ".");
        $jum13 = ($jum13 == 0) ? "0" : number_format(($jum13 / $real),2, ",", "."); //real
        echo '
            <tr>
                    <td>&nbsp;</td>
                    <td align="center"><b>Total</b></td>
                    <td align="right"><b>&nbsp;</b></td>
                    <td align="right"><b>&nbsp;</b></td>
                    <td align="right"><b></b></td>
                    <td align="right"><b>' . $jum3 . '</b></td>
                    <td align="right"><b>' . $jum4 . '</b></td>
                    <td align="right"><b>' . $jum5 . '</b></td>
                    <td align="right"><b>' . $jum16 . '</b></td>
                    <td align="right"><b>' . $jum17 . '</b></td>
                    <td align="right"><b>' . $jum13 . '</b></td>
                    <td align="right"><b>' . $jum11 . '</b></td>
                    <td align="right"><b>&nbsp;</b></td>
                    <td align="right"><b>&nbsp;</b></td>
                    <td align="right">&nbsp;</td>
                    <td align="right">&nbsp;</td>
                    <td align="right">&nbsp;</td>
                    <td align="right">&nbsp;</td>
            </tr>
            ';
        ?>
    </tbody>
</table>

<?php if ($skpd > 0) { ?>
    <p>&nbsp;</p>
    <p align="right">Boyolali, <?php echo date("d-m-Y"); ?>
        <br><br><br><br><br>
        <b>
            <?php
            $sql_kepala = $db->query("select kepala from m_skpd where id='" . $skpd . "' limit 1");
            $row_kepala = $sql_kepala->fetch_assoc();
            echo $row_kepala['kepala'];
            ?>
        </b>
    </p>
<?php } ?>
<p>
    <br>
    <b><i>Keterangan</i></b><br>
    Kodefikasi:
    <?php
    $sql_kode = $db->query("select * from m_kode_masalah order by id asc");
    echo '<ol>';
    while ($row_kode = $sql_kode->fetch_assoc()) {
        echo '<li> ' . $row_kode['masalah'] . '</li>';
    }
    echo '</ol>';
    ?>                                                                      
</p>
