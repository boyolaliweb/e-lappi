<?php
include ("../../../../core/db.config.php");

function bulan($bulan)
{
	switch($bulan){
		case 1: $bulan="Januari - Maret";
		break;
		case 2: $bulan= "April - Juni";
		break;
		case 3: $bulan= "Juli - September";
		break;
		case 4: $bulan= "Oktober - Desember";
		break;
	} 
	return $bulan;
}
function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h3>LAPORAN KEGIATAN YANG BERSUMBER DARI DANA TUGAS PEMBANTUAN (APBN)<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE <?php echo strtoupper(bulan($id));?> </h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">PROGRAM/KEGIATAN</th>
            <th colspan="3">ANGGARAN (Rp.)</th>
            <th colspan="3">PANJAR/SP2D/SPMU (Rp.)</th>
            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
            <th colspan="2">Progres Fisik</th>
            <th rowspan="2">KET.</th>
        </tr>
        <tr>
            <th>TP</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>TP</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>TP</th>
            <th>Dana Pendamping</th>
            <th>JUMLAH</th>
            <th>Target (%)</th>
            <th>Real (%)</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        if ($id == 1) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where id_skpd='" . $skpd . "' and bulan ='01' or bulan ='02' or bulan='03' and tahun='" . $tahun . "'");
            } else {
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where bulan ='01' or bulan ='02' or bulan='03' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 2) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where id_skpd='" . $skpd . "' and bulan ='04' or bulan ='05' or bulan='06' and tahun='" . $tahun . "'");
            } else {
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where bulan ='04' or bulan ='05' or bulan='06' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 3) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where id_skpd='" . $skpd . "' and bulan ='07' or bulan ='08' or bulan='09' and tahun='" . $tahun . "'");
            } else {
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where bulan ='07' or bulan ='08' or bulan='09' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 4) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where id_skpd='" . $skpd . "' and bulan ='10' or bulan ='11' or bulan='12' and tahun='" . $tahun . "'");
            } else {
                $sql = mysqli_query($db,"select distinct(id_skpd) from t_tp where bulan ='10' or bulan ='11' or bulan='12' and tahun='" . $tahun . "'");
            }
        }
        $noBid = 1;
        //make looping to get data
        while ($rowBid = mysqli_fetch_array($sql)) {
            $skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1"));
            echo '
							<tr>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $skpd['nama'] . '</b></td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
						';
            if ($id == 1) {
                $sql_program = mysqli_query($db,"select distinct(program) from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and bulan ='01' or bulan ='02' or bulan='03' and tahun='" . $tahun . "'");
            } else
            if ($id == 2) {
               $sql_program = mysqli_query($db,"select distinct(program) from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and bulan ='04' or bulan ='05' or bulan='06' and tahun='" . $tahun . "'");
            } else
            if ($id == 3) {
              $sql_program = mysqli_query($db,"select distinct(program) from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and bulan ='07' or bulan ='08' or bulan='09' and tahun='" . $tahun . "'");
            } else
            if ($id == 4) {
                $sql_program = mysqli_query($db,"select distinct(program) from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and bulan ='10' or bulan ='11' or bulan='12' and tahun='" . $tahun . "'");
            }
            $jum3 = 0;
            $jum4 = 0;
            $jum5 = 0;
            $jum6 = 0;
            $jum7 = 0;
            $jum8 = 0;
            $jum9 = 0;
            $jum10 = 0;
            $jum11 = 0;
            $jum12 = 0;
            $jum13 = 0;
            while ($row_program = mysqli_fetch_array($sql_program)) {
                echo '
                                    <tr>
                                            <td>&nbsp;</td>
                                            <td><b>' . $row_program['program'] . '</b></td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                    </tr>
                                ';
                if (isset($_SESSION['id_skpd'])) {
                    $sqlKegiatan = mysqli_query($db,"select * from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and tahun='" . $tahun . "'");
                } else {
                    $sqlKegiatan = mysqli_query($db,"select * from t_tp where id_skpd='" . $rowBid['id_skpd'] . "' and tahun='" . $tahun . "'");
                }
                $no = 1;
                while ($row = mysqli_fetch_array($sqlKegiatan)) {
                    $jum3 = $jum3 + $row['anggaran_tp'];
                    $jum4 = $jum4 + $row['anggaran_pendamping'];
                    $jum5 = $jum5 + $row['anggaran_jumlah'];
                    $jum6 = $jum6 + $row['panjar_tp'];
                    $jum7 = $jum7 + $row['panjar_pendamping'];
                    $jum8 = $jum8 + $row['panjar_jumlah'];
                    $jum9 = $jum9 + $row['realisasi_tp'];
                    $jum10 = $jum10 + $row['realisasi_pendamping'];
                    $jum11 = $jum11 + $row['realisasi_jumlah'];
                    $jum12 = $jum12 + $row['progres_target'];
                    $jum13 = $jum13 + $row['progres_real'];
                    $anggaran_tp = ($row['anggaran_tp'] == 0) ? "-" : number_format($row['anggaran_tp'], 0, ",", ".");
                    $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                    $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                    $panjar_tp = ($row['panjar_tp'] == 0) ? "-" : number_format($row['panjar_tp'], 0, ",", ".");
                    $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                    $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                    $realisasi_tp = ($row['realisasi_tp'] == 0) ? "-" : number_format($row['realisasi_tp'], 0, ",", ".");
                    $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                    $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                    $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
                    $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                    $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                    echo '
								<tr>
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_tp . '</td>
									<td align="right">' . $anggaran_pendamping . '</td>
									<td align="right">' . $anggaran_jumlah . '</td>
									<td align="right">' . $panjar_tp . '</td>
									<td align="right">' . $panjar_pendamping . '</td>
									<td align="right">' . $panjar_jumlah . '</td>
									<td align="right">' . $realisasi_tp . '</td>
									<td align="right">' . $realisasi_pendamping . '</td>
									<td align="right">' . $realisasi_jumlah . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td>' . $masalah . '</td>							
								</tr>
								';
                }
            }

            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
            echo '
							<tr>
								<td>&nbsp;</td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
								<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
								<td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>
							</tr>
						';
        }
        ?>
    </tbody>
</table>


