<?php if ((isset($_SESSION['level'])&&$_SESSION['level']=="skpd") && (isset($_SESSION['status_entry'])&&$_SESSION['status_entry']=="close")) exit('Entry data telah ditutup'); ?>
<?php
$default = mysqli_fetch_array(mysqli_query($db,"select * from t_tp where id='" . $_GET['id'] . "' limit 1"));
?>
<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
	function showUrusan(){
		$.ajax({
		type: "POST",
		url: "application/front/modules/laporan_tp/item_program.php",
		//url: "item_program.php",
		data: {id: true},
		success: function(response){
			//alert(response);
			$("#program").html(response);
		},
		dataType:"html"
		});
		return false;
	}
    $(function() {
		$('#program').change(function(event) {
		program = $('#select_program').val();
		//alert("Udah");
		urls = 'application/front/modules/laporan_tp/item_kegiatan.php';
		$.post(urls, {id: program},
		function(data) {
			$('#kegiatan').html(data);
		}
		);
		});
	});
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Ubah Laporan Dana Tugas Pembantuan (TP) <small>HALAMAN UNTUK MENGELOLA LAPORAN TP</small></h1>  
</div>
<div class="row-fluid">
    <!-- Back button -->
    <div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>
</div>
<form action="laporan-tp-update" method="POST" id="form">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Bulan</div>
                        <div class="span9">
                            <input type="hidden" name="kode" value="<?php echo $default['kode']; ?>">
                            <input type="hidden" name="bulan" value="<?php echo $default['bulan']; ?>">
                            <input type="text" name="month" required="true" readonly="readonly" value="<?php echo bulan($default['bulan']); ?>">
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = mysqli_fetch_array($sqlSKPD)) {
                                    $select1 = ($rowSPKD['id'] == $default['id_skpd']) ? "selected" : "";
                                    echo '<option value="' . $rowSPKD['id'] . '" ' . $select1 . '>' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--
    <div class="row-form">
        <div class="span3">Urusan</div>
                            <div class="span9">
                                    <select name="urusan" id="urusan">
                                            <option>-- Pilih Urusan --</option>
                    <?php
                    $sqlUrusan = mysqli_query($db,"select distinct(urusan) from m_bidang");
                    while ($rowUrusan = mysqli_fetch_array($sqlUrusan)) {
                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['urusan'] . '</option>';
                    }
                    ?>
                                    </select>
                            </div>
    </div>                    
    <div class="row-form">
        <div class="span3">Bidang Kegiatan</div>
                            <div class="span9">
                                    <select name="bidang" id="bidang" style="width: 100%;" required="true">
                                            <option value="<?php echo $default['bidang']; ?>"><?php echo $default['bidang']; ?></option>
                                    </select>
                            </div>
    </div>
                    -->
                     <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9" id="program">
                            <?php echo $default['program'];?>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="btn btn-warning" onClick="showUrusan();"><i class="ico-edit"></i> Ubah Program</span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9" id="kegiatan">
                            <?php echo $default['kegiatan']; ?>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Sub Kegiatan</div>
                        <div class="span9">
                           <input type="text" name="subkegiatan" style="width: 100%;" data-index="3" value="<?php echo $default['subkegiatan']; ?>">
                            <span> Subkegiatan boleh dikosongkan. Tuliskan detail subkegiatan di atas ini.</span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="<?php echo $default['lokasi']; ?>" style="width:100%;" id="lokasi" data-index="1">
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">TP</div>
                        <div class="span9">
                            <input type="text" name="anggaran_tp" value="<?php echo $default['anggaran_tp']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="2">
                            &nbsp;&nbsp;
                            <span id="anggaran_tp_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="anggaran_pendamping" value="<?php echo $default['anggaran_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="3">
                            &nbsp;&nbsp;
                            <span id="anggaran_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="anggaran_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran Kas</h2></div>
                    <div class="row-form">
                        <div class="span3">Anggaran Kas</div>
                        <div class="span9">
                            <input type="text" name="anggaran_kas" value="<?php echo $default['anggaran_kas']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="4">
                            &nbsp;&nbsp;
                            <span id="anggaran_kas" class="kanan"></span>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
                    <div class="row-form">
                        <div class="span3">TP</div>
                        <div class="span9">
                            <input type="text" name="panjar_tp" value="<?php echo $default['panjar_tp']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="5">
                            &nbsp;&nbsp;
                            <span id="panjar_tp_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="panjar_pendamping" value="<?php echo $default['panjar_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="6">
                            &nbsp;&nbsp;
                            <span id="panjar_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="panjar_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                    <div class="row-form">
                        <div class="span3">TP</div>
                        <div class="span9">
                            <input type="text" name="realisasi_tp" value="<?php echo $default['realisasi_tp']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="7">
                            &nbsp;&nbsp;
                            <span id="realisasi_tp_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="realisasi_pendamping" value="<?php echo $default['realisasi_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="8">
                            &nbsp;&nbsp;
                            <span id="realisasi_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="realisasi_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Target Fisik</h2></div>
					<div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" maxlength="6" style="width:60px;" class="decimal" value="<?php echo $default['progres_target']; ?>" data-index="9">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="<?php echo $default['progres_real']; ?>" maxlength="6" style="width:60px;" class="decimal" data-index="10">
                        </div> 
						<div class="span12">*) Gunakan titik untuk tanda pemisah koma</div>	
                    </div>
					
                    <div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9">
                            <input type="text" name="permasalahan" value="<?php echo $default['permasalahan']; ?>" data-index="11">
                        </div>              
                    </div>

                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="btn btn-large" onClick="window.location.reload()"><i class="ico-edit"></i> Batal Ubah</span>
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var tp = 0;
        var pendamping = 0;
        var jumlah = 0;
        tp = (parseInt($('input[name="anggaran_tp"]').val()));
        pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        jumlah = tp + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var tp = 0;
        var pendamping = 0;
        var jumlah = 0;
        tp = (parseInt($('input[name="panjar_tp"]').val()));
        pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        jumlah = tp + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var tp = 0;
        var pendamping = 0;
        var jumlah = 0;
        tp = (parseInt($('input[name="realisasi_tp"]').val()));
        pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        jumlah = tp + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }

    function detail() {
        $("#anggaran_tp_1").html("Rp. " + koma($('input[name="anggaran_tp"]').val()) + " ,-");
        $("#anggaran_pendamping_1").html("Rp. " + koma($('input[name="anggaran_pendamping"]').val()) + " ,-");
        $("#anggaran_jumlah_1").html("Rp. " + koma($('input[name="anggaran_jumlah"]').val()) + " ,-");

        $("#panjar_tp_1").html("Rp. " + koma($('input[name="panjar_tp"]').val()) + " ,-");
        $("#panjar_pendamping_1").html("Rp. " + koma($('input[name="panjar_pendamping"]').val()) + " ,-");
        $("#panjar_jumlah_1").html("Rp. " + koma($('input[name="panjar_jumlah"]').val()) + " ,-");

        $("#realisasi_tp_1").html("Rp. " + koma($('input[name="realisasi_tp"]').val()) + " ,-");
        $("#realisasi_pendamping_1").html("Rp. " + koma($('input[name="realisasi_pendamping"]').val()) + " ,-");
        $("#realisasi_jumlah_1").html("Rp. " + koma($('input[name="realisasi_jumlah"]').val()) + " ,-");
        $("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");
    }
    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);
	$('.decimal').keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			 val = val.replace(/[^0-9\.]/g,'');
			 if(val.split('.').length>2) 
				 val =val.replace(/\.+$/,"");
		}
		$(this).val(val); 
	});
	$('#form').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        $('[data-index="' + (index + 1).toString() + '"]').focus();
    }
	});
</script>