<script type="text/javascript">
    /*function showKode(){
     urusan = document.getElementById("urusan").value;
     $.ajax({
     type: "POST",
     url: "application/front/modules/master_program/item_urusan.php",
     //url: "item_program.php",
     data: {id: urusan},
     success: function(response){
     $("#kode_urusan").val(response);
     },
     dataType:"html"
     });
     return false;
     }*/
    function showBidang() {
        urusan = document.getElementById("urusan").value;
        $.ajax({
            type: "POST",
            url: "application/front/modules/master_program/item_bidang.php",
            //url: "item_program.php",
            data: {id: urusan},
            success: function(response) {
                //alert(response);
                $("#bidang").html(response);
            },
            dataType: "html"
        });
        return false;
    }

    function showKode() {
        bidang = document.getElementById("select_bidang").value;
        //alert(bidang);
        $.ajax({
            type: "POST",
            url: "application/front/modules/master_program/item_kode.php",
            //url: "item_program.php",
            data: {id: bidang},
            success: function(response) {
                //alert(response);
                $("#kode_program").val(response);
            },
            dataType: "html"
        });
        $("#program").focus();
        return false;

    }
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Program <small>HALAMAN UNTUK MENGATUR DATA MASTER PROGRAM</small></h1>  
</div>
<form action="" method="post">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Tambah Data Program</h2>
                </div>              
                <div class="data-fluid">
                    <div class="row-form">
                        <div class="span3">Urusan</div>
                        <div class="span9">
                            <select name="urusan" id="urusan" onChange="showBidang()">
                                <option>--pilih urusan--</option>
                                <?php
                                $sqlUrusan = $db->query("select * from m_urusan");
                                $sqlUrusan->data_seek(0);
                                while ($rowUrusan = $sqlUrusan->fetch_assoc()) {
                                    echo '<option value="' . $rowUrusan['kode'] . '">' . $rowUrusan['kode'] . ' &raquo; ' . $rowUrusan['urusan'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Bidang</div>
                        <div class="span9" id="bidang"></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Kode Program</div>
                        <div class="span9"><input type="text" name="kode" required="true" id="kode_program"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9"><input type="text" name="program" required="true" id="program"></div>
                    </div>

                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>


<?php
//processing submit data
if (isset($_POST['submit'])) {
    $kode = explode(".", $_POST['kode']);
    $kode_urusan = $kode[0];
    $kode_bidang = $kode[1];
    $kode_program = $kode[2];
    
    $sql_urusan = $db->query("select urusan from m_urusan where kode='" . $kode_urusan . "' limit 1");
    $sql_urusan->data_seek(0);
    $get_urusan = $sql_urusan->fetch_assoc();

    $sql_bidang = $db->query("select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1");
    $sql_bidang->data_seek(0);
    $get_bidang = $sql_bidang->fetch_assoc();
    
    $sql = $db->query("insert into m_program set
                        kode_urusan = '" . $kode_urusan . "',
                        kode_bidang = '" . $kode_bidang . "',
                        kode_program = '" . $kode_program . "',
                        urusan = '" . $get_urusan['urusan'] . "',
                        bidang = '" . $get_bidang['bidang'] . "',
                        program = '" . $_POST['program'] . "'
                ");
    if ($sql) {
        alert_success();
        redirect("master-program");
    }
}
?>
