<?php if ((isset($_SESSION['level'])&&$_SESSION['level']=="skpd") && (isset($_SESSION['status_entry'])&&$_SESSION['status_entry']=="close")) exit('Entry data telah ditutup'); ?>
<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
    $(function() {
        $('#urusan').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_bidang.php';
            $.post(urls, {id: $('#urusan').val()},
            function(data) {
                $('#bidang').html(data);
                $('#program').html("");
                $('#kegiatan').html("");
            }
            );
        });
        $('#bidang').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_program.php';
            $.post(urls, {id: $('#bidang').val()},
            function(data) {
                $('#program').html(data);
            }
            );
        });
        $('#program').change(function(event) {
            urls = 'application/front/modules/laporan_bankeu_kab/item_kegiatan.php';
            $.post(urls, {id: $('#program').val()},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
		$('#kegiatan').change(function(event) {
			$("#lokasi").focus();
		 });
    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Laporan Dana Bankeu Kepada Kab/Kota <small>HALAMAN UNTUK MENGELOLA LAPORAN BANKEU KEPADA KAB/KOTA</small></h1> 
</div>
<div class="row-fluid">
    <!-- Back button -->
    <!--<div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>-->
</div>
<form action="laporan-bankeu_kab-simpan" method="POST">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Bulan</div>
                        <div class="span9">
                            <select name="bulan">
                                <?php
                                $bulan = date("m");
                                ?>
                                <option value="01" <?php echo ($bulan == "01") ? "SELECTED" : ""; ?>>Januari</option>
                                <!--<option value="02" <?php echo ($bulan == "02") ? "SELECTED" : ""; ?>>Februari</option>
                                <option value="03" <?php echo ($bulan == "03") ? "SELECTED" : ""; ?>>Maret</option>
                                <option value="04" <?php echo ($bulan == "04") ? "SELECTED" : ""; ?>>April</option>
                                <option value="05" <?php echo ($bulan == "05") ? "SELECTED" : ""; ?>>Mei</option>
                                <option value="06" <?php echo ($bulan == "06") ? "SELECTED" : ""; ?>>Juni</option>
                                <option value="07" <?php echo ($bulan == "07") ? "SELECTED" : ""; ?>>Juli</option>
                                <option value="08" <?php echo ($bulan == "08") ? "SELECTED" : ""; ?>>Agustus</option>
                                <option value="09" <?php echo ($bulan == "09") ? "SELECTED" : ""; ?>>September</option>
                                <option value="10" <?php echo ($bulan == "10") ? "SELECTED" : ""; ?>>Oktober</option>
                                <option value="11" <?php echo ($bulan == "11") ? "SELECTED" : ""; ?>>Nopember</option>
                                <option value="12" <?php echo ($bulan == "12") ? "SELECTED" : ""; ?>>Desember</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = mysqli_query($db,"select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = mysqli_fetch_array($sqlSKPD)) {
                                    echo '<option value="' . $rowSPKD['id'] . '">' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--
    <div class="row-form">
        <div class="span3">Urusan</div>
                            <div class="span9">
                                    <select name="urusan" id="urusan">
                                            <option>-- Pilih Urusan --</option>
                    <?php
                    $sqlUrusan = mysqli_query($db,"select distinct(urusan) from m_bidang");
                    while ($rowUrusan = mysqli_fetch_array($sqlUrusan)) {
                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['urusan'] . '</option>';
                    }
                    ?>
                                    </select>
                            </div>
    </div>                    
    <div class="row-form">
        <div class="span3">Bidang Kegiatan</div>
                            <div class="span9">
                                    <select name="bidang" id="bidang" style="width: 100%;" required="true">
                                    </select>
                            </div>
    </div>
                    -->
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9">
                           <select name="program" id="program" class="select" style="width: 100%;" required="true">
                                <option>-- Pilih Program --</option>
                            <?php
                            $sqlProgram = mysqli_query($db,"select * from m_program order by kode_urusan asc");
                            while ($rowProgram = mysqli_fetch_array($sqlProgram)) {
                                echo '<option value="'.$rowProgram['kode_urusan'].'_'.$rowProgram['kode_bidang'].'_'.$rowProgram['kode_program'].'_'.$rowProgram['program'].'">'.$rowProgram['kode_urusan'].'.'.$rowProgram['kode_bidang'].'.'.$rowProgram['kode_program'].' &raquo; '.$rowProgram['program'].'</option>';
                            }
                            ?>						
                            </select>
							<!-- <input type="text" name="program" value="" style="width:100%;"> -->
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9" id="kegiatan"></div>              
                    </div>
                    <div class="row-form">
                       <div class="span3"><input type="checkbox" name="SubKeg" value="Sub Kegiatan" id="subkegiatanbtn"> Sub Kegiatan</div>
                        <div class="span9" id="subkegiatan">
                            <input type="text" name="subkegiatan" style="width: 100%;" data-index="3">
                            <span> Subkegiatan boleh dikosongkan. Tuliskan detail subkegiatan di atas ini.</span>
                        </div>            
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="" style="width:100%;" id="lokasi" data-index="1">
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="anggaran_bankeu_kab" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="2">
                            &nbsp;&nbsp;
                            <span id="anggaran_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="anggaran_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="3">
                            &nbsp;&nbsp;
                            <span id="anggaran_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="anggaran_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <!-- kolom anggaran kas -->
                    <div class="head"><h2>Anggaran Kas</h2></div>
                    <div class="row-form">
                        <div class="span3">Anggaran Kas</div>
                        <div class="span9">
                            <input type="text" name="anggaran_kas" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="3">
                            &nbsp;&nbsp;
                            <span id="anggaran_kas" class="kanan"></span>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="panjar_bankeu_kab" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="4">
                            &nbsp;&nbsp;
                            <span id="panjar_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="panjar_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="5">
                            &nbsp;&nbsp;
                            <span id="panjar_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="panjar_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                    <div class="row-form">
                        <div class="span3">BANKEU KAB</div>
                        <div class="span9">
                            <input type="text" name="realisasi_bankeu_kab" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="6">
                            &nbsp;&nbsp;
                            <span id="realisasi_bankeu_kab_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="realisasi_pendamping" placeholder="0" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="7">
                            &nbsp;&nbsp;
                            <span id="realisasi_pendamping_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="realisasi_jumlah_1" class="kanan"></span>
                        </div>              
                    </div>

                    <div class="head"><hr><h2>Target Fisik</h2></div>
                     <div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" value="" maxlength="6" style="width:60px;" class="decimal" data-index="8">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="" maxlength="6" style="width:60px;"  class="decimal" data-index="9">
                        </div>              
						<div class="span12">*) Gunakan titik untuk tanda pemisah koma</div>	
                    </div>

                    <div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9">
                            <input type="text" name="permasalahan" value="" data-index="10">
                        </div>              
                    </div>
                    <hr>
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="reset" value="Reset" class="btn btn-large btn-success">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
      
        if (isNaN(parseInt($('input[name="anggaran_bankeu_kab"]').val()))) {
            bankeu_kab = 0;
        } else {
            bankeu_kab = (parseInt($('input[name="anggaran_bankeu_kab"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="anggaran_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        } //anggaran 2
        
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
       
        if (isNaN(parseInt($('input[name="panjar_bankeu_kab"]').val()))) {
            bankeu_kab = 0;
        } else {
            bankeu_kab = (parseInt($('input[name="panjar_bankeu_kab"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="panjar_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        } //anggaran 2
        
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var bankeu_kab = 0;
        var pendamping = 0;
        var jumlah = 0;
       
        if (isNaN(parseInt($('input[name="realisasi_bankeu_kab"]').val()))) {
            bankeu_kab = 0;
        } else {
            bankeu_kab = (parseInt($('input[name="realisasi_bankeu_kab"]').val()));
        } //anggaran 1

        if (isNaN(parseInt($('input[name="realisasi_pendamping"]').val()))) {
            pendamping = 0;
        } else {
            pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        } //anggaran 2
        
        jumlah = bankeu_kab + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }

    function detail() {
        $("#anggaran_bankeu_kab_1").html("Rp. " + koma($('input[name="anggaran_bankeu_kab"]').val()) + " ,-");
        $("#anggaran_pendamping_1").html("Rp. " + koma($('input[name="anggaran_pendamping"]').val()) + " ,-");
        $("#anggaran_jumlah_1").html("Rp. " + koma($('input[name="anggaran_jumlah"]').val()) + " ,-");

        $("#panjar_bankeu_kab_1").html("Rp. " + koma($('input[name="panjar_bankeu_kab"]').val()) + " ,-");
        $("#panjar_pendamping_1").html("Rp. " + koma($('input[name="panjar_pendamping"]').val()) + " ,-");
        $("#panjar_jumlah_1").html("Rp. " + koma($('input[name="panjar_jumlah"]').val()) + " ,-");

        $("#realisasi_bankeu_kab_1").html("Rp. " + koma($('input[name="realisasi_bankeu_kab"]').val()) + " ,-");
        $("#realisasi_pendamping_1").html("Rp. " + koma($('input[name="realisasi_pendamping"]').val()) + " ,-");
        $("#realisasi_jumlah_1").html("Rp. " + koma($('input[name="realisasi_jumlah"]').val()) + " ,-");
        $("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");
    }

    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);
	$('.decimal').keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			 val = val.replace(/[^0-9\.]/g,'');
			 if(val.split('.').length>2) 
				 val =val.replace(/\.+$/,"");
		}
		$(this).val(val); 
	});
	$('#form').on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-index'));
        $('[data-index="' + (index + 1).toString() + '"]').focus();
    }
	});
</script>