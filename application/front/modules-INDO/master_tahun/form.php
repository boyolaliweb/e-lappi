<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Tahun Anggaran <small>HALAMAN UNTUK MENGATUR DATA MASTER TAHUN ANGGARAN</small></h1>  
</div>
<form action="" method="post">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Ubah Data Tahun Anggaran</h2>
                </div>              
                <div class="data-fluid">
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>

    </div>
</form>
<?php
//processing submit data
if (isset($_POST['submit'])) {
    $sql = $db->query("insert into m_tahun_anggaran set
                        tahun = '" . $_POST['tahun'] . "',
                        status = '1'
                ");
    if ($sql) {
        alert_success();
        redirect("master-tahun");
    }
}
?>