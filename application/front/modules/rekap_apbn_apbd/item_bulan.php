<?php
include ("../../../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h5>REKAPITULASI DANA APBN DAN APBD PROVINSI<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE <?php echo strtoupper(bulan($id)); ?> </h5>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">SUMBER DANA</th>
            <th rowspan="2">JML KEG</th>
            <th rowspan="2">ANGGARAN (Rp.)</th>
            <th rowspan="2">PENDAMPING/<br>SHARING APBD II</th>
            <th rowspan="2">JUMLAH</th>
            <th colspan="4">SPJ</th>
            <th colspan="2">PROGRES FISIK</th>
            <th rowspan="2">KET</th>
        </tr>
        <tr>
            <th>MURNI</th>
            <th>PENDAMPING</th>
            <th>JUMLAH</th>
            <th>%</th>
            <th>TARGET</th>
            <th>REALISASI</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        //hitung jumlah kegiatan
        $q_hitung_dak = mysqli_query($db, "select id from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_dak = $q_hitung_dak->num_rows;

        $q_hitung_dbhct = mysqli_query($db, "select id from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_dbhcht = $q_hitung_dbhct->num_rows;

        $q_hitung_bankeu_kab = mysqli_query($db, "select id from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_bankeu_kab = $q_hitung_bankeu_kab->num_rows;

        $q_hitung_bankeu_desa = mysqli_query($db, "select id from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_bankeu_desa = $q_hitung_bankeu_desa->num_rows;

        $q_hitung_dekon = mysqli_query($db, "select id from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_dekon = $q_hitung_dekon->num_rows;

        $q_hitung_tp = mysqli_query($db, "select id from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_tp = $q_hitung_tp->num_rows;

        $q_hitung_dub = mysqli_query($db, "select id from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $hitung_dub = $q_hitung_bankeu_desa->num_rows;

        $perda_1 = $hitung_dak + $hitung_dbhcht;
        $perda_2 = $hitung_bankeu_kab;

        $non_1 = $hitung_dekon + $hitung_tp + $hitung_dub;
        $non_2 = $hitung_bankeu_desa;

        $sum_perda = $perda_1 + $perda_2;
        $sum_non = $non_1 + $non_2;
        $no_target = 0;
        $no_real = 0;
        //kolom jml kegiatan beres
        //anggaran
        $anggaran_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dak) as total from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dbhcht) as total from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dekon) as total from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_tp) as total from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dub) as total from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $anggaran_perda_apbn = $anggaran_dak['total'] + $anggaran_dbhcht['total'];
        $anggaran_perda_apbd = $anggaran_bankeu_kab['total'];

        $anggaran_non_apbn = $anggaran_dekon['total'] + $anggaran_tp['total'] + $anggaran_dub['total'];
        $anggaran_non_apbd = $anggaran_bankeu_desa['total'];

        $sum_anggaran_perda = $anggaran_perda_apbn + $anggaran_perda_apbd;
        $sum_anggaran_non = $anggaran_non_apbn + $anggaran_non_apbd;
        //anggaran beres
        //pendamping
        $pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $pendamping_perda_apbn = $pendamping_dak['total'] + $pendamping_dbhcht['total'];
        $pendamping_perda_apbd = $pendamping_bankeu_kab['total'];

        $pendamping_non_apbn = $pendamping_dekon['total'] + $pendamping_tp['total'] + $pendamping_dub['total'];
        $pendamping_non_apbd = $pendamping_bankeu_desa['total'];

        $sum_pendamping_perda = $pendamping_perda_apbn + $pendamping_perda_apbd;
        $sum_pendamping_non = $pendamping_non_apbn + $pendamping_non_apbd;
        //pendamping beres
        //JUMLAH
        $jumlah_dak = $anggaran_dak['total'] + $pendamping_dak['total'];
        $jumlah_dbhcht = $anggaran_dbhcht['total'] + $pendamping_dbhcht['total'];
        $jumlah_bankeu_kab = $anggaran_bankeu_kab['total'] + $pendamping_bankeu_kab['total'];
        $jumlah_bankeu_desa = $anggaran_bankeu_desa['total'] + $pendamping_bankeu_desa['total'];
        $jumlah_dekon = $anggaran_dekon['total'] + $pendamping_dekon['total'];
        $jumlah_tp = $anggaran_tp['total'] + $pendamping_tp['total'];
        $jumlah_dub = $anggaran_dub['total'] + $pendamping_dub['total'];

        $jumlah_perda_apbn = $anggaran_perda_apbn + $pendamping_perda_apbn;
        $jumlah_perda_apbd = $anggaran_perda_apbd + $pendamping_perda_apbd;
        $jumlah_non_apbn = $anggaran_non_apbn + $pendamping_non_apbn;
        $jumlah_non_apbd = $anggaran_non_apbd + $pendamping_non_apbd;
        $jumlah_sum_perda = $sum_anggaran_perda + $sum_pendamping_perda;
        $jumlah_sum_non = $sum_anggaran_non + $sum_pendamping_non;
        //JUMLAH beres
        //realisasi spj
        $realisasi_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dak) as total from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dbhcht) as total from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dekon) as total from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_tp) as total from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dub) as total from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_perda_apbn = $realisasi_dak['total'] + $realisasi_dbhcht['total'];
        $realisasi_perda_apbd = $realisasi_bankeu_kab['total'];

        $realisasi_non_apbn = $realisasi_dekon['total'] + $realisasi_tp['total'] + $realisasi_dub['total'];
        $realisasi_non_apbd = $realisasi_bankeu_desa['total'];

        $sum_realisasi_perda = $realisasi_perda_apbn + $realisasi_perda_apbd;
        $sum_realisasi_non = $realisasi_non_apbn + $realisasi_non_apbd;
        //realisasi spj beres
        //pendamping spj
        $realisasi_pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendampingg_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $realisasi_pendamping_perda_apbn = $realisasi_pendamping_dak['total'] + $realisasi_pendamping_dbhcht['total'];
        $realisasi_pendamping_perda_apbd = $realisasi_pendamping_bankeu_kab['total'];

        $realisasi_pendamping_non_apbn = $realisasi_pendamping_dekon['total'] + $realisasi_pendampingg_tp['total'] + $realisasi_pendamping_dub['total'];
        $realisasi_pendamping_non_apbd = $realisasi_pendamping_bankeu_desa['total'];

        $sum_realisasi_pendamping_perda = $realisasi_pendamping_perda_apbn + $realisasi_pendamping_perda_apbd;
        $sum_realisasi_pendamping_non = $realisasi_pendamping_non_apbn + $realisasi_pendamping_non_apbd;
        //pendamping spj beres
        //JUMLAH SPJ
        $jumlah_realisasi_dak = $realisasi_dak['total'] + $realisasi_pendamping_dak['total'];
        $jumlah_realisasi_dbhcht = $realisasi_dbhcht['total'] + $realisasi_pendamping_dbhcht['total'];
        $jumlah_realisasi_bankeu_kab = $realisasi_bankeu_kab['total'] + $realisasi_pendamping_bankeu_kab['total'];
        $jumlah_realisasi_bankeu_desa = $realisasi_bankeu_desa['total'] + $realisasi_pendamping_bankeu_desa['total'];
        $jumlah_realisasi_dekon = $realisasi_dekon['total'] + $realisasi_pendamping_dekon['total'];
        $jumlah_realisasi_tp = $realisasi_tp['total'] + $realisasi_pendampingg_tp['total'];
        $jumlah_realisasi_dub = $realisasi_dub['total'] + $realisasi_pendamping_dub['total'];

        $jumlah_realisasi_perda_apbn = $realisasi_perda_apbn + $realisasi_pendamping_perda_apbn;
        $jumlah_realisasi_perda_apbd = $realisasi_perda_apbd + $realisasi_pendamping_perda_apbd;
        $jumlah_realisasi_non_apbn = $realisasi_non_apbn + $realisasi_pendamping_non_apbn;
        $jumlah_realisasi_non_apbd = $realisasi_non_apbd + $realisasi_pendamping_non_apbd;
        $jumlah_realisasi_sum_perda = $sum_realisasi_perda + $sum_realisasi_pendamping_perda;
        $jumlah_realisasi_sum_non = $sum_realisasi_non + $sum_realisasi_pendamping_non;
        //JUMLAH spj beres
        //Persen SPJ
        $persen_dak = ($jumlah_realisasi_dak != 0) ? $jumlah_realisasi_dak * 100 / $jumlah_dak : 0;
        $persen_dbhcht = ($jumlah_realisasi_dbhcht != 0) ? $jumlah_realisasi_dbhcht * 100 / $jumlah_dbhcht : 0;
        $persen_bankeu_kab = ($jumlah_realisasi_bankeu_kab != 0) ? $jumlah_realisasi_bankeu_kab * 100 / $jumlah_bankeu_kab : 0;
        $persen_bankeu_desa = ($jumlah_realisasi_bankeu_desa != 0) ? $jumlah_realisasi_bankeu_desa * 100 / $jumlah_bankeu_desa : 0;
        $persen_dekon = ($jumlah_realisasi_dekon != 0) ? $jumlah_realisasi_dekon * 100 / $jumlah_dekon : 0;
        $persen_tp = ($jumlah_realisasi_tp != 0) ? $jumlah_realisasi_tp * 100 / $jumlah_tp : 0;
        $persen_dub = ($jumlah_realisasi_dub != 0) ? $jumlah_realisasi_dub * 100 / $jumlah_dub : 0;

        $persen_perda_apbn = ($jumlah_realisasi_perda_apbn != 0) ? $jumlah_realisasi_perda_apbn * 100 / $jumlah_perda_apbn : 0;
        $persen_perda_apbd = ($jumlah_realisasi_perda_apbd != 0) ? $jumlah_realisasi_perda_apbd * 100 / $jumlah_perda_apbd : 0;
        $persen_non_apbn = ($jumlah_realisasi_non_apbn != 0) ? $jumlah_realisasi_non_apbn * 100 / $jumlah_non_apbn : 0;
        $persen_non_apbd = ($jumlah_realisasi_non_apbd != 0) ? $jumlah_realisasi_non_apbd * 100 / $jumlah_non_apbd : 0;
        $persen_sum_perda = ($jumlah_realisasi_sum_perda != 0) ? $jumlah_realisasi_sum_perda * 100 / $jumlah_sum_perda : 0;
        $persen_sum_non = ($jumlah_realisasi_sum_non != 0) ? $jumlah_realisasi_sum_non * 100 / $jumlah_sum_non : 0;
        //Persen spj beres
        //target & realisasi

        $target_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_dak = ($target_dak['target'] != 0) ? $target_dak['target'] / $hitung_dak : 0;
        $target_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_dbhcht = ($target_dbhcht['target'] != 0) ? $target_dbhcht['target'] / $hitung_dbhcht : 0;
        $target_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_bankeu_kab = ($target_bankeu_kab['target'] != 0) ? $target_bankeu_kab['target'] / $hitung_bankeu_kab : 0;
        $target_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_bankeu_desa = ($target_bankeu_desa['target'] != 0) ? $target_bankeu_desa['target'] / $hitung_bankeu_desa : 0;
        $target_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_dub = ($target_dub['target'] != 0 && $hitung_dub!=0) ? $target_dub['target'] / $hitung_dub : 0;
        $target_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_tp = ($target_tp['target'] != 0) ? $target_tp['target'] / $hitung_tp : 0;
        $target_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $target_dekon = ($target_dekon['target'] != 0) ? $target_dekon['target'] / $hitung_dekon : 0;

        //realisasi
        $real_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_dak = ($real_dak['reali'] != 0) ? $real_dak['reali'] / $hitung_dak : 0;
        $real_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dbhcht where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_dbhcht = ($real_dbhcht['reali'] != 0) ? $real_dbhcht['reali'] / $hitung_dbhcht : 0;
        $real_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_kab where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_bankeu_kab = ($real_bankeu_kab['reali'] != 0) ? $real_bankeu_kab['reali'] / $hitung_bankeu_kab : 0;
        $real_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_bankeu_desa = ($real_bankeu_desa['reali'] != 0) ? $real_bankeu_desa['reali'] / $hitung_bankeu_desa : 0;

        $real_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dub where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_dub = ($real_dub['reali'] != 0 && $hitung_dub!=0) ? $real_dub['reali'] / $hitung_dub : 0;
        $real_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_tp where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_tp = ($real_tp['reali'] != 0) ? $real_tp['reali'] / $hitung_tp : 0;
        $real_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dekon where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
        $real_dekon = ($real_dekon['reali'] != 0) ? $real_dekon['reali'] / $hitung_dekon : 0;
        ?>
        <tr><td colspan="13">&nbsp;</td></tr>
        <tr>
            <td><b>A. </b></td>
            <td><b>MASUK PERDA</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center"><b>I </b></td>
            <td><b>APBN</b></td>
            <td align="right"><?php echo $perda_1; ?></td>
            <td align="right"><?php echo number_format($anggaran_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_perda_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_perda_apbn, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($target_dak + $target_dbhcht) / 2, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($real_dak + $real_dbhcht) / 2, 2, ",", "."); ?></td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>a. DANA ALOKASI KHUSUS</td>
            <td align="right"><?php echo $hitung_dak; ?></td>
            <td align="right"><?php echo number_format($anggaran_dak['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_dak['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_dak, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_dak['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_dak['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_dak, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_dak, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_dak, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_dak, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>b. DBHCHT</td>
            <td align="right"><?php echo $hitung_dbhcht; ?></td>
            <td align="right"><?php echo number_format($anggaran_dbhcht['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_dbhcht['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_dbhcht, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_dbhcht['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_dbhcht['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_dbhcht, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_dbhcht, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_dbhcht, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_dbhcht, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center"><b>II </b></td>
            <td><b>APBD PROVINSI</b></td>
            <td align="right"><?php echo $perda_2; ?></td>
            <td align="right"><?php echo number_format($anggaran_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_perda_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_perda_apbd, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($target_bankeu_kab), 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($real_bankeu_kab), 2, ",", "."); ?></td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>a. DANA BANKEU KEPADA KAB/KOTA</td>
            <td align="right"><?php echo $hitung_bankeu_kab; ?></td>
            <td align="right"><?php echo number_format($anggaran_bankeu_kab['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_bankeu_kab['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_bankeu_kab, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_bankeu_kab['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_bankeu_kab['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_bankeu_kab, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_bankeu_kab, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_bankeu_kab, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_bankeu_kab, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><b>JUMLAH</b></td>
            <td align="right"><b><?php echo $sum_perda; ?></b></td>
            <td align="right"><b><?php echo number_format($sum_anggaran_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($sum_pendamping_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($jumlah_sum_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($sum_realisasi_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($sum_realisasi_pendamping_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($jumlah_realisasi_sum_perda, 0, ",", "."); ?></b></td>
            <td align="right"><b><?php echo number_format($persen_sum_perda, 2, ",", "."); ?></b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><b>B. </b></td>
            <td><b>TIDAK MASUK PERDA</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td align="center"><b>I</b></td>
            <td><b>APBN</b></td>
            <td align="right"><?php echo $non_1; ?></td>
            <td align="right"><?php echo number_format($anggaran_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_non_apbn, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_non_apbn, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($target_dekon + $target_tp + $target_dub) / 3, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format(($real_dekon + $real_tp + $real_dub) / 3, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>a. DEKONSENTRASI</td>
            <td align="right"><?php echo $hitung_dekon; ?></td>
            <td align="right"><?php echo number_format($anggaran_dekon['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_dekon['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_dekon, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_dekon['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_dekon['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_dekon, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_dekon, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_dekon, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_dekon, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>b. TUGAS PEMBANTUAN</td>
            <td align="right"><?php echo $hitung_tp; ?></td>
            <td align="right"><?php echo number_format($anggaran_tp['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_tp['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_tp, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_tp['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendampingg_tp['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_tp, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_tp, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_tp, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_tp, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>c. DANA URUSAN BERSAMA</td>
            <td align="right"><?php echo $hitung_dub; ?></td>
            <td align="right"><?php echo number_format($anggaran_dub['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_dub['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_dub, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_dub['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_dub['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_dub, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_dub, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_dub, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_dub, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center"><b>II</b></td>
            <td><b>APBD PROVINSI</b></td>
            <td align="right"><?php echo $non_2; ?></td>
            <td align="right"><?php echo number_format($anggaran_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_non_apbd, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_non_apbd, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_bankeu_desa, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_bankeu_desa, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>a. DANA BANKEU KEPADA DESA</td>
            <td align="right"><?php echo $hitung_bankeu_desa; ?></td>
            <td align="right"><?php echo number_format($anggaran_bankeu_desa['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($pendamping_bankeu_desa['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_bankeu_desa, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_bankeu_desa['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($realisasi_pendamping_bankeu_desa['total'], 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($jumlah_realisasi_bankeu_desa, 0, ",", "."); ?></td>
            <td align="right"><?php echo number_format($persen_bankeu_desa, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($target_bankeu_desa, 2, ",", "."); ?></td>
            <td align="right"><?php echo number_format($real_bankeu_desa, 2, ",", "."); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><b>JUMLAH<b></td>
                        <td align="right"><b><?php echo $sum_non; ?></b></td>
                        <td align="right"><b><?php echo number_format($sum_anggaran_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($sum_pendamping_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($jumlah_sum_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($sum_realisasi_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($sum_realisasi_pendamping_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($jumlah_realisasi_sum_non, 0, ",", "."); ?></b></td>
                        <td align="right"><b><?php echo number_format($persen_sum_non, 2, ",", "."); ?></b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        </tr>
                        <?php
                        $sum3 = $sum_perda + $sum_non;
                        $sum4 = $sum_anggaran_non + $sum_anggaran_perda;
                        $sum5 = $sum_pendamping_non + $sum_pendamping_perda;
                        $sum6 = $jumlah_sum_non + $jumlah_sum_perda;
                        $sum7 = $sum_realisasi_non + $sum_realisasi_perda;
                        $sum8 = $sum_realisasi_pendamping_non + $sum_realisasi_pendamping_perda;
                        $sum9 = $jumlah_realisasi_sum_perda + $jumlah_realisasi_sum_non;
                        echo '
	<tr>
	<td></td>
	<td align="center"><b>JUMLAH</b></td>
	<td align="right"><b>' . number_format($sum3, 0, ",", ".") . '</b></td><td align="right"><b>' . number_format($sum4, 0, ",", ".") . '</b></td>
	<td align="right"><b>' . number_format($sum5, 0, ",", ".") . '</b></td><td align="right"><b>' . number_format($sum6, 0, ",", ".") . '</b></td>
	<td align="right"><b>' . number_format($sum7, 0, ",", ".") . '</b></td><td align="right"><b>' . number_format($sum8, 0, ",", ".") . '</b></td>
	<td align="right"><b>' . number_format($sum9, 0, ",", ".") . '</b></td><td align="right"><b></b></td>
	<td align="right"><b></b></td><td align="right"><b></b></td><td>&nbsp;</td>

	</tr>
            ';
            ?>
</tbody>
</table>