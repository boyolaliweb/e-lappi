<?php if ((isset($_SESSION['level'])&&$_SESSION['level']=="skpd") && (isset($_SESSION['status_entry'])&&$_SESSION['status_entry']=="close")) exit('Entry data telah ditutup'); ?>

<?php

$default = mysqli_fetch_array($db->query("select * from t_apbd where id='" . $_GET['id'] . "' limit 1"));

?>

<style>

    .row-form{padding:5px !important;}

</style>

<script type="text/javascript">

    $(function() {

        var xhr = null;

        

        function list_program()

        {

            if (xhr) {

                xhr.abort();

            }

            xhr = $.ajax({

                type: 'POST',

                dataType: 'html',

                data: {kode_skpd: $('#skpd').val()},

                url: 'application/front/modules/laporan_apbd/item_program.php',

                success: function (data) {

                    $('#program').html(data);

                    xhr = null;

                },

                error: function (xhr, text_status, error_thrown) {},

                beforeSend: function () {},

                complete: function () {}

            });

        }

        $('#ubah_program').click(function(){

            list_program();

            return false;

        });

        // list_program();

        

        $('#program').change(function(event) {

            $(this).prop('selected', true);

            urls = 'application/front/modules/laporan_apbd/item_kegiatan.php';



            id_program = $(this).find('option:selected').data('id_program');

            kd_program = $(this).find('option:selected').data('kd_program');

            $('#nama_program').val($(this).find('option:selected').data('program'));

            $.post(urls, {id: $('#program').val(), id_program: id_program, kd_program: kd_program},

            function(data) {

                $('#kegiatan').html(data);

                $('#nama_kegiatan').val('');

                $('input[name=anggaran_apbd]').val('');

                $('input[name=sp2d]').val('');

            }

            );

        });

        $(document).on('change', '#select_kegiatan', function(event) {

            $(this).prop('selected', true);

            kode_program = $('#program').find('option:selected').data('kd_program');

            kode_urusan = $('#select_kegiatan').find('option:selected').data('kd_urusan');

            kode_bidang = $('#select_kegiatan').find('option:selected').data('kd_bidang');

            

            // alert(kode_bidang+kode_urusan);

            kode_kegiatan = $('#select_kegiatan').find('option:selected').data('kd_kegiatan');

            id_program = kode_urusan.toString()+kode_bidang.toString();

            $('#nama_kegiatan').val($(this).find('option:selected').data('kegiatan'));

            // kode_kegiatan = $(this).val();

            

            if (xhr) {

                //xhr.abort();

            }

            xhr = $.ajax({

                type: 'POST',

                dataType: 'json',

                data: {kode_program: kode_program, id_program: id_program, kode_kegiatan: kode_kegiatan},

                url: 'application/front/modules/laporan_apbd/item_anggaran.php',

                success: function (data) {

                    // alert(JSON.stringify(data));

                    pagu = data.pagu.split('.')[0];

                    total = data.total.split('.')[0];

                    $('input[name=anggaran_apbd]').val(pagu);

                    $('input[name=sp2d]').val(total);

                    real = (total/pagu)*100;

                    if(isNaN(floor10(real, -1))) real = '';

                    else real = floor10(real, -1);

                    $('#progres_real').val(floor10(real, -1));

                    xhr = null;

                },

                error: function (xhr, text_status, error_thrown) {},

                beforeSend: function () {

                    $('input[name=anggaran_apbd]').val('');

                    $('input[name=sp2d]').val('');

                },

                complete: function () {}

            });



            $("#lokasi").focus();

        });

	});

</script>

<div class="page-header">

    <div class="icon">

        <span class="ico-layout-7"></span>

    </div>

    <h1>Ubah Laporan Pelaksanaan Kegiatan Belanja Langsung <small>HALAMAN UNTUK MENGELOLA LAPORAN APDB KABUPATEN</small></h1>

</div>

<div class="row-fluid">

    <!-- Back button -->

    <div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>

</div>

<form action="laporan-apbd-update" method="POST" id="form">

    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">

    <input type="hidden" name="bagian" value="<?php echo $default['bagian']; ?>">

    <input type="hidden" name="subbagian" value="<?php echo $default['subbagian']; ?>">

    <div class="row-fluid">

        <div class="span6">                

            <div class="block">

                <div class="data-fluid">

                    <div class="head"><h2>Program / Kegiatan</h2></div>   

                    <div class="row-form">

                        <div class="span3">Bulan</div>

                        <div class="span9">

                            <input type="hidden" name="kode" value="<?php echo $default['kode']; ?>">

                            <input type="hidden" name="bulan" value="<?php echo $default['bulan']; ?>">

                            <input type="text" name="month" required="true" readonly="readonly" value="<?php echo bulan($default['bulan']); ?>">

                        </div>

                    </div>

                    <div class="row-form">

                        <div class="span3">Tahun</div>

                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>

                    </div>  

                    <div class="row-form">

                        <div class="span3">SKPD</div>

                        <div class="span9">

                            <select name="skpd" class="select" style="width: 100%;">

                                <?php

                                //check apakah login sebagai skpd / administrator

                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {

                                    $sqlSKPD = $db->query("select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");

                                } else {

                                    $sqlSKPD = $db->query("select * from m_skpd order by kode asc");

                                }

                                while ($rowSPKD = mysqli_fetch_array($sqlSKPD)) {

                                    $select1 = ($rowSPKD['id'] == $default['id_skpd']) ? "selected" : "";

                                    echo '<option value="' . $rowSPKD['id'] . '" ' . $select1 . '>' . $rowSPKD['nama'] . '</option>';

                                }

                                ?>

                            </select>

                        </div>

                    </div>

                    <!--

    <div class="row-form">

        <div class="span3">Urusan</div>

                            <div class="span9">

                                    <select name="urusan" id="urusan">

                                            <option>-- Pilih Urusan --</option>

                    <?php

                    $sqlUrusan = $db->query("select distinct(urusan) from m_bidang");

                    while ($rowUrusan = mysqli_fetch_array($sqlUrusan)) {

                        echo '<option value="' . $rowUrusan['urusan'] . '">' . $rowUrusan['urusan'] . '</option>';

                    }

                    ?>

                                    </select>

                            </div>

    </div>                    

    <div class="row-form">

        <div class="span3">Bidang Kegiatan</div>

                            <div class="span9">

                                    <select name="bidang" id="bidang" style="width: 100%;" required="true">

                                            <option value="<?php echo $default['bidang']; ?>"><?php echo $default['bidang']; ?></option>

                                    </select>

                            </div>

    </div>

                    -->

                    <div class="row-form">

                        <div class="span3">Program</div>

                        <div class="span9" id="program">

                            <?php echo $default['program'];?>

							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

							<span class="btn btn-warning" id="ubah_program"><i class="ico-edit"></i> Ubah Program</span>

                        </div>              

                    </div>

                    <div class="row-form">

                        <div class="span3">Kegiatan</div>

                        <div class="span9" id="kegiatan">

                            <?php echo $default['kegiatan']; ?>

                        </div>              

                    </div>

                    <div class="row-form">

                        <div class="span3">Sub Kegiatan</div>

                        <div class="span9">

                           <input type="text" name="subkegiatan" style="width: 100%;" data-index="3" value="<?php echo $default['subkegiatan']; ?>">

                            <span> Subkegiatan boleh dikosongkan. Tuliskan detail subkegiatan di atas ini.</span>

                        </div>              

                    </div>

                    <div class="row-form">

                        <div class="span3">Lokasi / Sasaran</div>

                        <div class="span9">

                            <input type="text" name="lokasi" value="<?php echo $default['lokasi']; ?>" style="width:100%;" id="lokasi" data-index="1">

                        </div>              

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="row-fluid">

        <div class="span6">                

            <div class="block">

                <hr>

                <div class="data-fluid">

                    <div class="head"><h2>Anggaran</h2></div>

                    <div class="row-form">

                        <div class="span3">Jumlah (Rp.)</div>

                        <div class="span9">

                            <input type="text" name="anggaran_apbd" value="<?php echo $default['anggaran_apbd']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                        this.value = this.value.replace(/\D/g, '')" data-index="2" readonly>

                            &nbsp;&nbsp;

                            <span id="anggaran_apbd_1" class="kanan"></span>

                        </div>              

                    </div>	

                </div>

            </div>

        </div>

        <div class="span6">                

            <div class="block">

                <hr>

                <div class="data-fluid">

                    <div class="head"><h2>Anggaran Kas</h2></div>

                    <div class="row-form">

                        <div class="span3">Anggaran Kas</div>

                        <div class="span9">

                            <input type="text" name="anggaran_kas" value="<?php echo $default['anggaran_kas']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                        this.value = this.value.replace(/\D/g, '')" data-index="3" readonly>

                            &nbsp;&nbsp;

                            <span id="anggaran_kas" class="kanan"></span>

                        </div>              

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="row-fluid">

        <div class="span6">                

            <div class="block">

                <div class="data-fluid">

                    <div class="head"><hr><h2>SP2D</h2></div>

                        <div class="row-form" style="display:none;">

                            <div class="span3">Kode SP2D</div>

                            <div class="span9">

                                <input type="text" name="sp2d_kode" value="<?php echo $default['sp2d_kode']; ?>" style="width:200px;" data-index="4" readonly>

                                &nbsp;&nbsp;

                            </div>             

                            <div class="span9 pull-right">*) Kode SP2D Boleh Dikosongkan</div> 

                        </div>      

                        <div class="row-form">

                            <div class="span3">Jumlah (Rp.)</div>

                            <div class="span9">

                                <input type="text" name="sp2d" value="<?php echo $default['sp2d']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')" data-index="5" readonly>

                                &nbsp;&nbsp;

                                <span id="sp2d" class="kanan"></span> 

                            </div>              

                        </div>				

                   		

                    <div class="head"><hr><h2>Progres Fisik</h2></div>

                    <div class="row-form">

                        <div class="span3">Target (%)</div>

                        <div class="span9">

                            <input type="text" name="progres_target" maxlength="6" style="width:60px;" class="decimal" value="<?php echo $default['progres_target']; ?>" data-index="7" required="true">

                        </div>              

                    </div>

                    <div class="row-form">

                        <div class="span3">Real (%)</div>

                        <div class="span9">

                            <input type="text" name="progres_real" value="<?php echo $default['progres_real']; ?>" maxlength="6" style="width:60px;" class="decimal" data-index="8" required="true">

                        </div> 

						<div class="span12">*) Gunakan titik untuk tanda pemisah koma</div>	

                    </div>

                    <div class="head"><hr><h2>Keterangan</h2></div>                  



                        <div class="row-form">

                            <div class="span3">Nilai Kontrak</div>

                            <div class="span9">

                                <input type="text" name="ket_nilai_kontrak" value="<?php echo $default['ket_nilai_kontrak']; ?>" data-index="9" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')">

                                 <span id="ket_nilai_kontrak" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Uang Muka</div>

                            <div class="span9">

                                <input type="text" name="ket_uang_muka" value="<?php echo $default['ket_uang_muka']; ?>" data-index="10" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')">

                                 <span id="ket_uang_muka" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Termin 1</div>

                            <div class="span9">

                                <input type="text" name="ket_termin1" value="<?php echo $default['ket_termin1']; ?>" data-index="11" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')">

                                 <span id="ket_termin1" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Termin 2</div>

                            <div class="span9">

                                <input type="text" name="ket_termin2" value="<?php echo $default['ket_termin2']; ?>" data-index="12" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')">

                                 <span id="ket_termin2" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Termin 3</div>

                            <div class="span9">

                                <input type="text" name="ket_termin3" value="<?php echo $default['ket_termin3']; ?>" data-index="13" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')">

                                 <span id="ket_termin3" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Sisa Kontrak</div>

                            <div class="span9">

                                <input type="text" name="ket_sisa_kontrak" value="<?php echo $default['ket_sisa_kontrak']; ?>" data-index="14" style="width:200px;" onkeyup="if (/\D/g.test(this.value))

                                            this.value = this.value.replace(/\D/g, '')" readonly="readonly">

                                 <span id="ket_sisa_kontrak" class="kanan"></span>

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3"> Tgl / No SPK</div>

                            <div class="span9">

                                <input type="text" name="ket_no_spk" value="<?php echo $default['ket_no_spk']; ?>" data-index="15" >

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Masalah</div>

                            <div class="span9">

                                <input type="text" name="permasalahan" value="<?php echo $default['permasalahan']; ?>" data-index="16">

                            </div>              

                        </div>



                        <div class="row-form">

                            <div class="span3">Keterangan</div>

                            <div class="span9">

                                <input type="text" name="keterangan" value="<?php echo $default['keterangan']; ?>" data-index="17">

                            </div>              

                        </div>

                    

                    <hr>



                    <div class="row-form">

                        <div class="span12">

                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">

							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

								<span class="btn btn-large" onClick="window.location.reload()"><i class="ico-edit"></i> Batal Ubah</span>

                        </div>               

                    </div>

                </div>  

            </div>

        </div>

    </div>

</form>



<script type="text/javascript">

    function anggaran_jumlah() {

        var apbd = 0;

        var pendamping = 0;

        var jumlah = 0;

        apbd = (parseInt($('input[name="anggaran_apbd"]').val()));

        pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));

        jumlah = apbd + pendamping;

        if (isNaN(jumlah)) {

            $('input[name="anggaran_jumlah"]').val("0");

        } else {

            $('input[name="anggaran_jumlah"]').val(jumlah);

        }

    }

    function panjar_jumlah() {

        var apbd = 0;

        var pendamping = 0;

        var jumlah = 0;

        apbd = (parseInt($('input[name="panjar_apbd"]').val()));

        pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));

        jumlah = apbd + pendamping;

        if (isNaN(jumlah)) {

            $('input[name="panjar_jumlah"]').val("0");

        } else {

            $('input[name="panjar_jumlah"]').val(jumlah);

        }

    }

    function realisasi_jumlah() {

        var apbd = 0;

        var pendamping = 0;

        var jumlah = 0;

        apbd = (parseInt($('input[name="realisasi_apbd"]').val()));

        pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));

        jumlah = apbd + pendamping;

        if (isNaN(jumlah)) {

            $('input[name="realisasi_jumlah"]').val("0");

        } else {

            $('input[name="realisasi_jumlah"]').val(jumlah);

        }

    }

    function detail() {

        $("#anggaran_apbd_1").html("Rp. " + koma($('input[name="anggaran_apbd"]').val()) + " ,-");

        $("#sp2d").html("Rp. " + koma($('input[name="sp2d"]').val()) + " ,-");

       

        $("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");

        $("#ket_nilai_kontrak").html("Rp. " + koma($('input[name="ket_nilai_kontrak"]').val()) + " ,-");

        $("#ket_uang_muka").html("Rp. " + koma($('input[name="ket_uang_muka"]').val()) + " ,-");

        $("#ket_termin1").html("Rp. " + koma($('input[name="ket_termin1"]').val()) + " ,-");

        $("#ket_termin2").html("Rp. " + koma($('input[name="ket_termin2"]').val()) + " ,-");

        $("#ket_termin3").html("Rp. " + koma($('input[name="ket_termin3"]').val()) + " ,-");

        

        var nilai_kontrak = parseInt($('input[name="ket_nilai_kontrak"]').val()) ? parseInt($('input[name="ket_nilai_kontrak"]').val()) : 0;

        var uang_muka = parseInt($('input[name="ket_uang_muka"]').val()) ? parseInt($('input[name="ket_uang_muka"]').val()) : 0;

        var termin1 = parseInt($('input[name="ket_termin1"]').val()) ? parseInt($('input[name="ket_termin1"]').val()) : 0;

        var termin2 = parseInt($('input[name="ket_termin2"]').val()) ? parseInt($('input[name="ket_termin2"]').val()) : 0;

        var termin3 = parseInt($('input[name="ket_termin3"]').val()) ? parseInt($('input[name="ket_termin3"]').val()) : 0;

        var sisa_kontrak = nilai_kontrak - (uang_muka + termin1 + termin2 + termin3);



        $("#ket_sisa_kontrak").html("Rp. " + koma(sisa_kontrak) + " ,-");

        $('input[name="ket_sisa_kontrak"]').val(sisa_kontrak);

    }



    setInterval(function() {

        anggaran_jumlah();

        panjar_jumlah();

        realisasi_jumlah();

        detail();

    }, 1000);

	$('.decimal').keyup(function(){

		var val = $(this).val();

		if(isNaN(val)){

			 val = val.replace(/[^0-9\.]/g,'');

			 if(val.split('.').length>2) 

				 val =val.replace(/\.+$/,"");

		}

		$(this).val(val); 

	});

	$('#form').on('keydown', 'input', function (event) {

    if (event.which == 13) {

        event.preventDefault();

        var $this = $(event.target);

        var index = parseFloat($this.attr('data-index'));

        $('[data-index="' + (index + 1).toString() + '"]').focus();

    }

	});

</script>