<?php
include ("../../../../core/db.config.php");

function bulan($bulan)
{
	switch($bulan){
		case 1: $bulan="Januari - Maret";
		break;
		case 2: $bulan= "April - Juni";
		break;
		case 3: $bulan= "Juli - September";
		break;
		case 4: $bulan= "Oktober - Desember";
		break;
	} 
	return $bulan;
}
function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h3>LAPORAN PELAKSANAAN KEGIATAN BELANJA LANGSUNG<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE <?php echo strtoupper(bulan($id)); ?> </h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">NAMA KEGIATAN</th>
            <th rowspan="2">ANGGARAN (Rp.)</th>
            <th colspan="4">PENYERAPAN DANA</th>
            <th colspan="2">PROGRES FISIK</th>
            <th rowspan="2">MASALAH</th>
            <th rowspan="2">KETERANGAN</th>
        </tr>
        <tr>
            <th>SP2D</th>
            <th>%</th>
            <th>SPJ</th>
            <th>%</th>
            <th>TARGET (%)</th>
            <th>REALISASI (%)</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5=4:3*100</th>
            <th>6</th>
            <th>7=6:3*100</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        if ($id == 1) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $skpd . "' and bulan ='01' or bulan ='02' or bulan='03' and tahun='" . $tahun . "'");
            } else {
                $sql = $db->query("select distinct(id_skpd) from t_apbd where bulan ='01' or bulan ='02' or bulan='03' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 2) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $skpd . "' and bulan ='04' or bulan ='05' or bulan='06' and tahun='" . $tahun . "'");
            } else {
                $sql = $db->query("select distinct(id_skpd) from t_apbd where bulan ='04' or bulan ='05' or bulan='06' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 3) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $skpd . "' and bulan ='07' or bulan ='08' or bulan='09' and tahun='" . $tahun . "'");
            } else {
                $sql = $db->query("select distinct(id_skpd) from t_apbd where bulan ='07' or bulan ='08' or bulan='09' and tahun='" . $tahun . "'");
            }
        } else
        if ($id == 4) {
            if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
                $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $skpd . "' and bulan ='10' or bulan ='11' or bulan='12' and tahun='" . $tahun . "'");
            } else {
                $sql = $db->query("select distinct(id_skpd) from t_apbd where bulan ='10' or bulan ='11' or bulan='12' and tahun='" . $tahun . "'");
            }
        }
        $noBid = 1;
        //make looping to get data
        while ($rowBid = mysqli_fetch_array($sql)) {
            $skpd = mysqli_fetch_array($db->query("select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1"));
            echo '
							<tr>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $skpd['nama'] . '</b></td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
						';
            $sql_program = $db->query("select distinct(program) from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "'");
            $jum3 = 0;
            $jum4 = 0;
            $jum5 = 0;
            $jum6 = 0;
            $jum7 = 0;
            $jum8 = 0;
            $jum9 = 0;
            $jum10 = 0;
            $jum11 = 0;
            $jum12 = 0;
            $jum13 = 0;
            while ($row_program = mysqli_fetch_array($sql_program)) {
                echo '
                                    <tr>
                                            <td>&nbsp;</td>
                                            <td><b>' . $row_program['program'] . '</b></td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            
                                    </tr>
                                ';
                if (isset($_SESSION['id_skpd'])) {
                    $sqlKegiatan = $db->query("select * from t_apbd where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "'");
                } else {
                    $sqlKegiatan = $db->query("select * from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "'");
                }
                $no = 1;
                $count = 0;
                $panjar_persen = 0;
                $realisasi_persen = 0;
                while ($row = mysqli_fetch_array($sqlKegiatan)) {
                    $count = $count + 1;
                    $jum3 = $jum3 + $row['anggaran_apbd'];
                    $jum4 = $jum4 + $row['panjar_apbd'];
                    $panjar_persen = $jum4 / $jum3 * 100;
                    $realisasi_persen = $jum4 / $jum3 * 100;
                    $jum5 = $jum5 + $panjar_persen;
                    $jum6 = $jum6 + $row['realisasi_apbd'];
                    $jum7 = $jum7 + $realisasi_persen;
                    $jum8 = $jum8 + $row['progres_target'];
                    $jum9 = $jum9 + $row['progres_real'];
                    $anggaran_apbd = ($row['anggaran_apbd'] == 0) ? "-" : number_format($row['anggaran_apbd'], 0, ",", ".");
                    $panjar_apbd = ($row['panjar_apbd'] == 0) ? "-" : number_format($row['panjar_apbd'], 0, ",", ".");
                    $panjar_persen = ($panjar_persen == 0) ? "-" : number_format($panjar_persen, 2, ",", ".");
                    $realisasi_apbd = ($row['realisasi_apbd'] == 0) ? "-" : number_format($row['realisasi_apbd'], 0, ",", ".");
                    $realisasi_persen = ($realisasi_persen == 0) ? "-" : number_format($realisasi_persen, 2, ",", ".");
                    $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 2, ",", ".");
                    $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 2, ",", ".");
                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                    $keterangan = ($row['keterangan']) ? $row['keterangan'] : "&nbsp;";
                    $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                    echo '
								<tr>
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_apbd . '</td>
									<td align="right">' . $panjar_apbd . '</td>
									<td align="right">' . $panjar_persen . '</td>
									<td align="right">' . $realisasi_apbd . '</td>
									<td align="right">' . $realisasi_persen . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td>' . $masalah . '</td>							
									<td>' . $keterangan . '</td>
								</tr>
								';
                }
            }

            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
            $jum5 = ($jum5 / $count == 0) ? "-" : number_format($jum5 / $count, 2, ",", ".");
            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
            $jum7 = ($jum7 / $count == 0) ? "-" : number_format($jum7 / $count, 2, ",", ".");
            $jum8 = ($jum8 / $count == 0) ? "-" : number_format($jum8 / $count, 2, ",", ".");
            $jum9 = ($jum9 / $count == 0) ? "-" : number_format($jum9 / $count, 2, ",", ".");
            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
            echo '
							<tr>
								<td>&nbsp;</td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td>&nbsp;</td><td>&nbsp;</td>
								
							</tr>
						';
        }
        ?>
    </tbody>
</table>


