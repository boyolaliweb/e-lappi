<script type="text/javascript">
    $(function () {
        $('#bulan').change(function (event) {
             //begin here
            var perSKPD;
            <?php if (isset($_SESSION['id_skpd'])) { ?>
                perSKPD = null;
            <?php } else { ?>
                perSKPD = $('#per_skpd').val();
            <?php } ?>
            //alert($('#bulan').val());
            var start = new Date().getTime(); // note getTime()
            
             urls2 = 'application/front/modules/laporan_apbd/item_bulan_print.php';
            $.post(urls2, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, username: "<?php echo $_SESSION['username']; ?>", level: "<?php echo $_SESSION['level'];?>", per_skpd:perSKPD},
                function (data2) {
                    $('#tableExcel').html(data2);
                }
            );
            
            
            urls = 'application/front/modules/laporan_apbd/item_bulan.php';
            $.post(urls, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, username: "<?php echo $_SESSION['username']; ?>", level: "<?php echo $_SESSION['level'];?>", per_skpd:perSKPD},
                function (data) {
                    $('#tableWeb').html(data);
                    var end = new Date().getTime();
                    console.log( end - start );
                }
            );

           
        });
    });
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Laporan Pelaksanaan Kegiatan Belanja Langsung
        <small>HALAMAN UNTUK MENGELOLA LAPORAN APDB KABUPATEN</small>
    </h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Laporan Pelaksanaan Kegiatan Belanja Langsung </h2>
                <ul class="buttons">
                    <button class="btn" onclick="print('tableExcel')">
                        <span class="icon-print icon-white"></span> Cetak
                    </button>
                    <a href="#" class="btn" id="Excel" type="button">
                        <span class="icon-book icon-white"></span> Export to Excel
                    </a>

                </ul>
            </div>
            <div>
                <h3>Filtering </h3>
                <table>
                    <tr>
                         <?php if (!isset($_SESSION['id_skpd'])) { ?>
                            <td>
                                SKPD:
                            </td>
                            <td>
                                <select name="per_skpd" id="per_skpd" style="width:200px;">
                                    <option value="all">Semua SKPD</option>
                                    <?php
                                    $sql_skpd = $db->query("select id,nama from m_skpd order by kode asc");
                                    while ($row_skpd = $sql_skpd->fetch_assoc()) {
                                        echo '<option value="' . $row_skpd['id'] . '">' . $row_skpd['nama'] . '</option>';
                                    }
                                    ?>
                                </select>

                            </td>
                            <td width="80px;">&nbsp;</td>
                        <?php } ?>
                        <td>
                            Bulanan:
                        </td>
                        <td>
                            <select name="bulan" id="bulan">
                                <option value="">--Pilih Bulan--</option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">Nopember</option>
                                <option value="12">Desember</option>
                            </select>
                        </td>
                        <td>
                            <span id="loading_image" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading <img src="media/img/loader.gif"/></span>
                        </td>
                    </tr>
                    <!--
					<tr>
                        <td>Triwulan: </td>
                        <td>
                            <select name="triwulan" id="triwulan">
                                <option value="">--Pilih Triwulan--</option>
                                <option value="1">Januari-Maret</option>
                                <option value="2">April-Juni</option>
                                <option value="3">Juli-September</option>
                                <option value="4">Oktober-Desember</option>
                            </select>
                        </td>
                    </tr>
					-->
                </table>
            </div>
            <hr>
            <div>&nbsp;</div>
            <div class="data-fluid" id="tableExcel">
                <h4>LAPORAN PELAKSANAAN KEGIATAN BELANJA LANGSUNG<br>KABUPATEN BOYOLALI TAHUN
                        ANGGARAN <?php echo $_SESSION['tahun']; ?></h4>

                <table cellpadding="0" border="1" cellspacing="0" width="100%" id="tableExcels">
                    <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">NO</th>
                        <th rowspan="2">NAMA KEGIATAN</th>
                        <th rowspan="2">ANGGARAN (Rp.)</th>
                        <th rowspan="2">ANGGARAN KAS</th>
                        <th colspan="5">PENYERAPAN DANA</th>
                        <th colspan="2">PROGRES FISIK</th>
                        <th colspan="9">KETERANGAN KEGIATAN FISIK/NON FISIK DGN KONTRAK PENYEDIA BARANG/JASA</th>
                        <th rowspan="2">SISA ANGGARAN</th>
                        <th rowspan="2">KETERANGAN</th>

                    </tr>
                    <tr>
                        <th colspan="2">SP2D</th>
                        <th>%</th>
                        <th>TARGET (%)</th>
                        <th>REALISASI (%)</th>
                        <th>NILAI KONTRAK</th> 
                        <th>UANG MUKA</th> 
                        <th>TERMIN 1</th> 
                        <th>TERMIN 2</th> 
                        <th>TERMIN 3</th> 
                        <th>SISA KONTRAK</th> 
                        <th>TGL/NO.SPK</th> 
                        <th>LOKASI</th> 
                        <th>MASALAH</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th colspan="2">5</th>
                        <th>6=5:3*100</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        
                        
                    </tr>
                    </thead>

                </table>
            </div>
            <div class="data-fluid" id="tableWeb">
                <table cellpadding="0" border="1" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">NO</th>
                        <th rowspan="2">NAMA KEGIATAN</th>
                        <th rowspan="2">ANGGARAN (Rp.)</th>
                        <th rowspan="2">ANGGARAN KAS</th>
                        <th colspan="3">PENYERAPAN DANA</th>
                        <th colspan="2">PROGRES FISIK</th>
                        <th colspan="9">KETERANGAN KEGIATAN FISIK/NON FISIK DGN KONTRAK PENYEDIA BARANG/JASA</th>
                        <th rowspan="2">SISA ANGGARAN</th>
                        <th rowspan="2">KETERANGAN</th>

                    </tr>
                    <tr>
                        <th colspan="2">SP2D</th>
                        <th>%</th>
                        <th>TARGET (%)</th>
                        <th>REALISASI (%)</th>
                        <th>NILAI KONTRAK</th> 
                        <th>UANG MUKA</th> 
                        <th>TERMIN 1</th> 
                        <th>TERMIN 2</th> 
                        <th>TERMIN 3</th> 
                        <th>SISA KONTRAK</th> 
                        <th>TGL/NO.SPK</th> 
                        <th>LOKASI</th> 
                        <th>MASALAH</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th colspan="2">5</th>
                        <th>6=5:3*100</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div>
</div>
