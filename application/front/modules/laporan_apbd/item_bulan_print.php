<?php
include ("../../../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
$level = $_POST['level'];
$per_skpd = $_POST['per_skpd'];
?>
<h5>LAPORAN PELAKSANAAN KEGIATAN BELANJA LANGSUNG<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE BULAN <?php echo strtoupper(bulan($id)); ?> </h5>

<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">NAMA KEGIATAN</th>
            <th rowspan="2">ANGGARAN (Rp.)</th>
            <th rowspan="2">ANGGARAN KAS</th>
            <th colspan="3">PENYERAPAN DANA</th>
            <th colspan="2">PROGRES FISIK</th>
            <th colspan="9">KETERANGAN KEGIATAN FISIK/NON FISIK DGN KONTRAK PENYEDIA BARANG/JASA</th>
            <th rowspan="2">SISA ANGGARAN</th>
            <th rowspan="2">KETERANGAN</th>

        </tr>
        <tr>
            <th>NILAI SP2D FINAL</th>
            <th>%</th>
            <th>TARGET (%)</th>
            <th>REALISASI (%)</th>
            <th>NILAI KONTRAK</th> 
            <th>UANG MUKA</th> 
            <th>TERMIN 1</th> 
            <th>TERMIN 2</th> 
            <th>TERMIN 3</th> 
            <th>SISA KONTRAK</th> 
            <th>TGL/NO.SPK</th> 
            <th>LOKASI</th> 
            <th>MASALAH</th>
            
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th colspan="2">5</th>
            <th>6=5:3*100</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
            <th>16</th>
            <th>17</th>
            <th>18</th>
            <th>19</th>
            
        </tr>
    </thead>
    <tbody id="table">
       <?php
        if ($skpd > 0) { //cek apakah login sebagai skpd atau administrator
            $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $skpd . "' and tahun='" . $tahun . "' and bulan='" . $id . "' and status_update='0'");
        } else {
            if ($per_skpd == "all") { //login as administrator
                $sql = $db->query("select distinct(id_skpd) from t_apbd where tahun='" . $tahun . "' and bulan='" . $id . "' and status_update='0'");
            } else {
                $sql = $db->query("select distinct(id_skpd) from t_apbd where id_skpd='" . $per_skpd . "' and tahun='" . $tahun . "' and bulan='" . $id . "' and status_update='0'");
            }
        }
        $noBid = 1;
        //make looping to get data
        while ($rowBid = $sql->fetch_assoc()) {
            $q_skpd = $db->query("select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1");
            $row_skpd = $q_skpd->fetch_assoc();
            echo '
				<tr>
					<td align="center"><b>' . romawi($noBid++) . '</b></td>
					<td><b>' . $row_skpd['nama'] . '</b></td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td>&nbsp;</td>
				</tr>
			';
            //$sql_program = $db->query("select distinct(program) from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
             $sql_program = $db->query("SELECT DISTINCT (a.program)
                            FROM t_apbd a
                            LEFT JOIN m_program b ON a.program = b.program where a.id_skpd='" . $rowBid['id_skpd'] . "' and a.bulan='" . $id . "'
                            and a.tahun='" . $tahun . "' and status_update='0' and (a.program<>'' and a.kegiatan<>'') order by b.kode_urusan asc,
                            b.kode_bidang asc,b.kode_program asc");
            $jum3 = 0;
            $jum4 = 0;
            $jum5 = 0;
            $jum6 = 0;
            $jum7 = 0;
            $jum8 = 0;
            $jum9 = 0;
            $jum10 = 0;
            $jum11 = 0;
            $jum12 = 0;
            $jum13 = 0;
            //progres real
            $real = 0;
            $target = 0;
            $sp2d = 0;
           
            $sp2d_pro = 0;
            $ket_nilai_kontrak = 0;
            $ket_uang_muka = 0;
            $ket_termin1 = 0;
            $ket_termin2 = 0;
            $ket_termin3 = 0;
            $ket_sisa_kontrak = 0;
            $ket_sisa_anggaran_kas = 0;
           
            while ($row_program = $sql_program->fetch_assoc()) {
                echo '
                    <tr>
                    <td>&nbsp;</td>
                    <td><b>' . $row_program['program'] . '</b></td>
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                ';
                 if ($skpd > 0) {
                    $sqlKegiatan = $db->query("select * from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "'  and status_update='0'");
                } else {
                    if ($per_skpd == "all") { //login as administrator
                        $sqlKegiatan = $db->query("select * from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "'  and status_update='0'");
                    } else {
                        $sqlKegiatan = $db->query("select * from t_apbd where id_skpd='" . $rowBid['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and program='" . $row_program['program'] . "'  and status_update='0'");
                    }
                }
                $no = 1;
                $count = 0;
                $sp2d_persen = 0;
               
                while ($row = $sqlKegiatan->fetch_assoc()) {
                    $count = $count + 1;
                    $jum3 = $jum3 + $row['anggaran_apbd'];
                    $jum4 = $jum4 + $row['sp2d'];
                    if ($row['anggaran_apbd'] != 0) {
                        $sp2d_persen = $row['sp2d'] / $row['anggaran_apbd'] * 100;
                       
                    } else {
                        $sp2d_persen = 0;
                        
                    }
                    $jum5 = $jum5 + $sp2d_persen;
                   
                    $jum8 = $jum8 + $row['progres_target'];
                    $jum9 = $jum9 + $row['progres_real'];
                    $jum11 = $jum11 + $row['anggaran_kas'];

                    $ket_nilai_kontrak = $ket_nilai_kontrak + $row['ket_nilai_kontrak'];
                    $ket_uang_muka = $ket_uang_muka + $row['ket_uang_muka'];
                    $ket_termin1 = $ket_termin1 + $row['ket_termin1'];
                    $ket_termin2 = $ket_termin2 + $row['ket_termin2'];
                    $ket_termin3 = $ket_termin3 + $row['ket_termin3'];
                    $ket_sisa_kontrak = $ket_sisa_kontrak + $row['ket_sisa_kontrak'];
                    $ket_sisa_anggaran_kas = $ket_sisa_anggaran_kas + ($row['anggaran_kas'] - $row['sp2d']);


                    //walaupun 0 persen tetap ikut dihitung Rev 2.0
                    $real = $real + 1;
                    $target = $target + 1;
                    $sp2d = $sp2d + 1;
                   
                    $anggaran_apbd = ($row['anggaran_apbd'] == 0) ? "0" : number_format($row['anggaran_apbd'], 0, ".", ",");
                    $sp2d = ($row['sp2d'] == 0) ? "0" : number_format($row['sp2d'], 0, ".", ",");
                    $sp2d_persen = ($sp2d_persen == 0) ? "0" : number_format($sp2d_persen, 2, ".", ",");
                   
                    $progres_target = ($row['progres_target'] == 0) ? "0" : number_format($row['progres_target'], 2, ".", ",");
                    //$progres_target = ($row['progres_target'] == 0) ? "0" : ($row['progres_target']);
                    $progres_real = ($row['progres_real'] == 0) ? "0" : number_format($row['progres_real'], 2, ".", ",");
                    $anggaran_kas = ($row['anggaran_kas'] == 0) ? "0" : number_format($row['anggaran_kas'], 0, ".", ",");
                   
                    $sp2d_kode = ($row['sp2d_kode']) ? $row['sp2d_kode'] : "&nbsp;";

                    $nilai_kontrak = ($row['ket_nilai_kontrak']) ? $row['ket_nilai_kontrak'] : "&nbsp;";
                    $uang_muka = ($row['ket_uang_muka']) ? $row['ket_uang_muka'] : "&nbsp;";
                    $termin1 = ($row['ket_termin1']) ? $row['ket_termin1'] : "&nbsp;";
                    $termin2 = ($row['ket_termin2']) ? $row['ket_termin2'] : "&nbsp;";
                    $termin3 = ($row['ket_termin3']) ? $row['ket_termin3'] : "&nbsp;";
                    $sisa_kontrak = ($row['ket_sisa_kontrak']) ? $row['ket_sisa_kontrak'] : "&nbsp;";
                    $no_spk = ($row['ket_no_spk']) ? $row['ket_no_spk'] : "&nbsp;";
                    $lokasi = ($row['lokasi']) ? $row['lokasi'] : "&nbsp;";
                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";

                    $subkegiatan = ($row['subkegiatan']) ? " " . $row['subkegiatan'] . "<br>" : "";
                    $sisa_anggaran_kas = number_format($row['anggaran_apbd'] - $row['sp2d'], 0, ".", ",");

                    $keterangan = ($row['keterangan']) ? $row['keterangan'] : "&nbsp;";

                    $nilai_kontrak = ($row['ket_nilai_kontrak'] == 0) ? "-" : number_format($row['ket_nilai_kontrak'], 0, ".", ",");
                    $uang_muka = ($row['ket_uang_muka'] == 0) ? "-" : number_format($row['ket_uang_muka'], 0, ".", ",");
                    $termin1 = ($row['ket_termin1'] == 0) ? "-" : number_format($row['ket_termin1'], 0, ".", ",");
                    $termin2 = ($row['ket_termin2'] == 0) ? "-" : number_format($row['ket_termin2'], 0, ".", ",");
                    $termin3 = ($row['ket_termin3'] == 0) ? "-" : number_format($row['ket_termin3'], 0, ".", ",");
                    $sisa_kontrak = ($row['ket_sisa_kontrak'] == 0) ? "-" : number_format($row['ket_sisa_kontrak'], 0, ".", ",");

                    echo '
                        <tr>
                            <td align="center">' . $no++ . '</td>
                            <td>' . $row['kegiatan'] . $subkegiatan . $lokasi . '</td>
                            <td align="right">' . $anggaran_apbd . '</td>
                            <td align="right">' . $anggaran_kas . '</td>
                            <td align="right">' . $sp2d . '</td>
                            <td align="right">' . $sp2d_persen . '</td>
                           
                            <td align="right">' . $progres_target . '</td>
                            <td align="right">' . $progres_real . '</td>
                            <td>' . $nilai_kontrak . '</td>                         
                            <td>' . $uang_muka . '</td>                           
                            <td>' . $termin1 . '</td>                           
                            <td>' . $termin2 . '</td>                           
                            <td>' . $termin3 . '</td>                           
                            <td>' . $sisa_kontrak . '</td>                           
                            <td>' . $no_spk . '</td>                           
                            <td>' . $lokasi . '</td>                           
                            <td>' . $masalah . '</td>                           
                            <td align="right">' . $sisa_anggaran_kas . '</td>
                            <td>' . $keterangan . '</td> 
                        </tr>
                    ';
                }
            }
             //$jum5 = ($jum5 == 0) ? "-" : number_format($jum5 / $panjar, 2, ",", ".");
            //menghitung rata2 persen panjar
            if($jum3==0){
                $sp2d_pro = 0;
            }else{
                $sp2d_pro = $jum4 / $jum3 * 100;
            }    
            $sp2d_pro = ($sp2d_pro == 0) ? "0" : number_format($sp2d_pro, 2, ".", ",");
            //menghitung rata2 persen spj
            
            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ".", ",");
            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ".", ",");
            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ".", ",");

            

            $jum7 = ($jum7 == 0) ? "-" : number_format($jum4 / $jum3, 2, ".", ",");
            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8 / $target, 2, ".", ",");
            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9 / $real, 2, ".", ",");

            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ".", ",");
            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ".", ",");
            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ".", ",");
            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ".", ",");

             $ket_nilai_kontrak = ($ket_nilai_kontrak == 0) ? "-" : number_format($ket_nilai_kontrak, 0, ",", ".");
            $ket_uang_muka = ($ket_uang_muka == 0) ? "-" : number_format($ket_uang_muka, 0, ",", ".");
            $ket_termin1 = ($ket_termin1 == 0) ? "-" : number_format($ket_termin1, 0, ",", ".");
            $ket_termin2 = ($ket_termin2 == 0) ? "-" : number_format($ket_termin2, 0, ",", ".");
            $ket_termin3 = ($ket_termin3 == 0) ? "-" : number_format($ket_termin3, 0, ",", ".");
            $ket_sisa_kontrak = ($ket_sisa_kontrak == 0) ? "-" : number_format($ket_sisa_kontrak, 0, ",", ".");

            $ket_sisa_anggaran_kas = ($ket_sisa_anggaran_kas == 0) ? "-" : number_format($ket_sisa_anggaran_kas, 0, ",", ".");

            echo '
                    <tr>
                            <td>&nbsp;</td>
                            <td align="center"><b>Jumlah</b></td>
                            <td align="right"><b>' . $jum3 . '</b></td>
                            <td align="right"><b>' . $jum11 . '</b></td>
                            <td align="right"><b>' . $jum4 . '</b></td>
                            <td align="right"><b>' . $sp2d_pro . '</b></td>
                           
                            <td align="right"><b>' . $jum8 . '</b></td>
                            <td align="right"><b>' . $jum9 . '</b></td>
                            <td align="right"><b>' . $ket_nilai_kontrak . '</b></td>
                            <td align="right"><b>' . $ket_uang_muka . '</b></td>
                            <td align="right"><b>' . $ket_termin1 . '</b></td>
                            <td align="right"><b>' . $ket_termin2 . '</b></td>
                            <td align="right"><b>' . $ket_termin3 . '</b></td>
                            <td align="right"><b>' . $ket_sisa_kontrak . '</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right"><b>' . $ket_sisa_anggaran_kas . '</b></td><td>&nbsp;</td>
                    </tr>
            ';
        }
        ?>
    </tbody>
</table>
<?php if ($skpd > 0) { ?>
    <p>&nbsp;</p>
    <p align="right">Boyolali, <?php echo date("d-m-Y"); ?>
        <br><br><br><br><br>
		<?php
			$sql_kepala = $db->query("select kepala,pangkat,nip from m_skpd where id='" . $skpd . "' limit 1");
			$row_kepala = $sql_kepala->fetch_assoc();
			echo "<u><b>".$row_kepala['kepala']."</b></u><br>";
			echo $row_kepala['pangkat']."<br>NIP. ";
			echo $row_kepala['nip'];
	    ?>
    </p>
<?php } ?>

