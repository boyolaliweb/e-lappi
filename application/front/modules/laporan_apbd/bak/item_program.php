<?php

include ("../../../../core/db.config.php");

$id = $_POST['id'];

?>

<select name="program" id="select_program" required="true" class="select" style="width: 100%;">

<option>-- Pilih Program --</option>

<?php

	$sqlProgram = $db->query("select * from m_program order by kode_urusan asc");

	while ($rowProgram = mysqli_fetch_array($sqlProgram)) {

		echo '<option value="'.$rowProgram['kode_urusan'].'_'.$rowProgram['kode_bidang'].'_'.$rowProgram['kode_program'].'_'.$rowProgram['program'].'">'.$rowProgram['kode_urusan'].'.'.$rowProgram['kode_bidang'].'.'.$rowProgram['kode_program'].' &raquo; '.$rowProgram['program'].'</option>';

	}

?>

</select>