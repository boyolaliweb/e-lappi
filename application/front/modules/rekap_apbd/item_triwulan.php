<?php
include ("../../../../core/db.config.php");

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
Triwulan: <?php echo $id ?> | Tahun: <?php echo $tahun ?>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2">SATUAN KERJA</th>
			<th rowspan="2">JML KEG</th>
			<th rowspan="2">JUMLAH ANGGARAN</th>
			<th colspan="4">REALISASI PENYERAPAN DANA</th>
			<th colspan="2">PROGRES FISIK</th>
			<th rowspan="2">KET.</th>
		</tr>
		<tr>
			<th>PANJAR/SP2D</th>
			<th>%</th>
			<th>SPJ</th>
			<th>%</th>
			<th>TARGET</th>
			<th>REALISASI</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
		</tr>
	</thead>
	<tbody id="table">
		<?php
		if ($id == 1) {          
            $bulan = " (bulan ='01' or bulan ='02' or bulan='03') ";
        } else
        if ($id == 2) {
			$bulan = " (bulan ='04' or bulan ='05' or bulan='06') ";
        } else
        if ($id == 3) {
			$bulan = " (bulan ='07' or bulan ='08' or bulan='09') ";          
        } else
        if ($id == 4) {
			$bulan = " (bulan ='10' or bulan ='11' or bulan='12') ";
        }
		
		$sql = mysqli_query($db,"select distinct(id_skpd) from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'");
		
		$noBid = 1;
		$baris = 0;
		$jumlah_baris = 0;
		//make looping to get data
		$sum_kegiatan = 0;
		$sum_anggaran_apbd = 0;
		$sum_anggaran_pendamping = 0;
		$sum_anggaran_jumlah = 0;
		$sum_panjar_apbd = 0; 
		$sum_panjar_apbd_persen = 0;
		$sum_panjar_pendamping = 0;
		$sum_panjar_pendamping_persen = 0;
		$sum_realisasi_apbd = 0;
		$sum_realisasi_apbd_persen = 0;
		$sum_realisasi_pendamping = 0;
		$sum_realisasi_pendamping_persen = 0;
		$sum_persen_target = 0;
		$sum_persen_real = 0;
		$no=0;
	
		echo '<tr>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>';
		
		while ($rowProgram = mysqli_fetch_array($sql)) {
			$nama_skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='".$rowProgram['id_skpd']."' limit 1"));
			$sum_kegiatan = mysql_num_rows(mysqli_query($db,"select id from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$anggaran_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_apbd) anggaran from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			
			$panjar_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_apbd) panjar from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			
			$realisasi_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_apbd) anggaran from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
		
			$target_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$real_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) target from t_apbd where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			
			echo '
			<tr>
				<td align="center"><b>' . ($noBid++) . '</b></td>
				<td><b>' . $nama_skpd['nama'] . '</b></td>
				<td align="right">' . number_format($sum_kegiatan, 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_apbd['anggaran'], 0, ",", ".") . '</td>
			
				<td align="right">' . number_format($panjar_apbd['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format(($panjar_apbd['panjar']!=0) ? $panjar_apbd['panjar'] * 100 / $anggaran_apbd['anggaran'] : 0, 2, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_apbd['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format(($realisasi_apbd['anggaran']!=0) ? $realisasi_apbd['anggaran'] * 100 / $anggaran_apbd['anggaran'] : 0, 2, ",", ".") . '</td>
				<td align="right">' . number_format($target_apbd['target'], 2, ",", ".") . '</td>
				<td align="right">' . number_format($real_apbd['target'], 2, ",", ".") . '</td>
				<td>&nbsp;</td>
			</tr>
			';
			//$sqlKegiatan = mysqli_query($db,"select * from t_apbd where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "'");
			
			$sqlSKPD = mysqli_query($db,"select * from t_apbd where id_skpd='" . $rowProgram['id_skpd'] . "' and $bulan and tahun='" . $tahun . "' and status_update='0'");
			$no = $no + 1;
			$baris = $baris + $sum_kegiatan;
			$sum_anggaran_apbd = $sum_anggaran_apbd + $anggaran_apbd['anggaran'];
			
			$sum_panjar_apbd = $sum_panjar_apbd + $panjar_apbd['panjar'];
			$sum_panjar_persen = ($sum_panjar_persen + (($panjar_apbd['panjar']!=0) ? $panjar_apbd['panjar'] * 100 / $anggaran_apbd['anggaran'] : 0));
			$sum_realisasi_apbd = $sum_realisasi_apbd + $realisasi_apbd['anggaran'];
			$sum_realisasi_persen = ($sum_realisasi_persen + (($realisasi_apbd['anggaran']!=0) ? $realisasi_apbd['anggaran'] * 100 / $anggaran_apbd['anggaran'] : 0));
			$sum_persen_target = $sum_persen_target + $target_apbd['target'];
			$sum_persen_real = $sum_persen_real + $real_apbd['target'];
		
			
		}
		echo '<tr>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			</tr>';
		echo '
			<tr>
				<td>&nbsp;</td>
				<td align="center"><b>JUMLAH</b></td>
				<td align="right"><b>' . number_format($baris, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_apbd, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_apbd, 0, ",", ".") . '</b></td>			
				<td align="right"><b>' . number_format(($sum_panjar_persen!=0) ? $sum_panjar_persen / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_apbd, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_realisasi_persen!=0) ? $sum_realisasi_persen / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_target!=0) ? $sum_persen_target / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_real!=0) ? $sum_persen_real / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right">&nbsp;</td>
													
			</tr>
			';
		?>
	</tbody>
</table>