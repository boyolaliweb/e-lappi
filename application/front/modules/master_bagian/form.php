<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Bagian <small>HALAMAN UNTUK MENGATUR DATA MASTER BAGIAN</small></h1>  
</div>
<form action="" method="post">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Tambah Data Bagian</h2>
                </div>              
                <div class="data-fluid">
                    <div class="row-form">
                        <div class="span3">Nama Biro</div>
                        <div class="span9"><input type="text" name="biro" required="true"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span12">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>               
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>

<?php
//processing submit data
if (isset($_POST['submit'])) {
    $sql = $db->query("insert into m_bagian set
                            biro = '" . $_POST['biro'] . "'
                    ");
    if ($sql) {
        alert_success();
        redirect("master-bagian");
    }
}
?>