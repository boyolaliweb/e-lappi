<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Bagian <small>HALAMAN UNTUK MENGATUR DATA MASTER BAGIAN</small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Master Bagian</h2>
                <ul class="buttons">
                    <a href="master-bagian-form" class="btn" type="button">
                        <span class="icon-plus icon-white"></span> Tambah Bagian
                    </a>
                </ul>                                                        
            </div>                
            <div class="data-fluid">
                <table class="table fpTable lcnp dataTable" cellpadding="0" cellspacing="0" width="100%" id="DataTables_Table_2" aria-describedby="DataTables_Table_2_info" style="width: 100%;">
                    <thead>
                        <tr role="row">
                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 50px;">No</th>
                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 122px;">Biro</th>
                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Opsi</th>
                        </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
                        $sql = $db->query("select * from m_bagian order by id asc");
                        $sql->data_seek(0);
                        $no = 1;
                        while ($row = $sql->fetch_assoc()) {
                            echo '
                                <tr class="odd">
                                    <td>' . $no++ . '</td>
                                    <td>' . $row['biro'] . '</td>
                                    <td>
                                    <a href="index.php?val=master-bagian-edit&id=' . $row['id'] . '" class="button green">
                                        <div class="icon"><span class="ico-pencil"></span></div>
                                    </a>
                                    <a href="index.php?val=master-bagian-delete&id=' . $row['id'] . '" class="button red">
                                        <div class="icon"><span class="ico-remove"></span></div>
                                    </a>                                              
                                    </td>
                                </tr>
                                ';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
        </div>

    </div>
</div>

