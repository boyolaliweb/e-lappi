<?php
include ("../../../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h5>REKAPITULASI CAPAIAN PERKEMBANGAN PELAKSANAAN KEGIATAN PER SKPD<BR>SUMBER DANA ALOKASI KHUSUS TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE BULAN <?php echo strtoupper(bulan($id)); ?></h5>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">SATUAN KERJA</th>
            <th rowspan="2">JML KEG</th>
            <th rowspan="2">DAK MURNI</th>
            <th rowspan="2">PENDAMPING</th>
            <th rowspan="2">JUMLAH</th>
            <th colspan="4">SP2D</th>
            <th colspan="4">SPJ</th>
            <th colspan="2">PROGRES FISIK</th>
            <th rowspan="2">KET.</th>
        </tr>
        <tr>
            <th>DAK MURNI</th>
            <th>%</th>
            <th>PENDAMPING</th>
            <th>%</th>
            <th>DAK MURNI</th>
            <th>%</th>
            <th>PENDAMPING</th>
            <th>%</th>
            <th>TARGET</th>
            <th>REAL</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
            <th>16</th>
            <th>17</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        //$sql = mysqli_query($db, "select distinct(bidang) from t_dak where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
        $sql = $db->query("SELECT DISTINCT (a.bidang)
                            FROM t_dak a
                            INNER JOIN m_bidang b ON a.bidang = b.bidang where a.bulan='".$id."'
                            and a.tahun='".$tahun."' and status_update='0' order by b.kode_urusan asc,
                            b.kode_bidang asc");
        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_dak = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_dak = 0;
        $sum_panjar_dak_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_dak = 0;
        $sum_realisasi_dak_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;

        while ($rowBid = mysqli_fetch_array($sql)) {
            echo '
			<tr>
				<td align="center"><b>' . romawi($noBid++) . '</b></td>
				<td><b>Bidang ' . $rowBid['bidang'] . '</b></td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
			</tr>
			';
            //$sqlKegiatan = mysqli_query($db,"select * from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $tahun . "' and status_update='0'");

            $sqlSKPD = mysqli_query($db, "select distinct(id_skpd) from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");

            $baris = 0;
            while ($row = mysqli_fetch_array($sqlSKPD)) {

                $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $row['id_skpd'] . "'"));
                $kegiatan_skpd = mysqli_num_rows(mysqli_query($db, "select kegiatan from t_dak where bidang='" . $rowBid['bidang'] . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                //hitung anggaran Rp
                $anggaran_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_dak = $anggaran_dak['anggaran'];
                $anggaran_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $anggaran_pendamping = $anggaran_pendamping['anggaran'];
                $anggaran_jumlah = $anggaran_dak + $anggaran_pendamping;
                $panjar_dak = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_dak = $panjar_dak['anggaran'];
                $panjar_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $panjar_pendamping = $panjar_pendamping['anggaran'];
                $realisasi_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dak) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_dak = $realisasi_dak['anggaran'];
                $realisasi_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) anggaran from t_dak where bidang='" . $rowBid['bidang'] . "' and  bulan='" . $id . "' and tahun='" . $tahun . "' and id_skpd='" . $row['id_skpd'] . "'"));
                $realisasi_pendamping = $realisasi_pendamping['anggaran'];
                //realisasi dan target
                $target_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $real_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dak where bidang='" . $rowBid['bidang'] . "' and id_skpd='" . $row['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
                $persen_target = ($target_dak['target'] != 0) ? $target_dak['target'] / $kegiatan_skpd : 0;
                $persen_real = ($real_dak['reali'] != 0) ? $real_dak['reali'] / $kegiatan_skpd : 0;
                echo '
				<tr>
					
					<td align="center">-</td>
					<td>' . $nama_skpd['nama'] . '</td>
					<td align="right">' . number_format($kegiatan_skpd, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_dak, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_jumlah, 0, ",", ".") . '</td>
					<td align="right">' . number_format($panjar_dak, 0, ",", ".") . '</td>
					<td align="right">' . number_format(($panjar_dak != 0) ? $panjar_dak * 100 / $anggaran_dak : 0, 2, ",", ".") . '</td>
					<td align="right">' . number_format($panjar_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format(($panjar_pendamping != 0) ? $panjar_pendamping * 100 / $anggaran_pendamping : 0, 2, ",", ".") . '</td>
					<td align="right">' . number_format($realisasi_dak, 0, ",", ".") . '</td>
					<td align="right">' . number_format(($realisasi_dak != 0) ? $realisasi_dak * 100 / $anggaran_dak : 0, 2, ",", ".") . '</td>
					<td align="right">' . number_format($realisasi_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format(($realisasi_pendamping != 0) ? $realisasi_pendamping * 100 / $anggaran_pendamping : 0, 2, ",", ".") . '</td>
					<td align="right">' . number_format($persen_target, 2, ",", ".") . '</td>
					<td align="right">' . number_format($persen_real, 2, ",", ".") . '</td>
					<td>&nbsp;</td>							
				</tr>
				';

                $baris = $baris + 1;


                $sum_kegiatan = $sum_kegiatan + $kegiatan_skpd;
                $sum_anggaran_dak = $sum_anggaran_dak + $anggaran_dak;
                $sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping;
                $sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
                $sum_panjar_dak = $sum_panjar_dak + $panjar_dak;
                $sum_panjar_dak_persen = $sum_panjar_dak_persen + (($panjar_dak != 0) ? $panjar_dak * 100 / $anggaran_dak : 0);
                $sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping;
                $sum_panjar_pendamping_persen = $sum_panjar_pendamping_persen + (($panjar_pendamping != 0) ? $panjar_pendamping * 100 / $anggaran_pendamping : 0);
                $sum_realisasi_dak = $sum_realisasi_dak + $realisasi_dak;
                $sum_realisasi_dak_persen = $sum_realisasi_dak_persen + (($realisasi_dak != 0) ? $realisasi_dak * 100 / $anggaran_dak : 0);
                $sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping;
                $sum_realisasi_pendamping_persen = $sum_realisasi_pendamping_persen + (($realisasi_pendamping != 0) ? $realisasi_pendamping * 100 / $anggaran_pendamping : 0);
                $sum_persen_target = $sum_persen_target + $persen_target;
                $sum_persen_real = $sum_persen_real + $persen_real;
            }
            $jumlah_baris = $jumlah_baris + $baris;
        }
        echo '
			<tr>
				<td></td>
				<td align="center"><b>JUMLAH TOTAL</b></td>
				<td align="right"><b>' . number_format($sum_kegiatan, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_dak, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_dak, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_panjar_dak_persen != 0) ? $sum_panjar_dak_persen / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_panjar_pendamping_persen != 0) ? $sum_panjar_pendamping_persen / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_dak, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_realisasi_dak_persen != 0) ? $sum_realisasi_dak_persen / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_realisasi_pendamping_persen != 0) ? $sum_realisasi_pendamping_persen / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_target != 0) ? $sum_persen_target / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_real != 0) ? $sum_persen_real / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right">&nbsp;</td>
			</tr>
			';
        ?>
    </tbody>
</table>
