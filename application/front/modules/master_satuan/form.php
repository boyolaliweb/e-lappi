<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Satuan <small>HALAMAN UNTUK MENGATUR DATA MASTER SATUAN</small></h1>  
</div>
<form action="" method="post">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Tambah Data Satuan</h2>
                </div>              
                <div class="row-form">
                    <div class="span3">Nama Satuan</div>
                    <div class="span9"><input type="text" name="nama" required="true"></div>
                </div>                    
                <div class="row-form">
                    <div class="span3">Keterangan</div>
                    <div class="span9"><input type="text" name="keterangan" required="true"></div>
                </div>
                <div class="row-form">
                    <div class="span3">
                        <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                    </div>
                </div>
            </div>  
        </div>
    </div>
</form>


<?php
//processing submit data
if (isset($_POST['submit'])) {
    $sql = $db->query("insert into m_satuan set
                        nama = '" . $_POST['nama'] . "',
                        keterangan = '" . $_POST['keterangan'] . "'
                ");
    if ($sql) {
        alert_success();
        redirect("master-satuan");
    }
}
?>