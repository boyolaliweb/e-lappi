<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Satuan <small>HALAMAN UNTUK MENGATUR DATA MASTER SATUAN</small></h1>  
</div>
<?php
if (isset($_GET['id'])) {
    $sqlOpen = $db->query("select * from m_satuan where id='" . $_GET['id'] . "' limit 1");
    $sqlOpen->data_seek(0);
    $default = $sqlOpen->fetch_assoc();
} else {
    redirect("master-satuan");
}
?>
<form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="head">                                
                    <h2>Ubah Data Satuan</h2>
                </div>              
                <div class="data-fluid">

                    <div class="row-form">
                        <div class="span3">Nama Satuan</div>
                        <div class="span9"><input type="text" name="nama" required="true" value="<?php echo $default['nama']; ?>"></div>
                    </div>                    
                    <div class="row-form">
                        <div class="span3">Keterangan</div>
                        <div class="span9"><input type="text" name="keterangan" required="true" value="<?php echo $default['keterangan']; ?>"></div>
                    </div>
                    <div class="row-form">
                        <div class="span3">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</form>


<?php
//processing submit data
if (isset($_POST['submit'])) {
    $sql = $db->query("update m_satuan set
                        nama = '" . $_POST['nama'] . "',
                        keterangan = '" . $_POST['keterangan'] . "'
                        where id ='" . $_POST['id'] . "'
                ");
    if ($sql) {
        alert_success();
        redirect("master-satuan");
    }
}
?>