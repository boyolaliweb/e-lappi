<script type="text/javascript">
    $(function() {
        $('#bulan').change(function(event) {
            //alert($('#bulan').val());
            urls = 'application/front/modules/rekap_bankeu_desa/item_bulan.php';
            $.post(urls, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            
            function(data) {
                $('#tableWeb').html(data);
            }
            );
    
            urls2 = 'application/front/modules/rekap_bankeu_desa/item_bulan_print.php';
            $.post(urls2, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
        $('#triwulan').change(function(event) {
            urls = 'application/front/modules/rekap_bankeu_desa/item_triwulan.php';
            $.post(urls, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data) {
                $('#tableWeb').html(data);
            }
            );
            urls2 = 'application/front/modules/rekap_bankeu_desa/item_triwulan_print.php';
            $.post(urls2, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
    });
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Rekapitulasi Capaian Perkembangan Pelaksanaan Kegiatan Per SKPD <small>Sumber Dana Bantuan Keuangan Kepada Desa Tahun Anggaran <?php echo $_SESSION['tahun'];?></small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Sumber Dana Bantuan Kepada Desa</h2>
                <ul class="buttons">
                    <button class="btn" onclick="print('tableExcel')">
                        <span class="icon-print icon-white"></span> Cetak
                    </button>
                    <a href="#" class="btn" id="excel" type="button">
                        <span class="icon-book icon-white"></span> Export to Excel
                    </a>
                </ul>                                                        
            </div>         

            <div>
                <h3>Periode </h3>
                <table>
                    <tr>
                        <td>
                            Bulanan: 
                        </td>
                        <td>
                            <select name="bulan" id="bulan">
                                <option>-- Pilih Bulan --</option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">Nopember</option>
                                <option value="12">Desember</option>
                            </select>
                        </td>
                        <td>
                            <span id="loading_image" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading <img src="media/img/loader.gif"/></span>
                        </td>
                    </tr>
                </table>
                </ul>                                                        
            </div>     
            <hr>
            <div>&nbsp;</div>
			<div class="data-fluid" id="tableWeb">
            </div> 
			<div>&nbsp;</div>
			<div class="data-fluid" id="tableExcel">
            </div> 
        </div>
    </div>
</div>