<?php
include ("../../../../core/db.config.php");

function bulan($bulan) {
    switch ($bulan) {
        case 1: $bulan = "Januari";
            break;
        case 2: $bulan = "Februari";
            break;
        case 3: $bulan = "Maret";
            break;
        case 4: $bulan = "April";
            break;
        case 5: $bulan = "Mei";
            break;
        case 6: $bulan = "Juni";
            break;
        case 7: $bulan = "Juli";
            break;
        case 8: $bulan = "Agustus";
            break;
        case 9: $bulan = "September";
            break;
        case 10: $bulan = "Oktober";
            break;
        case 11: $bulan = "Nopember";
            break;
        case 12: $bulan = "Desember";
            break;
    }
    return $bulan;
}

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h3>REKAPITULASI CAPAIAN PERKEMBANGAN PELAKSANAAN KEGIATAN PER SKPD<BR>SUMBER DANA BANTUAN KEPADA DESA TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE BULAN <?php echo strtoupper(bulan($id)); ?></h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">PROGRAM/SATKER</th>
            <th rowspan="2">JML KEG</th>
            <th rowspan="2">BANKEU KEPADA DESA</th>
            <th rowspan="2">PENDAMPING</th>
            <th rowspan="2">JUMLAH</th>
            <th colspan="3">SP2D</th>
            <th colspan="3">SPJ</th>
            <th colspan="2">PROGRES FISIK</th>
            <th rowspan="2">KET.</th>
        </tr>
        <tr>
            <th>BANKEU KEPADA DESA</th>
            <th>DANA PENDAMPING/PENUNJANG</th>
            <th>JUMLAH</th>
            <th>BANKEU KEPADA DESA</th>
            <th>PENDAMPING/PENUNJANG</th>
            <th>JUMLAH</th>
            <th>TARGET</th>
            <th>REAL</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
        </tr>
    </thead>
    <tbody id="table">
        <?php
        $sql = mysqli_query($db, "select distinct(id_skpd) from t_bankeu_desa where bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");

        $noBid = 1;
        $baris = 0;
        $jumlah_baris = 0;
        //make looping to get data
        $sum_kegiatan = 0;
        $sum_anggaran_bankeu_desa = 0;
        $sum_anggaran_pendamping = 0;
        $sum_anggaran_jumlah = 0;
        $sum_panjar_bankeu_desa = 0;
        $sum_panjar_bankeu_desa_persen = 0;
        $sum_panjar_pendamping = 0;
        $sum_panjar_jumlah = 0;
        $sum_realisasi_jumlah = 0;
        $sum_panjar_pendamping_persen = 0;
        $sum_realisasi_bankeu_desa = 0;
        $sum_realisasi_bankeu_desa_persen = 0;
        $sum_realisasi_pendamping = 0;
        $sum_realisasi_pendamping_persen = 0;
        $sum_persen_target = 0;
        $sum_persen_real = 0;
        $no = 0;
        $no_target = 0;
        $no_real = 0;

        while ($rowProgram = mysqli_fetch_array($sql)) {
            $nama_skpd = mysqli_fetch_array(mysqli_query($db, "select nama from m_skpd where id='" . $rowProgram['id_skpd'] . "' limit 1"));
            $sum_kegiatan = mysqli_num_rows(mysqli_query($db, "select id from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_desa) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $anggaran_jumlah = $anggaran_bankeu_desa['anggaran'] + $anggaran_pendamping['anggaran'];

            $panjar_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_bankeu_desa) panjar from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $panjar_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(panjar_pendamping) panjar from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $panjar_jumlah = $panjar_bankeu_desa['panjar'] + $panjar_pendamping['panjar'];

            $realisasi_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_desa) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $realisasi_pendamping = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) anggaran from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $realisasi_jumlah = $realisasi_bankeu_desa['anggaran'] + $realisasi_pendamping['anggaran'];

            $target_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            $real_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) target from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'"));
            //target
            $persen_target = ($target_bankeu_desa['target'] != 0) ? $target_bankeu_desa['target'] / $sum_kegiatan : 0;
            $persen_real = ($real_bankeu_desa['target'] != 0) ? $real_bankeu_desa['target'] / $sum_kegiatan : 0;
            echo '
			<tr>
				<td align="center"><b>' . ($noBid++) . '</b></td>
				<td><b>' . $nama_skpd['nama'] . '</b></td>
				<td align="right">' . number_format($sum_kegiatan, 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_bankeu_desa['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_pendamping['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_bankeu_desa['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_pendamping['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_bankeu_desa['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_pendamping['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($persen_target, 2, ",", ".") . '</td>
				<td align="right">' . number_format($persen_real, 2, ",", ".") . '</td>
				<td>&nbsp;</td>
			</tr>
			';
            //$sqlKegiatan = mysqli_query($db,"select * from t_bankeu_desa where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "' and status_update='0'");

            $sqlSKPD = mysqli_query($db, "select * from t_bankeu_desa where id_skpd='" . $rowProgram['id_skpd'] . "' and bulan='" . $id . "' and tahun='" . $tahun . "' and status_update='0'");
            if ($target_bankeu_desa['target'] != 0) {
                $no_target = $no_target + 1;
            }
            if ($real_bankeu_desa['target'] != 0) {
                $no_real = $no_real + 1;
            }
            $baris = $baris + $sum_kegiatan;
            $sum_anggaran_bankeu_desa = $sum_anggaran_bankeu_desa + $anggaran_bankeu_desa['anggaran'];
            $sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping['anggaran'];
            $sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
            $sum_panjar_bankeu_desa = $sum_panjar_bankeu_desa + $panjar_bankeu_desa['panjar'];
            $sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping['panjar'];
            $sum_panjar_jumlah = $sum_panjar_jumlah + $panjar_jumlah;
            $sum_realisasi_bankeu_desa = $sum_realisasi_bankeu_desa + $realisasi_bankeu_desa['anggaran'];
            $sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping['anggaran'];
            $sum_realisasi_jumlah = $sum_realisasi_jumlah + $realisasi_jumlah;
            $sum_persen_target = $sum_persen_target + $persen_target;
            $sum_persen_real = $sum_persen_real + $persen_real;
        }
        echo '
			<tr>
				<td></td>
				<td align="center"><b>JUMLAH TOTAL</b></td>
				<td align="right"><b>' . number_format($baris, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_bankeu_desa, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_bankeu_desa, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_bankeu_desa, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_target != 0) ? $sum_persen_target / $no_target : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_real != 0) ? $sum_persen_real / $no_real : 0, 2, ",", ".") . '</b></td>
				<td align="right">&nbsp;</td>
													
			</tr>
			';
        ?>
    </tbody>
</table>
