<?php
$default = mysqli_fetch_array(mysqli_query($db,"select * from t_dak where id='".$_GET['id']."' limit 1"));
?>
<style>
.row-form{padding:5px !important;}
</style>
<script type="text/javascript">
	 $(function() {
		$('#urusan').change(function(event) {
            urls = 'application/front/modules/laporan_dak/item_bidang.php';
            $.post(urls, {id: $('#urusan').val()},
            function(data) {
                $('#bidang').html(data);
				$('#program').html("");
				$('#kegiatan').html("");
            }
            );
        });
        $('#bidang').change(function(event) {
            urls = 'application/front/modules/laporan_dak/item_program.php';
            $.post(urls, {id: $('#bidang').val()},
            function(data) {
                $('#program').html(data);
            }
            );
        });
		$('#program').change(function(event) {
            urls = 'application/front/modules/laporan_dak/item_kegiatan.php';
            $.post(urls, {id: $('#program').val()},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Ubah Laporan Dana Alokasi Khusus (DAK) <small>HALAMAN UNTUK MENGELOLA LAPORAN DAK</small></h1>  
</div>
<form action="laporan-dak-update" method="POST">
<input type="hidden" name="id" value="<?php echo $default['id'];?>">
<div class="row-fluid">
    <div class="span6">                
        <div class="block">
            <div class="data-fluid">
			 <div class="head"><h2>Bidang / Program / Kegiatan</h2></div>   
				<div class="row-form">
                    <div class="span3">Tahun</div>
                    <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun'];?>"></div>
                </div>  
                <div class="row-form">
                    <div class="span3">SKPD</div>
					<div class="span9">
						<select name="skpd" class="select" style="width: 100%;">
						<?php 
						//check apakah login sebagai skpd / administrator
						if(isset($_SESSION['id_skpd']) && $_SESSION['id_skpd']!=""){
							$sqlSKPD = mysqli_query($db,"select * from m_skpd where id='".$_SESSION['id_skpd']."' limit  1");
						}else{
							$sqlSKPD = mysqli_query($db,"select * from m_skpd order by kode asc");
						}
						while($rowSPKD=mysqli_fetch_array($sqlSKPD)){
							$select1 = ($rowSPKD['id']==$default['id_skpd']) ? "selected" : "";
							echo '<option value="'.$rowSPKD['id'].'" '.$select1.'>'.$rowSPKD['nama'].'</option>';
						}
						?>
						</select>
					</div>
                </div>
                <div class="row-form">
                    <div class="span3">Urusan</div>
					<div class="span9">
						<select name="urusan" id="urusan">
							<option>-- Pilih Urusan --</option>
							<?php
							$sqlUrusan = mysqli_query($db,"select distinct(urusan) from m_bidang");
							while($rowUrusan=mysqli_fetch_array($sqlUrusan)){
								echo '<option value="'.$rowUrusan['urusan'].'">'.$rowUrusan['urusan'].'</option>';
							}
							?>
						</select>
					</div>
                </div>                    
                <div class="row-form">
                    <div class="span3">Bidang Kegiatan</div>
					<div class="span9">
						<select name="bidang" id="bidang" style="width: 100%;" required="true">
							<option value="<?php echo $default['bidang'];?>"><?php echo $default['bidang'];?></option>
						</select>
					</div>
                </div>
				<div class="row-form">
                    <div class="span3">Program</div>
                    <div class="span9">
						<select name="program" id="program" style="width: 100%;" required="true">	
							<option value="<?php echo $default['program'];?>"><?php echo $default['program'];?></option>						
						</select>
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Kegiatan</div>
                    <div class="span9">
						<select name="kegiatan" id="kegiatan" class="select" style="width: 100%;" required="true">
						<option value="<?php echo $default['kegiatan'];?>"><?php echo $default['kegiatan'];?></option>
						</select>
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Lokasi / Sasaran</div>
                    <div class="span9">
						<input type="text" name="lokasi" value="<?php echo $default['lokasi'];?>" style="width:100%;">
					</div>              
				</div>
				
				<div class="head"><hr><h2>Anggaran</h2></div>
				<div class="row-form">
                    <div class="span3">DAK</div>
                    <div class="span9">
						<input type="text" name="anggaran_dak" value="<?php echo $default['anggaran_dak'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Dana Pendamping</div>
                    <div class="span9">
						<input type="text" name="anggaran_pendamping" value="<?php echo $default['anggaran_pendamping'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">JUMLAH</div>
                    <div class="span9">
						<input type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
					</div>              
				</div>
				
				<div class="head"><hr><h2>Panjar / SP2D / SPMU</h2></div>
				<div class="row-form">
                    <div class="span3">DAK</div>
                    <div class="span9">
						<input type="text" name="panjar_dak" value="<?php echo $default['panjar_dak'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Dana Pendamping</div>
                    <div class="span9">
						<input type="text" name="panjar_pendamping" value="<?php echo $default['panjar_pendamping'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">JUMLAH</div>
                    <div class="span9">
						<input type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
					</div>              
				</div>
				
				<div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
				<div class="row-form">
                    <div class="span3">DAK</div>
                    <div class="span9">
						<input type="text" name="realisasi_dak" value="<?php echo $default['realisasi_dak'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Dana Pendamping</div>
                    <div class="span9">
						<input type="text" name="realisasi_pendamping" value="<?php echo $default['realisasi_pendamping'];?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">JUMLAH</div>
                    <div class="span9">
						<input type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
					</div>              
				</div>
				
				<div class="head"><hr><h2>Target Fisik</h2></div>
				<div class="row-form">
                    <div class="span3">Target (%)</div>
                    <div class="span9">
						<input type="text" name="progres_target" value="<?php echo $default['progres_target'];?>" style="width:60px;">
					</div>              
				</div>
				<div class="row-form">
                    <div class="span3">Real (%)</div>
                    <div class="span9">
						<input type="text" name="progres_real" value="<?php echo $default['progres_real'];?>" style="width:60px;">
					</div>              
				</div>
				
				<div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
				<div class="row-form">
                    <div class="span3">Keterangan</div>
                    <div class="span9">
						<input type="text" name="permasalahan" value="<?php echo $default['permasalahan'];?>">
					</div>              
				</div>
				
				<div class="row-form">
                    <div class="span12">
						<input type="submit" name="submit" value="Simpan" class="btn btn-large">
					</div>               
				</div>
			</div>  
		</div>
	</div>
</div>
</form>

<script type="text/javascript">
	function anggaran_jumlah(){
        var dak = 0;
        var pendamping = 0;
		var jumlah = 0;
		dak = (parseInt($('input[name="anggaran_dak"]').val()));
		pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
		jumlah = dak + pendamping;
		if(isNaN(jumlah)){
			$('input[name="anggaran_jumlah"]').val("0");
		}else{
			$('input[name="anggaran_jumlah"]').val(jumlah);
		}
	}
	function panjar_jumlah(){
        var dak = 0;
        var pendamping = 0;
		var jumlah = 0;
		dak = (parseInt($('input[name="panjar_dak"]').val()));
		pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
		jumlah = dak + pendamping;
		if(isNaN(jumlah)){
			$('input[name="panjar_jumlah"]').val("0");
		}else{
			$('input[name="panjar_jumlah"]').val(jumlah);
		}
	}
	function realisasi_jumlah(){
        var dak = 0;
        var pendamping = 0;
		var jumlah = 0;
		dak = (parseInt($('input[name="realisasi_dak"]').val()));
		pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
		jumlah = dak + pendamping;
		if(isNaN(jumlah)){
			$('input[name="realisasi_jumlah"]').val("0");
		}else{
			$('input[name="realisasi_jumlah"]').val(jumlah);
		}
	}

    setInterval(function() {
        anggaran_jumlah();
		panjar_jumlah();
		realisasi_jumlah();
    }, 1000);
    
</script>