<?php
include ("../../../../core/db.config.php");

function bulan($bulan)
{
	switch($bulan){
		case 1: $bulan="Januari - Maret";
		break;
		case 2: $bulan= "April - Juni";
		break;
		case 3: $bulan= "Juli - September";
		break;
		case 4: $bulan= "Oktober - Desember";
		break;
	} 
	return $bulan;
}
function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
   <h3>REKAPITULASI PERKEMBANGAN KEGIATAN APBD KABUPATEN<br>(Pagu Anggaran Sesuai PERDA APBD Kab. Boyolali TAHUN ANGGARAN <?php echo $tahun; ?><br>PERIODE BULAN <?php echo strtoupper(bulan($id));?> </h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">URAIAN</th>
			<th rowspan="2">JUMLAH KEG</th>
			<th rowspan="2">ANGGARAN (Rp)</th>
			<th colspan="4">REALISASI PENYERAPAN DANA</th>
			<th colspan="2">PELAKSANAAN FISIK</th>
			<th rowspan="2">KET.</th>
		</tr>
		<tr>
			<th>PANJAR/SP2D</th>
			<th>%</th>
			<th>SPJ</th>
			<th>%</th>
			<th>Target</th>
			<th>Realisasi</th>
		</tr>
	</thead>
	<tbody id="table">
		<?php
        if ($id == 1) {          
            $bulan = "bulan ='01' or bulan ='02' or bulan='03' ";
        } else
        if ($id == 2) {
			$bulan = "bulan ='04' or bulan ='05' or bulan='06' ";
        } else
        if ($id == 3) {
			$bulan = "bulan ='07' or bulan ='08' or bulan='09' ";          
        } else
        if ($id == 4) {
			$bulan = "bulan ='10' or bulan ='11' or bulan='12' ";
        }
		
		//hitung jumlah kegiatan kegiatan APBD
		$kegiatan_dak = mysql_num_rows(mysqli_query($db,"select id from t_dak where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$kegiatan_dbhcht = mysql_num_rows(mysqli_query($db,"select id from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$kegiatan_bankeu_kab = mysql_num_rows(mysqli_query($db,"select id from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$kegiatan_apbd = mysql_num_rows(mysqli_query($db,"select id from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_kegiatan = $kegiatan_dak + $kegiatan_dbhcht + $kegiatan_bankeu_kab + $kegiatan_apbd;
		
		//hitung anggaran Rp
		$anggaran_dak = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_jumlah) anggaran from t_dak where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_dbhcht = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_jumlah) anggaran from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_bankeu_kab = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_jumlah) anggaran from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_apbd) anggaran from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_anggaran = $anggaran_dak['anggaran'] + $anggaran_dbhcht['anggaran'] + $anggaran_bankeu_kab['anggaran'] + $anggaran_apbd['anggaran'];
		
		//hitung Panjar Rp
		$panjar_dak = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_jumlah) panjar from t_dak where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$panjar_dbhcht = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_jumlah) panjar from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$panjar_bankeu_kab = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_jumlah) panjar from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$panjar_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_apbd) panjar from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_panjar = $panjar_dak['panjar'] + $panjar_dbhcht['panjar'] + $panjar_bankeu_kab['panjar'] + $panjar_apbd['panjar'];
		//persen Panjar
		$persen_panjar = ($sum_panjar!=0) ? $sum_panjar * 100 / $sum_anggaran : 0;
		
		//hitung Realisasi Rp
		$realisasi_dak = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_jumlah) realisasi from t_dak where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_dbhcht = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_jumlah) realisasi from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_bankeu_kab = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_jumlah) realisasi from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_apbd) realisasi from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_realisasi = $realisasi_dak['realisasi'] + $realisasi_dbhcht['realisasi'] + $realisasi_bankeu_kab['realisasi'] + $realisasi_apbd['realisasi'];
		//persen realisasi
		$persen_realisasi = ($sum_realisasi!=0) ? $sum_realisasi * 100 / $sum_anggaran : 0;
		
		//hitung Target
		$target_dak = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_dak where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$target_dbhcht = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$target_bankeu_kab = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$target_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_target = $target_dak['target'] + $target_dbhcht['target'] + $target_bankeu_kab['target'] + $target_apbd['target'];
		$persen_target = ($sum_target!=0) ? $sum_target / $sum_kegiatan : 0;
		
		//hitung Realisasi
		$real_dak = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) reali from t_dak where $bulan and tahun='" . $tahun . "'"));
		$real_dbhcht = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) reali from t_dbhcht where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$real_bankeu_kab = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) reali from t_bankeu_kab where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$real_apbd = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) reali from t_apbd where $bulan and tahun='" . $tahun . "' and status_update='0'"));
		$sum_real = $real_dak['reali'] + $real_dbhcht['reali'] + $real_bankeu_kab['reali'] + $real_apbd['reali'];
		$persen_real = ($sum_real!=0) ? $sum_real / $sum_kegiatan : 0;
		?>
		<tr>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">I</td>
			<td>Anggaran Belanja Langsung Kabupaten Boyolali Tahun <?php echo $tahun;?></td>
			<td align="center"><?php echo number_format($sum_kegiatan, 0, ",", ".");?></td>
			<td align="center"><?php echo number_format($sum_anggaran, 0, ",", ".");?></td>
			<td align="center"><?php echo number_format($sum_panjar, 0, ",", ".");?></td>
			<td align="center"><?php echo number_format($persen_panjar, 2, ",", ".");?></td>
			<td align="center"><?php echo number_format($sum_realisasi, 0, ",", ".");?></td>
			<td align="center"><?php echo number_format($persen_realisasi, 2, ",", ".");?></td>
			<td align="center"><?php echo number_format($persen_target, 2, ",", ".");?></td>
			<td align="center"><?php echo number_format($persen_real, 2, ",", ".");?></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td>
			<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>
	</tbody>
</table>