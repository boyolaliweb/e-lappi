<script type="text/javascript">
    $(function() {
        $('#bulan').change(function(event) {
            //alert($('#bulan').val());
            urls = 'application/front/modules/kas_laporan_bankeu_desa/item_bulan.php';
            kop = $('#kop').val();
            $.post(urls, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, kop:kop, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            
            function(data) {
                $('#tableWeb').html(data);
            }
            );
    
            urls2 = 'application/front/modules/kas_laporan_bankeu_desa/item_bulan_print.php';
            kop = $('#kop').val();
            $.post(urls2, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, kop:kop, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
        $('#triwulan').change(function(event) {
            urls = 'application/front/modules/kas_laporan_bankeu_desa/item_triwulan.php';
            $.post(urls, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data) {
                $('#tableWeb').html(data);
            }
            );
            urls2 = 'application/front/modules/kas_laporan_bankeu_desa/item_triwulan_print.php';
            $.post(urls2, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
        
       
    });
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Anggaran Kas :: Bankeu Kepada Desa <small>ANGGARAN KAS BERSUMBER DARI DANA BANKEU KEPADA DESA</small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Bankeu Desa </h2>
                <ul class="buttons">
                    <button class="btn" onclick="print('tableExcel')">
                        <span class="icon-print icon-white"></span> Cetak
                    </button>
                    <a href="#" class="btn" id="excel" type="button">
                        <span class="icon-book icon-white"></span> Export to Excel
                    </a>
                </ul>                                                        
            </div>         

            <div>
                <h3>Periode </h3>
                <table>
                    <tr>
                        <td>
                            Bulanan: 
                        </td>
                        <td>
                            <select name="bulan" id="bulan">
                                <option>-- Pilih Bulan --</option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">Nopember</option>
                                <option value="12">Desember</option>
                            </select>
                        </td>
                        <td width="200px">&nbsp;</td>
                        <?php if(!isset($_SESSION['id_skpd'])){ ?>
                            <td>Ubah Kop:</td>
                            <td>
                                <select name="kop" id="kop">
                                    <option value="BAPPEDA">BAPPEDA</option>
                                    <option value="DPPKAD">DPPKAD</option>
                                    <option value="BAGIAN PEMBANGUNAN SETDA">BAGIAN PEMBANGUNAN SETDA</option>
                                </select>
                            </td>
                        <?php } ?>
                        <td>
                            <span id="loading_image" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading <img src="media/img/loader.gif"/></span>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>Triwulan: </td>
                        <td>
                            <select name="triwulan" id="triwulan">
                                <option value="1">Januari-Maret</option>
                                <option value="2">April-Juni</option>
                                <option value="3">Juli-September</option>
                                <option value="4">Oktober-Desember</option>
                            </select>
                        </td>
                    </tr>
                    -->
                </table>
                </ul>                                                        
            </div>   
            <hr>
            <div>&nbsp;</div>
			<div class="data-fluid" id="tableWeb">
            </div> 
			<div>&nbsp;</div>
			<div class="data-fluid" id="tableExcel">
            </div> 
        </div>
    </div>
</div>
