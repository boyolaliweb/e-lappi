<?php

//processing submit data
if (isset($_POST['submit'])) {
    if (isset($_POST['kegiatan'])) {
        $kegiatan = explode("_", $_POST['kegiatan']);
        $kode_permendagri = $kegiatan[0];
        $kegiatan = $kegiatan[1];
        $permendagri = explode(".", $kode_permendagri);
        $kode_urusan = $permendagri[0];
        $kode_bidang = $permendagri[1];
        $kode_program = $permendagri[2];
        $kode_kegiatan = $permendagri[3];
        //$get_kode = mysqli_fetch_array(mysqli_query($db,"select kode from t_dekon where id='".$_POST['id']."'"));
        //$kode = $get_kode['kode'];
        //$kode++;
        //$sql = mysqli_query($db,"insert into `t_dekon` (kode,bidang,program,kegiatan,lokasi,anggaran_dekon,anggaran_pendamping,anggaran_jumlah,panjar_dekon,panjar_pendamping,panjar_jumlah,realisasi_dekon,realisasi_pendamping,realisasi_jumlah,progres_target,progres_real,permasalahan,id_skpd,bulan,tahun,final,anggaran_kas) values ('".$kode."','','".$_POST['program']."','".$_POST['kegiatan']."','".$_POST['lokasi']."','".$_POST['anggaran_dekon']."','".$_POST['anggaran_pendamping']."','".$_POST['anggaran_jumlah']."','".$_POST['panjar_dekon']."','".$_POST['panjar_pendamping']."','".$_POST['panjar_jumlah']."','".$_POST['realisasi_dekon']."','".$_POST['realisasi_pendamping']."','".$_POST['realisasi_jumlah']."','".$_POST['progres_target']."','".$_POST['progres_real']."','".$_POST['permasalahan']."','".$_POST['skpd']."','".$_POST['bulan']."','".$_POST['tahun']."','0','".$_POST['anggaran_kas']."')");
        //update status
        //$sql = mysqli_query($db,"update `t_dekon` set status_update='1' where id='".$_POST['id']."'");
        $get_bidang = mysqli_fetch_array(mysqli_query($db, "select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1"));
        $get_program = mysqli_fetch_array(mysqli_query($db, "select program from m_program where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' limit 1"));
        $get_kegiatan = mysqli_fetch_array(mysqli_query($db, "select kegiatan from m_kegiatan where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' and kode_kegiatan='" . $kode_kegiatan . "' limit 1"));

        $sql = mysqli_query($db, "update `t_dekon` set
					bidang='',
					program='" . $get_program['program'] . "',
					kegiatan='" . $get_kegiatan['kegiatan'] . "',
					lokasi='" . $_POST['lokasi'] . "',
                                            subkegiatan = '" . $_POST['subkegiatan'] . "',
					anggaran_dekon='" . $_POST['anggaran_dekon'] . "',
					anggaran_pendamping='" . $_POST['anggaran_pendamping'] . "',
					anggaran_jumlah='" . $_POST['anggaran_jumlah'] . "',
					panjar_dekon='" . $_POST['panjar_dekon'] . "',
					panjar_pendamping='" . $_POST['panjar_pendamping'] . "',
					panjar_jumlah='" . $_POST['panjar_jumlah'] . "',
					realisasi_dekon='" . $_POST['realisasi_dekon'] . "',
					realisasi_pendamping='" . $_POST['realisasi_pendamping'] . "',
					realisasi_jumlah='" . $_POST['realisasi_jumlah'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					kode_bidang='" . $kode_bidang . "',
					kode_program='" . $kode_program . "',
					kode_kegiatan='" . $kode_kegiatan . "',
					id_skpd='" . $_POST['skpd'] . "',
                    anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");

        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-dekon", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-dekon", "2");
        }
    } else {
        $sql = mysqli_query($db, "update `t_dekon` set
                                        subkegiatan = '".$_POST['subkegiatan']."',
					lokasi='" . $_POST['lokasi'] . "',
					anggaran_dekon='" . $_POST['anggaran_dekon'] . "',
					anggaran_pendamping='" . $_POST['anggaran_pendamping'] . "',
					anggaran_jumlah='" . $_POST['anggaran_jumlah'] . "',
					panjar_dekon='" . $_POST['panjar_dekon'] . "',
					panjar_pendamping='" . $_POST['panjar_pendamping'] . "',
					panjar_jumlah='" . $_POST['panjar_jumlah'] . "',
					realisasi_dekon='" . $_POST['realisasi_dekon'] . "',
					realisasi_pendamping='" . $_POST['realisasi_pendamping'] . "',
					realisasi_jumlah='" . $_POST['realisasi_jumlah'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					id_skpd='" . $_POST['skpd'] . "',
                    anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");

        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-dekon", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-dekon", "2");
        }
    }
}
?>