<?php
include ("../../../../core/db.config.php");

function bulan($bulan)
{
	switch($bulan){
		case 1: $bulan="Januari - Maret";
		break;
		case 2: $bulan= "April - Juni";
		break;
		case 3: $bulan= "Juli - September";
		break;
		case 4: $bulan= "Oktober - Desember";
		break;
	} 
	return $bulan;
}
function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
<center>
    <h3>REKAPITULASI CAPAIAN PERKEMBANGAN PELAKSANAAN KEGIATAN PER SKPD<BR>SUMBER DANA TUGAS PEMBANTUAN TAHUN ANGGARAN <?php echo $tahun;?><br>PERIODE BULAN <?php echo strtoupper(bulan($id));?></h3>
</center>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2">PROGRAM/SATKER</th>
			<th rowspan="2">JML KEG</th>
			<th rowspan="2">DANA TP</th>
			<th rowspan="2">PENDAMPING</th>
			<th rowspan="2">JUMLAH</th>
			<th colspan="3">PANJAR/SP2D</th>
			<th colspan="3">SPJ</th>
			<th colspan="2">PROGRES FISIK</th>
			<th rowspan="2">KET.</th>
		</tr>
		<tr>
			<th>DANA TP</th>
			<th>DANA PENDAMPING/PENUNJANG</th>
			<th>JUMLAH</th>
			<th>DANA TP</th>
			<th>DANA PENDAMPING/PENUNJANG</th>
			<th>JUMLAH</th>
			<th>TARGET</th>
			<th>REAL</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
			<th>15</th>
		</tr>
	</thead>
	<tbody id="table">
		<?php
		if ($id == 1) {          
            $bulan = "bulan ='01' or bulan ='02' or bulan='03' ";
        } else
        if ($id == 2) {
			$bulan = "bulan ='04' or bulan ='05' or bulan='06' ";
        } else
        if ($id == 3) {
			$bulan = "bulan ='07' or bulan ='08' or bulan='09' ";          
        } else
        if ($id == 4) {
			$bulan = "bulan ='10' or bulan ='11' or bulan='12' ";
        }
		$sql = mysqli_query($db,"select distinct(program) from t_tp where $bulan and tahun='" . $tahun . "' and status_update='0'");
		
		$noBid = 1;
		$baris = 0;
		$jumlah_baris = 0;
		//make looping to get data
		$sum_kegiatan = 0;
		$sum_anggaran_tp = 0;
		$sum_anggaran_pendamping = 0;
		$sum_anggaran_jumlah = 0;
		$sum_panjar_tp = 0; 
		$sum_panjar_tp_persen = 0;
		$sum_panjar_pendamping = 0;
		$sum_panjar_pendamping_persen = 0;
		$sum_realisasi_tp = 0;
		$sum_realisasi_tp_persen = 0;
		$sum_realisasi_pendamping = 0;
		$sum_realisasi_pendamping_persen = 0;
		$sum_persen_target = 0;
		$sum_persen_real = 0;
	
		while ($rowProgram = mysqli_fetch_array($sql)) {
			echo '
			<tr>
				<td align="center"><b>' . romawi($noBid++) . '</b></td>
				<td><b>' . $rowProgram['program'] . '</b></td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			';
			//$sqlKegiatan = mysqli_query($db,"select * from t_tp where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "' and status_update='0'");
			
			$sqlSKPD = mysqli_query($db,"select distinct(id_skpd) from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "' and status_update='0'");
			
			$baris = 0;
			while ($row = mysqli_fetch_array($sqlSKPD)) {
				
				$nama_skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='".$row['id_skpd']."'"));
				$kegiatan_skpd = mysql_num_rows(mysqli_query($db,"select kegiatan from t_tp where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				//hitung anggaran Rp
				$anggaran_tp = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$anggaran_tp = $anggaran_tp['anggaran'];
				$anggaran_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$anggaran_pendamping = $anggaran_pendamping['anggaran'];
				$anggaran_jumlah = $anggaran_tp + $anggaran_pendamping;
				$panjar_tp = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$panjar_tp = $panjar_tp['anggaran'];
				$panjar_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$panjar_pendamping = $panjar_pendamping['anggaran'];
				$panjar_jumlah = $panjar_tp + $panjar_pendamping;
				$realisasi_tp = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_tp) anggaran from t_tp where program='" . $rowProgram['program'] . "' and $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$realisasi_tp = $realisasi_tp['anggaran'];
				$realisasi_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_pendamping) anggaran from t_tp where program='" . $rowProgram['program'] . "' and  $bulan and tahun='" . $tahun . "'  and status_update='0' and id_skpd='".$row['id_skpd']."'"));
				$realisasi_pendamping = $realisasi_pendamping['anggaran'];
				$realisasi_jumlah = $realisasi_tp + $realisasi_pendamping;
				//realisasi dan target
				$target_tp = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_tp where program='" . $rowProgram['program'] . "' and id_skpd='".$row['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
				$real_tp = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) reali from t_tp where program='" . $rowProgram['program'] . "' and id_skpd='".$row['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
				$persen_target = ($target_tp['target']!=0) ? $target_tp['target'] / $kegiatan_skpd : 0;
				$persen_real = ($real_tp['reali']!=0) ? $real_tp['reali'] / $kegiatan_skpd : 0;
				echo '
				<tr>
					
					<td align="center">-</td>
					<td>' . $nama_skpd['nama'] . '</td>
					<td align="right">' . number_format($kegiatan_skpd, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_tp, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format($anggaran_jumlah, 0, ",", ".") . '</td>
					<td align="right">' . number_format($panjar_tp, 0, ",", ".") . '</td>					
					<td align="right">' . number_format($panjar_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format($panjar_jumlah, 0, ",", ".") . '</td>
					<td align="right">' . number_format($realisasi_tp, 0, ",", ".") . '</td>
					<td align="right">' . number_format($realisasi_pendamping, 0, ",", ".") . '</td>
					<td align="right">' . number_format($realisasi_jumlah, 0, ",", ".") . '</td>
					<td align="right">' . number_format($persen_target, 2, ",", ".") . '</td>
					<td align="right">' . number_format($persen_real, 2, ",", ".") . '</td>
					<td>&nbsp;</td>							
				</tr>
				';
				
				$baris = $baris + 1;
				
				
				$sum_kegiatan = $sum_kegiatan + $kegiatan_skpd;
				$sum_anggaran_tp = $sum_anggaran_tp + $anggaran_tp;
				$sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping;
				$sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
				$sum_panjar_tp = $sum_panjar_tp + $panjar_tp;
				$sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping;
				$sum_panjar_jumlah = $sum_panjar_jumlah + $panjar_jumlah;
				$sum_realisasi_tp = $sum_realisasi_tp + $realisasi_tp;
				$sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping;
				$sum_realisasi_jumlah = $sum_realisasi_jumlah + $realisasi_jumlah;
				$sum_persen_target = $sum_persen_target + $persen_target;
				$sum_persen_real = $sum_persen_real + $persen_real;
			}
			$jumlah_baris = $jumlah_baris + $baris;
		}
		echo '
			<tr>
				<td>&nbsp;</td>
				<td align="center"><b>JUMLAH TOTAL</b></td>
				<td align="right"><b>' . number_format($sum_kegiatan, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_tp, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_tp, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_tp, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_target!=0) ? $sum_persen_target / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_real!=0) ? $sum_persen_real / $jumlah_baris : 0, 2, ",", ".") . '</b></td>
				<td align="right">&nbsp;</td>
													
			</tr>
			';
		?>
	</tbody>
</table>