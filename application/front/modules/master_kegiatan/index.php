<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Master Kegiatan <small>HALAMAN UNTUK MENGATUR DATA MASTER KEGIATAN</small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Master Kegiatan</h2>
                <ul class="buttons">
                    <a href="index.php?val=master-kegiatan-form" class="btn" type="button">
                        <span class="icon-plus icon-white"></span> Tambah Kegiatan
                    </a>
                </ul>                                                        
            </div>                
            <div class="data-fluid">
                <table class="table fpTable lcnp dataTable" cellpadding="0" cellspacing="0" width="100%" id="DataTables_Table_2" aria-describedby="DataTables_Table_2_info" style="width: 100%;">
                    <thead>
                        <tr role="row">
                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 50px;">No</th>
                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 122px;">Kode Permendagri</th>
                            <th width="20%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1"  style="width: 212px;">Bidang</th>
                            <th width="20%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 192px;">Program</th>
                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 192px;">Kegiatan</th>
                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Tahun</th>

                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Opsi</th>
                        </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
                        $sql = $db->query("select * from m_kegiatan order by id asc");
                        $sql->data_seek(0);
                        $no = 1;
                        while ($row = $sql->fetch_assoc()) {
                            $sql_bidang = $db->query("select bidang from m_bidang where kode_urusan='" . $row['kode_urusan'] . "' and kode_bidang='" . $row['kode_bidang'] . "' limit 1");
                            $sql_bidang->data_seek(0);
                            $get_bidang = $sql_bidang->fetch_assoc();

                            $sql_program = $db->query("select program from m_program where kode_urusan='" . $row['kode_urusan'] . "' and kode_bidang='" . $row['kode_bidang'] . "' and kode_program='" . $row['kode_program'] . "' limit 1");
                            $sql_program->data_seek(0);
                            $get_program = $sql_program->fetch_assoc();
                            echo '
                                <tr class="odd">
                                    <td>' . $no++ . '</td>
                                    <td>' . $row['kode_urusan'] . '.' . $row['kode_bidang'] . '.' . $row['kode_program'] . '.' . $row['kode_kegiatan'] . '</td>
                                    <td>' . $get_bidang['bidang'] . '</td>
                                    <td>' . $get_program['program'] . '</td>
                                    <td>' . $row['kegiatan'] . '</td>
                                    <td>' . $row['tahun'] . '</td>
                                    <td>
                                    <a href="index.php?val=master-kegiatan-edit&id=' . $row['id'] . '" class="button green">
                                        <div class="icon"><span class="ico-pencil"></span></div>
                                    </a>
                                    <a href="index.php?val=master-kegiatan-delete&id=' . $row['id'] . '" class="button red">
                                        <div class="icon"><span class="ico-remove"></span></div>
                                    </a>                                              
                                    </td>
                                </tr>
                                ';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
        </div>

    </div>
</div>

