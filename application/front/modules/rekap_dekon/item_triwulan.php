<?php
include ("../../../../core/db.config.php");

function romawi($num) {
    // Make sure that we only use the integer portion of the value
    $n = intval($num);
    $result = '';

    // Declare a lookup array that we will use to traverse the number:
    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

    foreach ($lookup as $roman => $value) {
        // Determine the number of matches
        $matches = intval($n / $value);

        // Store that many characters
        $result .= str_repeat($roman, $matches);

        // Substract that from the number
        $n = $n % $value;
    }

    // The Roman numeral should be built, return it
    return $result;
}

$id = $_POST['id'];
$tahun = $_POST['tahun'];
$skpd = $_POST['skpd'];
?>
Triwulan: <?php echo $id ?> | Tahun: <?php echo $tahun ?>
<table cellpadding="0" border="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th rowspan="2">NO</th>
			<th rowspan="2">PROGRAM/SATKER</th>
			<th rowspan="2">JML KEG</th>
			<th rowspan="2">DANA DEKON</th>
			<th rowspan="2">PENDAMPING</th>
			<th rowspan="2">JUMLAH</th>
			<th colspan="3">PANJAR/SP2D</th>
			<th colspan="3">SPJ</th>
			<th colspan="2">PROGRES FISIK</th>
			<th rowspan="2">KET.</th>
		</tr>
		<tr>
			<th>DANA DEKON</th>
			<th>DANA PENDAMPING/PENUNJANG</th>
			<th>JUMLAH</th>
			<th>DANA DEKON</th>
			<th>DANA PENDAMPING/PENUNJANG</th>
			<th>JUMLAH</th>
			<th>TARGET</th>
			<th>REAL</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
			<th>15</th>
		</tr>
	</thead>
	<tbody id="table">
		<?php
		if ($id == 1) {          
            $bulan = " (bulan ='01' or bulan ='02' or bulan='03') ";
        } else
        if ($id == 2) {
			$bulan = " (bulan ='04' or bulan ='05' or bulan='06') ";
        } else
        if ($id == 3) {
			$bulan = " (bulan ='07' or bulan ='08' or bulan='09') ";          
        } else
        if ($id == 4) {
			$bulan = " (bulan ='10' or bulan ='11' or bulan='12') ";
        }
		
		$sql = mysqli_query($db,"select distinct(id_skpd) from t_dekon where $bulan and tahun='" . $tahun . "' and status_update='0'");
		
		$noBid = 1;
		$baris = 0;
		$jumlah_baris = 0;
		//make looping to get data
		$sum_kegiatan = 0;
		$sum_anggaran_dekon = 0;
		$sum_anggaran_pendamping = 0;
		$sum_anggaran_jumlah = 0;
		$sum_panjar_dekon = 0; 
		$sum_panjar_dekon_persen = 0;
		$sum_panjar_pendamping = 0;
		$sum_panjar_pendamping_persen = 0;
		$sum_realisasi_dekon = 0;
		$sum_realisasi_dekon_persen = 0;
		$sum_realisasi_pendamping = 0;
		$sum_realisasi_pendamping_persen = 0;
		$sum_persen_target = 0;
		$sum_persen_real = 0;
		$no=0;
	
		while ($rowProgram = mysqli_fetch_array($sql)) {
			$nama_skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='".$rowProgram['id_skpd']."' limit 1"));
			$sum_kegiatan = mysql_num_rows(mysqli_query($db,"select id from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$anggaran_dekon = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_dekon) anggaran from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$anggaran_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(anggaran_pendamping) anggaran from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$anggaran_jumlah = $anggaran_dekon['anggaran'] + $anggaran_pendamping['anggaran'];
			
			$panjar_dekon = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_dekon) panjar from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$panjar_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(panjar_pendamping) panjar from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$panjar_jumlah = $panjar_dekon['panjar'] + $panjar_pendamping['panjar'];
			
			$realisasi_dekon = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_dekon) anggaran from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$realisasi_pendamping = mysqli_fetch_array(mysqli_query($db,"select sum(realisasi_pendamping) anggaran from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$realisasi_jumlah = $realisasi_dekon['anggaran'] + $realisasi_pendamping['anggaran'];
			
			$target_dekon = mysqli_fetch_array(mysqli_query($db,"select sum(progres_target) target from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			$real_dekon = mysqli_fetch_array(mysqli_query($db,"select sum(progres_real) target from t_dekon where id_skpd='".$rowProgram['id_skpd']."' and $bulan and tahun='" . $tahun . "' and status_update='0'"));
			
			echo '
			<tr>
				<td align="center"><b>' . ($noBid++) . '</b></td>
				<td><b>' . $nama_skpd['nama'] . '</b></td>
				<td align="right">' . number_format($sum_kegiatan, 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_dekon['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_pendamping['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($anggaran_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_dekon['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_pendamping['panjar'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($panjar_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_dekon['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_pendamping['anggaran'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($realisasi_jumlah, 0, ",", ".") . '</td>
				<td align="right">' . number_format($target_dekon['target'], 0, ",", ".") . '</td>
				<td align="right">' . number_format($real_dekon['target'], 0, ",", ".") . '</td>
				<td>&nbsp;</td>
			</tr>
			';
			//$sqlKegiatan = mysqli_query($db,"select * from t_dekon where program='" . $rowProgram['program'] . "' and tahun='" . $tahun . "' and status_update='0'");
			
			$sqlSKPD = mysqli_query($db,"select * from t_dekon where id_skpd='" . $rowProgram['id_skpd'] . "' and $bulan and tahun='" . $tahun . "' and status_update='0'");
			$no = $no + 1;
			$baris = $baris + $sum_kegiatan;
			$sum_anggaran_dekon = $sum_anggaran_dekon + $anggaran_dekon['anggaran'];
			$sum_anggaran_pendamping = $sum_anggaran_pendamping + $anggaran_pendamping['anggaran'];
			$sum_anggaran_jumlah = $sum_anggaran_jumlah + $anggaran_jumlah;
			$sum_panjar_dekon = $sum_panjar_dekon + $panjar_dekon['panjar'];
			$sum_panjar_pendamping = $sum_panjar_pendamping + $panjar_pendamping['panjar'];
			$sum_panjar_jumlah = $sum_panjar_jumlah + $panjar_jumlah;
			$sum_realisasi_dekon = $sum_realisasi_dekon + $realisasi_dekon['anggaran'];
			$sum_realisasi_pendamping = $sum_realisasi_pendamping + $realisasi_pendamping['anggaran'];
			$sum_realisasi_jumlah = $sum_realisasi_jumlah + $realisasi_jumlah;
			$sum_persen_target = $sum_persen_target + $target_dekon['target'];
			$sum_persen_real = $sum_persen_real + $real_dekon['target'];
		
			
		}
		echo '
			<tr>
				<td></td>
				<td align="center"><b>JUMLAH TOTAL</b></td>
				<td align="right"><b>' . number_format($baris, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_dekon, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_anggaran_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_dekon, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_panjar_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_dekon, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_pendamping, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format($sum_realisasi_jumlah, 0, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_target!=0) ? $sum_persen_target / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right"><b>' . number_format(($sum_persen_real!=0) ? $sum_persen_real / $no : 0, 2, ",", ".") . '</b></td>
				<td align="right">&nbsp;</td>
													
			</tr>
			';
		?>
	</tbody>
</table>
