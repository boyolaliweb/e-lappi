<div class="page-header">

    <div class="icon">

        <span class="ico-layout-7"></span>

    </div>

    <h1>Master SKPD <small>HALAMAN UNTUK MENGATUR DATA MASTER SKPD</small></h1>

</div>

<form action="" method="post">

    <div class="row-fluid">

        <div class="span6">

            <div class="block">

                <div class="head">

                    <h2>Ubah Data SKPD</h2>

                </div>

                <div class="data-fluid">

                    <div class="row-form">

                        <div class="span3">Jenis SKPD</div>

                        <div class="span9">

                            <select name="jenis_skpd">

                                <option value="SKPD">SKPD</option>

                                <option value="Kecamatan">Kecamatan</option>

                                <option value="Kelurahan">Kelurahan</option>

                            </select>

                        </div>

                    </div>

                    <div class="row-form">

                        <div class="span3">Kode SKPD</div>

                        <div class="span9"><input type="text" name="kode" required="true"></div>

                    </div>

                    <div class="row-form">

                        <div class="span3">Nama SKPD:</div>

                        <div class="span9"><input type="text" name="nama" required="true"></div>

                    </div>

                    <div class="well">

                        <div class="row-form">

                            <div class="span3">Kepala SKPD</div>

                            <div class="span9"><input type="text" name="kepala"></div>

                        </div>

                        <div class="row-form">

                            <div class="span3">Pangkat Kepala SKPD</div>

                            <div class="span9"><input type="text" name="pangkat"></div>

                        </div>

                        <div class="row-form">

                            <div class="span3">NIP Kepala SKPD</div>

                            <div class="span9"><input type="text" name="nip"></div>

                        </div>

                    </div>

                    <div class="row-form">

                        <div class="span12">

                            <input type="submit" name="submit" value="Simpan" class="btn btn-large">

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="span6">

            <div class="block">

                <div class="head">

                    <h2>Data Login</h2>

                </div>

                <div class="data-fluid">

                </div>
                <div class="row-form">

                    <div class="span3">Email</div>

                    <div class="span9">

                        <div class="input-prepend">

                            <span class="add-on"><i class="icon-user icon-white"></i></span>

                            <input type="email" name="email" style="width: 331px;" required="true">

                        </div>

                    </div>
                </div>
                <div class="row-form">

                    <div class="span3">Username</div>

                    <div class="span9">

                        <div class="input-prepend">

                            <span class="add-on"><i class="icon-user icon-white"></i></span>

                            <input type="text" name="username" style="width: 331px;" required="true">

                        </div>

                    </div>
                </div>
                <div class="row-form">

                    <div class="span3">Password</div>

                    <div class="span9">

                        <div class="input-prepend">

                            <span class="add-on"><i class="icon-lock icon-white"></i></span>

                            <input type="password" name="password" style="width: 331px;" required="true">

                        </div>

                    </div>

                </div>

            </div>

        </div>

</form>

</div>



<?php

//processing submit data

if (isset($_POST['submit'])) {

    $sql = $db->query("insert into m_skpd set

                    kode = '" . $_POST['kode'] . "',

                    nama = '" . $_POST['nama'] . "',

                    kepala = '" . $_POST['kepala'] . "',

					pangkat = '" . $_POST['pangkat'] . "',

					nip = '" . $_POST['nip'] . "',

                    jenis = '" . $_POST['jenis_skpd'] . "',

                    email = '" . $_POST['email'] . "',

                    username = '" . $_POST['username'] . "',

                    password = '" . $_POST['password'] . "'

            ");

    if ($sql) {

        alert_success();

        redirect("master-skpd");

    }

}

?>