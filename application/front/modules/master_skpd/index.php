<div class="page-header">

    <div class="icon">

        <span class="ico-layout-7"></span>

    </div>

    <h1>Master SKPD <small>HALAMAN UNTUK MENGATUR DATA MASTER SKPD</small></h1>

</div>

<div class="row-fluid">

    <div class="span12">

        <div class="block">

            <div class="head dblue">

                <div class="icon"><span class="ico-layout-9"></span></div>

                <h2>Master SKPD</h2>

                <ul class="buttons">

                    <a href="master-skpd-form" class="btn" type="button">

                        <span class="icon-plus icon-white"></span> Tambah SKPD

                    </a>

                </ul>                                                        

            </div>                

            <div class="data-fluid">

                <table class="table fpTable lcnp dataTable" cellpadding="0" cellspacing="0" width="100%" id="DataTables_Table_2" aria-describedby="DataTables_Table_2_info" style="width: 100%;">

                    <thead>

                        <tr role="row">

                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 50px;">No</th>

                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 122px;">Kode SKPD</th>

                            <th width="20%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1"  style="width: 212px;">Nama SKPD</th>

                            <th width="20%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 192px;">Kepala SKPD</th>

                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 192px;">Jenis</th>

                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Email</th>

                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Username</th>

                            <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Password</th>

                            <th width="15%" class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_2" rowspan="1" colspan="1" style="width: 100px;">Opsi</th>

                        </tr>

                    </thead>



                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                        <?php

                        $sql = $db->query("select * from m_skpd order by id asc");

                        $sql->data_seek(0);

                        $no = 1;

                        while ($row = $sql->fetch_assoc()) {

                            echo '

                                <tr class="odd">

                                    <td>' . $no++ . '</td>

                                    <td>' . $row['kode'] . '</td>

                                    <td>' . $row['nama'] . '</td>

                                    <td>' . $row['kepala'] . '<br>

									<i>

									Pangkat: ' . $row['pangkat'] . '<br>

									NIP: ' . $row['nip'] . '								

									</i>

									</td>

                                    <td>' . $row['jenis'] . '</td>

                                    <td>' . $row['email'] . '</td>
                                    
									<td>' . $row['username'] . '</td>

									<td>' . $row['password'] . '</td>

                                    <td>

                                    <a href="index.php?val=master-skpd-edit&id=' . $row['id'] . '" class="button green">

                                        <div class="icon"><span class="ico-pencil"></span></div>

                                    </a>

                                    <a href="index.php?val=master-skpd-delete&id=' . $row['id'] . '" class="button red">

                                        <div class="icon"><span class="ico-remove"></span></div>

                                    </a>                                              

                                    </td>

                                </tr>

                                ';

                        }

                        ?>

                    </tbody>

                </table>

            </div> 

        </div>

    </div>

</div>



