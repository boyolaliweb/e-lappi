<?php

//processing submit data
if (isset($_POST['submit'])) {
    if (isset($_POST['kegiatan'])) {
        $kegiatan = explode("_", $_POST['kegiatan']);
        $kode_permendagri = $kegiatan[0];
        $kegiatan = $kegiatan[1];
        $permendagri = explode(".", $kode_permendagri);
        $kode_urusan = $permendagri[0];
        $kode_bidang = $permendagri[1];
        $kode_program = $permendagri[2];
        $kode_kegiatan = $permendagri[3];
        //$get_kode = mysqli_fetch_array($db->query("select kode from t_dak where id='".$_POST['id']."'"));
        //$kode = $get_kode['kode'];
        //$kode++;
        //$sql = $db->query("update `t_dak` (kode,bidang,program,kegiatan,lokasi,anggaran_dak,anggaran_pendamping,anggaran_jumlah,panjar_dak,panjar_pendamping,panjar_jumlah,realisasi_dak,realisasi_pendamping,realisasi_jumlah,progres_target,progres_real,permasalahan,id_skpd,bulan,tahun,final,anggaran_kas) values ('".$kode."','".$_POST['bidang']."','".$_POST['program']."','".$_POST['kegiatan']."','".$_POST['lokasi']."','".$_POST['anggaran_dak']."','".$_POST['anggaran_pendamping']."','".$_POST['anggaran_jumlah']."','".$_POST['panjar_dak']."','".$_POST['panjar_pendamping']."','".$_POST['panjar_jumlah']."','".$_POST['realisasi_dak']."','".$_POST['realisasi_pendamping']."','".$_POST['realisasi_jumlah']."','".$_POST['progres_target']."','".$_POST['progres_real']."','".$_POST['permasalahan']."','".$_POST['skpd']."','".$_POST['bulan']."','".$_POST['tahun']."','0','".$_POST['anggaran_kas']."')");
        //update status
        //$sql = $db->query("update `t_dak` set status_update='1' where id='".$_POST['id']."'");
        /*
          $get_bidang = mysqli_fetch_array($db->query("select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1"));
          $get_program = mysqli_fetch_array($db->query("select program from m_program where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' limit 1"));
          $get_kegiatan = mysqli_fetch_array($db->query("select kegiatan from m_kegiatan where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' and kode_kegiatan='" . $kode_kegiatan . "' limit 1"));
         */
        $q_bidang = $db->query("select bidang from m_bidang where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' limit 1");
        $get_bidang = $q_bidang->fetch_assoc();
        $q_program = $db->query("select program from m_program where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' limit 1");
        $get_program = $q_program->fetch_assoc();
        $q_kegiatan = $db->query("select kegiatan from m_kegiatan where kode_urusan='" . $kode_urusan . "' and kode_bidang='" . $kode_bidang . "' and kode_program='" . $kode_program . "' and kode_kegiatan='" . $kode_kegiatan . "' limit 1");
        $get_kegiatan = $q_kegiatan->fetch_assoc();

        $sql = $db->query("update `t_dak` set
					bidang='" . $get_bidang['bidang'] . "',
					program='" . $get_program['program'] . "',
					kegiatan='" . $get_kegiatan['kegiatan'] . "',
                                        subkegiatan = '".$_POST['subkegiatan']."',
					lokasi='" . $_POST['lokasi'] . "',
					volume='" . $_POST['volume'] . "',
					satuan='" . $_POST['satuan'] . "',
					penerima='" . $_POST['penerima'] . "',
					anggaran_dak='" . $_POST['anggaran_dak'] . "',
					anggaran_pendamping='" . $_POST['anggaran_pendamping'] . "',
					anggaran_jumlah='" . $_POST['anggaran_jumlah'] . "',
					panjar_dak='" . $_POST['panjar_dak'] . "',
					panjar_pendamping='" . $_POST['panjar_pendamping'] . "',
					panjar_jumlah='" . $_POST['panjar_jumlah'] . "',
					realisasi_dak='" . $_POST['realisasi_dak'] . "',
					realisasi_pendamping='" . $_POST['realisasi_pendamping'] . "',
					realisasi_jumlah='" . $_POST['realisasi_jumlah'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					kode_masalah='" . $_POST['kode_masalah'] . "',
					kode_bidang='" . $kode_bidang . "',
					kode_program='" . $kode_program . "',
					kode_kegiatan='" . $kode_kegiatan . "',
					id_skpd='" . $_POST['skpd'] . "',
					anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");

        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-dak", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-dak", "2");
        }
    } else {
        //no isset kegiatan
        $sql = $db->query("update `t_dak` set
                                        subkegiatan = '".$_POST['subkegiatan']."',
					lokasi='" . $_POST['lokasi'] . "',
					volume='" . $_POST['volume'] . "',
					satuan='" . $_POST['satuan'] . "',
					penerima='" . $_POST['penerima'] . "',
					anggaran_dak='" . $_POST['anggaran_dak'] . "',
					anggaran_pendamping='" . $_POST['anggaran_pendamping'] . "',
					anggaran_jumlah='" . $_POST['anggaran_jumlah'] . "',
					panjar_dak='" . $_POST['panjar_dak'] . "',
					panjar_pendamping='" . $_POST['panjar_pendamping'] . "',
					panjar_jumlah='" . $_POST['panjar_jumlah'] . "',
					realisasi_dak='" . $_POST['realisasi_dak'] . "',
					realisasi_pendamping='" . $_POST['realisasi_pendamping'] . "',
					realisasi_jumlah='" . $_POST['realisasi_jumlah'] . "',
					progres_target='" . $_POST['progres_target'] . "',
					progres_real='" . $_POST['progres_real'] . "',
					permasalahan='" . $_POST['permasalahan'] . "',
					kode_masalah='" . $_POST['kode_masalah'] . "',
					id_skpd='" . $_POST['skpd'] . "',
					anggaran_kas='" . $_POST['anggaran_kas'] . "'
					where kode='" . $_POST['kode'] . "' and bulan >= '" . $_POST['bulan'] . "'");
        if ($sql) {
            echo "<script type='text/javascript'>alertify.success(\"Berhasil melakukan proses\");</script>";
            redirect_time("laporan-dak", "2");
        } else {
            echo "<script type='text/javascript'>alertify.error(\"Gagal melakukan proses\");</script>";
            redirect_time("laporan-dak", "2");
        }
    }
}
?>