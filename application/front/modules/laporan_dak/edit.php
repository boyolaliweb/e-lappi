<?php if ((isset($_SESSION['level'])&&$_SESSION['level']=="skpd") && (isset($_SESSION['status_entry'])&&$_SESSION['status_entry']=="close")) exit('Entry data telah ditutup'); ?>
<?php
$q_default = $db->query("select * from t_dak where id='" . $_GET['id'] . "' limit 1");
$default = $q_default->fetch_assoc();
?>
<style>
    .row-form{padding:5px !important;}
</style>
<script type="text/javascript">
	function showUrusan(){
			$.ajax({
			type: "POST",
			url: "application/front/modules/laporan_dak/item_bidang.php",
			//url: "item_program.php",
			data: {id: true},
			success: function(response){
				//alert(response);
				$("#bidang").html(response);
			},
			dataType:"html"
			});
			return false;
	}
    $(function() {
		
         $('#bidang').change(function(event) {
			bidang = $('#select_bidang').val();
			//alert(bidang);
            urls = 'application/front/modules/laporan_dak/item_program.php';
            $.post(urls, {id: bidang},
            function(data) {
                $('#program').html(data);
            }
            );
        });
        $('#program').change(function(event) {
			program = $('#select_program').val();
			//alert(program);
            urls = 'application/front/modules/laporan_dak/item_kegiatan.php';
            $.post(urls, {id: program},
            function(data) {
                $('#kegiatan').html(data);
            }
            );
        });
    });
</script>
<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Ubah Laporan Dana Alokasi Khusus (DAK) <small>HALAMAN UNTUK MENGELOLA LAPORAN DAK</small></h1>  
</div>
<div class="row-fluid">
    <!-- Back button -->
    <div class="span12" align="right"><a href='javascript:history.go(-1)' class="btn ">&laquo; Kembali</a></div>
</div>
<form action="laporan-dak-update" method="POST" id="form">
    <input type="hidden" name="id" value="<?php echo $default['id']; ?>">
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <div class="data-fluid">
                    <div class="head"><h2>Bidang / Program / Kegiatan</h2></div>   
                    <div class="row-form">
                        <div class="span3">Bulan</div>
                        <div class="span9">
                            <input type="hidden" name="kode" value="<?php echo $default['kode']; ?>">
                            <input type="hidden" name="bulan" value="<?php echo $default['bulan']; ?>">
                            <input type="text" name="month" required="true" readonly="readonly" value="<?php echo bulan($default['bulan']); ?>">
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Tahun</div>
                        <div class="span9"><input type="text" name="tahun" required="true" readonly="readonly" value="<?php echo $_SESSION['tahun']; ?>"></div>
                    </div>  
                    <div class="row-form">
                        <div class="span3">SKPD</div>
                        <div class="span9">
                            <select name="skpd" class="select" style="width: 100%;">
                                <?php
                                //check apakah login sebagai skpd / administrator
                                if (isset($_SESSION['id_skpd']) && $_SESSION['id_skpd'] != "") {
                                    $sqlSKPD = $db->query("select * from m_skpd where id='" . $_SESSION['id_skpd'] . "' limit  1");
                                } else {
                                    $sqlSKPD = $db->query("select * from m_skpd order by kode asc");
                                }
                                while ($rowSPKD = $sqlSKPD->fetch_assoc()) {
                                    $select1 = ($rowSPKD['id'] == $default['id_skpd']) ? "selected" : "";
                                    echo '<option value="' . $rowSPKD['id'] . '" ' . $select1 . '>' . $rowSPKD['nama'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                   			
                    <div class="row-form">
                        <div class="span3">Bidang Kegiatan</div>
                        <div class="span9" id="bidang">
                            <?php echo $default['bidang'];?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="btn btn-warning" onClick="showUrusan();"><i class="ico-edit"></i> Ubah Bidang</span>
                        </div>
                    </div>
                    <div class="row-form">
                        <div class="span3">Program</div>
                        <div class="span9" id="program">
                            <!--<select name="program" id="program" style="width: 100%;" required="true">	
                                <option value="<?php echo $default['program']; ?>"><?php echo $default['program']; ?></option>						
                            </select>-->
                           <?php echo $default['program']; ?>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Kegiatan</div>
                        <div class="span9" id="kegiatan">
                            <!--<select name="kegiatan" id="kegiatan" class="select" style="width: 100%;" required="true">
                                <option value="<?php echo $default['kegiatan']; ?>"><?php echo $default['kegiatan']; ?></option>
                            </select>-->
                            <?php echo $default['kegiatan']; ?>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Sub Kegiatan</div>
                        <div class="span9">
                           <input type="text" name="subkegiatan" style="width: 100%;" data-index="3" value="<?php echo $default['subkegiatan']; ?>">
                            <span> Subkegiatan boleh dikosongkan. Tuliskan detail subkegiatan di atas ini.</span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Lokasi / Sasaran</div>
                        <div class="span9">
                            <input type="text" name="lokasi" value="<?php echo $default['lokasi']; ?>" style="width:100%;" data-index="0">
                        </div>              
                    </div>
					<div class="row-form">
                        <div class="span3">Volume</div>
                        <div class="span9">
                            <input type="text" name="volume" value="<?php echo $default['volume']; ?>" style="width:50%;" data-index="1">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Satuan</div>
                        <div class="span9">
                            <select name="satuan"  style="width:50%;">
                                <option value="">-- Pilih Satuan --</option>
                                <?php
                                $sql_satuan = $db->query("select * from m_satuan order by id asc");
                                while ($r_satuan = $sql_satuan->fetch_assoc()) {
                                    if ($r_satuan['keterangan'] == "-" || empty($r_satuan['keterangan'])) {
                                        $ket = "";
                                    } else {
                                        $ket = '(' . $r_satuan['keterangan'] . ')';
                                    }
									if($r_satuan['nama']==$default['satuan']){
										 echo '<option value="' . $r_satuan['nama'] . '" SELECTED>' . $r_satuan['nama'] . ' ' . $ket . '</option>';
									}else{
										echo '<option value="' . $r_satuan['nama'] . '">' . $r_satuan['nama'] . ' ' . $ket . '</option>';
									}
                                   
                                }
                                ?>
                            </select>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Jumlah Penerima</div>
                        <div class="span9">
                            <input type="text" name="penerima" value="<?php echo $default['penerima']; ?>" style="width:50%;" data-index="2">
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran</h2></div>
                    <div class="row-form">
                        <div class="span3">DAK</div>
                        <div class="span9">
                            <input type="text" name="anggaran_dak" value="<?php echo $default['anggaran_dak']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="3">
                            &nbsp;&nbsp;
                            <span id="anggaran_dak_1" class="kanan"></span>
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Dana Pendamping</div>
                        <div class="span9">
                            <input type="text" name="anggaran_pendamping" value="<?php echo $default['anggaran_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" data-index="4">
                            &nbsp;&nbsp;
                            <span id="anggaran_pendamping_1" class="kanan"></span> 
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">JUMLAH</div>
                        <div class="span9">
                            <input type="text" name="anggaran_jumlah" value="" style="width:200px;" readonly="readonly">
                            &nbsp;&nbsp;
                            <span id="anggaran_jumlah_1" class="kanan"></span> 
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">                
            <div class="block">
                <hr>
                <div class="data-fluid">
                    <div class="head"><h2>Anggaran Kas</h2></div>
                    <div class="row-form">
                        <div class="span3">Anggaran Kas</div>
                        <div class="span9">
                            <input type="text" name="anggaran_kas" value="<?php echo $default['anggaran_kas']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')"  data-index="5">
                            &nbsp;&nbsp;
                            <span id="anggaran_kas" class="kanan"></span>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">                
                <div class="block">
                    <div class="data-fluid">
                        <div class="head"><hr><h2>SP2D</h2></div>
                        <div class="row-form">
                            <div class="span3">DAK</div>
                            <div class="span9">
                                <input type="text" name="panjar_dak" value="<?php echo $default['panjar_dak']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                            this.value = this.value.replace(/\D/g, '')" data-index="6">
                                &nbsp;&nbsp;
                                <span id="panjar_dak_1" class="kanan"></span> 
                            </div>              
                        </div>
                        <div class="row-form">
                            <div class="span3">Dana Pendamping</div>
                            <div class="span9">
                                <input type="text" name="panjar_pendamping" value="<?php echo $default['panjar_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                            this.value = this.value.replace(/\D/g, '')" data-index="7">
                                &nbsp;&nbsp;
                                <span id="panjar_pendamping_1" class="kanan"></span> 
                            </div>              
                        </div>
                        <div class="row-form">
                            <div class="span3">JUMLAH</div>
                            <div class="span9">
                                <input type="text" name="panjar_jumlah" value="" style="width:200px;" readonly="readonly">
                                &nbsp;&nbsp;
                                <span id="panjar_jumlah_1" class="kanan"></span> 
                            </div>              
                        </div>

                        <div class="head"><hr><h2>Realisasi Pertanggungjawaban (SPJ)</h2></div>
                        <div class="row-form">
                            <div class="span3">DAK</div>
                            <div class="span9">
                                <input type="text" name="realisasi_dak" value="<?php echo $default['realisasi_dak']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                            this.value = this.value.replace(/\D/g, '')" data-index="8">
                                &nbsp;&nbsp;
                                <span id="realisasi_dak_1" class="kanan"></span>
                            </div>              
                        </div>
                        <div class="row-form">
                            <div class="span3">Dana Pendamping</div>
                            <div class="span9">
                                <input type="text" name="realisasi_pendamping" value="<?php echo $default['realisasi_pendamping']; ?>" style="width:200px;" onkeyup="if (/\D/g.test(this.value))
                            this.value = this.value.replace(/\D/g, '')" data-index="9">
                                &nbsp;&nbsp;
                                <span id="realisasi_pendamping_1" class="kanan"></span>
                            </div>              
                        </div>
                        <div class="row-form">
                            <div class="span3">JUMLAH</div>
                            <div class="span9">
                                <input type="text" name="realisasi_jumlah" value="" style="width:200px;" readonly="readonly">
                                &nbsp;&nbsp;
                                <span id="realisasi_jumlah_1" class="kanan"></span> 
                            </div>              
                        </div>

                        <div class="head"><hr><h2>Target Fisik</h2></div>
                        <div class="row-form">
                        <div class="span3">Target (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_target" maxlength="6" style="width:60px;" class="decimal" value="<?php echo $default['progres_target']; ?>" data-index="10" required="true">
                        </div>              
                    </div>
                    <div class="row-form">
                        <div class="span3">Real (%)</div>
                        <div class="span9">
                            <input type="text" name="progres_real" value="<?php echo $default['progres_real']; ?>" maxlength="6" style="width:60px;" class="decimal" data-index="11" required="true">
                        </div> 
						<div class="span12">*) Gunakan titik untuk tanda pemisah koma</div>	
                    </div>
                        <div class="head"><hr><h2>Keterangan / Permasalahan</h2></div>
                        <div class="row-form">
                            <div class="span3">Keterangan</div>
                            <div class="span9">
                                <input type="text" name="permasalahan" value="<?php echo $default['permasalahan']; ?>" data-index="12">
                            </div>              
                        </div>
						<div class="row-form">
                        <div class="span3">Kodefikasi Masalah</div>
                        <div class="span9">
                            <select name="kode_masalah"  style="width:100%;">
                                <option>-- Pilih Kodefikasi --</option>
                                <?php
                                $sql_kode = $db->query("select * from m_kode_masalah order by id asc");
                                while ($r_kode = mysqli_fetch_array($sql_kode)) {
									if($r_kode['id']==$default['kode_masalah']){
										echo '<option value="' . $r_kode['id'] . '" SELECTED>' . $r_kode['id'] . '. ' . $r_kode['masalah'] . '</option>';
									}else{
										echo '<option value="' . $r_kode['id'] . '">' . $r_kode['id'] . '. ' . $r_kode['masalah'] . '</option>';
									}
                                    
                                }
                                ?>
                            </select>
                        </div>              
                    </div>
                        <div class="row-form">
                            <div class="span12">
                                <input type="submit" name="submit" value="Simpan" class="btn btn-large">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="btn btn-large" onClick="window.location.reload()"><i class="ico-edit"></i> Batal Ubah</span>
                            </div>               
                        </div>
                    </div>  
                </div>
            </div>
        </div>
</form>

<script type="text/javascript">
    function anggaran_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;
        dak = (parseInt($('input[name="anggaran_dak"]').val()));
        pendamping = (parseInt($('input[name="anggaran_pendamping"]').val()));
        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="anggaran_jumlah"]').val("0");
        } else {
            $('input[name="anggaran_jumlah"]').val(jumlah);
        }
    }
    function panjar_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;
        dak = (parseInt($('input[name="panjar_dak"]').val()));
        pendamping = (parseInt($('input[name="panjar_pendamping"]').val()));
        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="panjar_jumlah"]').val("0");
        } else {
            $('input[name="panjar_jumlah"]').val(jumlah);
        }
    }
    function realisasi_jumlah() {
        var dak = 0;
        var pendamping = 0;
        var jumlah = 0;
        dak = (parseInt($('input[name="realisasi_dak"]').val()));
        pendamping = (parseInt($('input[name="realisasi_pendamping"]').val()));
        jumlah = dak + pendamping;
        if (isNaN(jumlah)) {
            $('input[name="realisasi_jumlah"]').val("0");
        } else {
            $('input[name="realisasi_jumlah"]').val(jumlah);
        }
    }
    function detail() {
        $("#anggaran_dak_1").html("Rp. " + koma($('input[name="anggaran_dak"]').val()) + " ,-");
        $("#anggaran_pendamping_1").html("Rp. " + koma($('input[name="anggaran_pendamping"]').val()) + " ,-");
        $("#anggaran_jumlah_1").html("Rp. " + koma($('input[name="anggaran_jumlah"]').val()) + " ,-");

        $("#panjar_dak_1").html("Rp. " + koma($('input[name="panjar_dak"]').val()) + " ,-");
        $("#panjar_pendamping_1").html("Rp. " + koma($('input[name="panjar_pendamping"]').val()) + " ,-");
        $("#panjar_jumlah_1").html("Rp. " + koma($('input[name="panjar_jumlah"]').val()) + " ,-");

        $("#realisasi_dak_1").html("Rp. " + koma($('input[name="realisasi_dak"]').val()) + " ,-");
        $("#realisasi_pendamping_1").html("Rp. " + koma($('input[name="realisasi_pendamping"]').val()) + " ,-");
        $("#realisasi_jumlah_1").html("Rp. " + koma($('input[name="realisasi_jumlah"]').val()) + " ,-");

        $("#anggaran_kas").html("Rp. " + koma($('input[name="anggaran_kas"]').val()) + " ,-");

    }

    setInterval(function() {
        anggaran_jumlah();
        panjar_jumlah();
        realisasi_jumlah();
        detail();
    }, 1000);
	$('.decimal').keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			 val = val.replace(/[^0-9\.]/g,'');
			 if(val.split('.').length>2) 
				 val =val.replace(/\.+$/,"");
		}
		$(this).val(val); 
	});
	
	//pindah dengan enter
	$('#form').on('keydown', 'input', function (event) {
		if (event.which == 13) {
			event.preventDefault();
			var $this = $(event.target);
			var index = parseFloat($this.attr('data-index'));
			$('[data-index="' + (index + 1).toString() + '"]').focus();
		}
	});
</script>