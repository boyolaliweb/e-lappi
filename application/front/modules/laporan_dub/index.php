<script type="text/javascript">
    $(function() {
        $('#bulan').change(function(event) {
            //begin here
            var perSKPD;
            <?php if (isset($_SESSION['id_skpd'])) { ?>
                perSKPD = null;
            <?php } else { ?>
                perSKPD = $('#per_skpd').val();
            <?php } ?>
            //alert($('#bulan').val());
            urls = 'application/front/modules/laporan_dub/item_bulan.php';
            $.post(urls, {username: "<?php echo $_SESSION['username'];?>",id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, level: "<?php echo $_SESSION['level'];?>", per_skpd:perSKPD},
            function(data) {
                $('#tableWeb').html(data);
            }
            );

            urls2 = 'application/front/modules/laporan_dub/item_bulan_print.php';
            $.post(urls2, {id: $('#bulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, per_skpd:perSKPD},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
        $('#triwulan').change(function(event) {
            urls = 'application/front/modules/laporan_dub/item_triwulan.php';
            $.post(urls, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, per_skpd:perSKPD},
            function(data) {
                $('#tableWeb').html(data);
            }
            );
            urls2 = 'application/front/modules/laporan_dub/item_triwulan_print.php';
            $.post(urls2, {id: $('#triwulan').val(), tahun: <?php echo $_SESSION['tahun'] ?>, skpd: <?php echo (isset($_SESSION['id_skpd'])) ? $_SESSION['id_skpd'] : 0; ?>, per_skpd:perSKPD},
            function(data2) {
                $('#tableExcel').html(data2);
            }
            );
        });
    });
</script>

<div class="page-header">
    <div class="icon">
        <span class="ico-layout-7"></span>
    </div>
    <h1>Laporan Dana Urusan Bersama (DUB) <small>HALAMAN UNTUK MENGELOLA LAPORAN DUB</small></h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="block">
            <div class="head dblue">
                <div class="icon"><span class="ico-layout-9"></span></div>
                <h2>Laporan Dana Urusan Bersama </h2>
                <ul class="buttons">
                    <button class="btn" onclick="print('tableExcel')">
                        <span class="icon-print icon-white"></span> Cetak
                    </button>
                    <a href="#" class="btn" id="excel" type="button">
                        <span class="icon-book icon-white"></span> Export to Excel
                    </a>

                </ul>                                                        
            </div>           
            <div>
                <h3>Filtering </h3>
                <table>
                    <tr>
                         <?php if (!isset($_SESSION['id_skpd'])) { ?>
                            <td>
                                SKPD:
                            </td>
                            <td>
                                <select name="per_skpd" id="per_skpd" style="width:200px;">
                                    <option value="all">Semua SKPD</option>
                                    <?php
                                    $sql_skpd = $db->query("select id,nama from m_skpd order by kode asc");
                                    while ($row_skpd = $sql_skpd->fetch_assoc()) {
                                        echo '<option value="' . $row_skpd['id'] . '">' . $row_skpd['nama'] . '</option>';
                                    }
                                    ?>
                                </select>

                            </td>
                            <td width="80px;">&nbsp;</td>
                        <?php } ?>
                        <td>
                            Bulanan: 
                        </td>
                        <td>
                            <select name="bulan" id="bulan">
                                <option value="">--Pilih Bulan--</option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">Nopember</option>
                                <option value="12">Desember</option>
                            </select>
                        </td>
                        <td>
                            <span id="loading_image" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading <img src="media/img/loader.gif"/></span> 
                        </td>
                    </tr>
                    <!--
                                        <tr>
                        <td>Triwulan: </td>
                        <td>
                            <select name="triwulan" id="triwulan">
                                <option value="">--Pilih Triwulan--</option>
                                <option value="1">Januari-Maret</option>
                                <option value="2">April-Juni</option>
                                <option value="3">Juli-September</option>
                                <option value="4">Oktober-Desember</option>
                            </select>
                        </td>
                    </tr>
                    -->
                </table>                                                  
            </div> 	
            <hr>
            <div>&nbsp;</div>
            <div class="data-fluid" id="tableExcel">
                <center>
                    <h3>LAPORAN KEGIATAN YANG BERSUMBER DARI DANA URUSAN BERSAMA (DUB)<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $_SESSION['tahun']; ?></h3>
                </center>
                <table cellpadding="0" border="1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">PROGRAM/KEGIATAN</th>
                            <th colspan="3">ANGGARAN (Rp.)</th>
                            <th colspan="3">SP2D (Rp.)</th>
                            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
                            <th colspan="2">Progres Fisik</th>
                            <th rowspan="2">KET.</th>
                        </tr>
                        <tr>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>Target (%)</th>
                            <th>Real (%)</th>
                        </tr>
                        <tr>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
                        </tr>
                    </thead>
                    <tbody style="display:none;">
                        <?php
                        if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
                            $sql = mysqli_query($db,"select distinct(id_skpd) from t_dub where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        } else {
                            $sql = mysqli_query($db,"select distinct(id_skpd) from t_dub where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        }
                        $noBid = 1;
                        //make looping to get data
                        while ($rowBid = mysqli_fetch_array($sql)) {
                            $skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1"));
                            echo '
							<tr>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $skpd['nama'] . '</b></td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
						';
                            $sql_program = mysqli_query($db,"select distinct(program) from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and status_update='0'");
                            $jum3 = 0;
                            $jum4 = 0;
                            $jum5 = 0;
                            $jum6 = 0;
                            $jum7 = 0;
                            $jum8 = 0;
                            $jum9 = 0;
                            $jum10 = 0;
                            $jum11 = 0;
                            $jum12 = 0;
                            $jum13 = 0;
                            while ($row_program = mysqli_fetch_array($sql_program)) {
                                echo '
                                    <tr>
                                            <td>&nbsp;</td>
                                            <td><b>' . $row_program['program'] . '</b></td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                    </tr>
                                ';
                                if (isset($_SESSION['id_skpd'])) {
                                    $sqlKegiatan = mysqli_query($db,"select * from t_dub where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and program='" . $row_program['program'] . "' and status_update='0'");
                                } else {
                                    $sqlKegiatan = mysqli_query($db,"select * from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and program='" . $row_program['program'] . "' and status_update='0'");
                                }
                                $no = 1;

                                while ($row = mysqli_fetch_array($sqlKegiatan)) {
                                    $jum3 = $jum3 + $row['anggaran_dub'];
                                    $jum4 = $jum4 + $row['anggaran_pendamping'];
                                    $jum5 = $jum5 + $row['anggaran_jumlah'];
                                    $jum6 = $jum6 + $row['panjar_dub'];
                                    $jum7 = $jum7 + $row['panjar_pendamping'];
                                    $jum8 = $jum8 + $row['panjar_jumlah'];
                                    $jum9 = $jum9 + $row['realisasi_dub'];
                                    $jum10 = $jum10 + $row['realisasi_pendamping'];
                                    $jum11 = $jum11 + $row['realisasi_jumlah'];
                                    $jum12 = $jum12 + $row['progres_target'];
                                    $jum13 = $jum13 + $row['progres_real'];
                                    $anggaran_dub = ($row['anggaran_dub'] == 0) ? "-" : number_format($row['anggaran_dub'], 0, ",", ".");
                                    $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                                    $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                                    $panjar_dub = ($row['panjar_dub'] == 0) ? "-" : number_format($row['panjar_dub'], 0, ",", ".");
                                    $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                                    $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                                    $realisasi_dub = ($row['realisasi_dub'] == 0) ? "-" : number_format($row['realisasi_dub'], 0, ",", ".");
                                    $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                                    $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                                    $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
                                    $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");


                                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                                    $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                                    echo '
								<tr>
									
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_dub . '</td>
									<td align="right">' . $anggaran_pendamping . '</td>
									<td align="right">' . $anggaran_jumlah . '</td>
									<td align="right">' . $panjar_dub . '</td>
									<td align="right">' . $panjar_pendamping . '</td>
									<td align="right">' . $panjar_jumlah . '</td>
									<td align="right">' . $realisasi_dub . '</td>
									<td align="right">' . $realisasi_pendamping . '</td>
									<td align="right">' . $realisasi_jumlah . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td>' . $masalah . '</td>							
								</tr>
								';
                                }
                            }

                            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
                            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
                            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
                            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
                            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
                            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
                            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
                            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
                            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
                            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
                            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
                            echo '
							<tr>
								<td>&nbsp;</td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
								<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
								<td align="right"><b>' . $jum13 . '</b></td><td align="right">&nbsp;</td>
							</tr>
						';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
            <div class="data-fluid" id="tableWeb">
                <table cellpadding="0" border="1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">No</th>
                            <th rowspan="2">PROGRAM/KEGIATAN</th>
                            <th colspan="3">ANGGARAN (Rp.)</th>
                             <th rowspan="2">Anggaran Kas</th>
                            <th colspan="3">SP2D (Rp.)</th>
                            <th colspan="3">Realisasi Pertanggungjawaban (SPJ) (Rp.)</th>
                            <th colspan="2">Progres Fisik</th>
                            <th rowspan="2">KET.</th>
                           <th rowspan="2">Sisa Anggaran Kas</th>
                        </tr>
                        <tr>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>DUB</th>
                            <th>Dana Pendamping</th>
                            <th>JUMLAH</th>
                            <th>Target (%)</th>
                            <th>Real (%)</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
                            <th>15</th>
                            <th>16</th>
                        </tr>
                    </thead>
                    <tbody style="display:none;">
                        <?php
                        if (isset($_SESSION['id_skpd'])) { //cek apakah login sebagai skpd atau administrator
                            $sql = mysqli_query($db,"select distinct(id_skpd) from t_dub where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        } else {
                            $sql = mysqli_query($db,"select distinct(id_skpd) from t_dub where tahun='" . $_SESSION['tahun'] . "' and status_update='0'");
                        }
                        $noBid = 1;
                        //make looping to get data
                        while ($rowBid = mysqli_fetch_array($sql)) {
                            $skpd = mysqli_fetch_array(mysqli_query($db,"select nama from m_skpd where id='" . $rowBid['id_skpd'] . "' limit 1"));
                            echo '
							<tr>
								<td></td>
								<td align="center"><b>' . romawi($noBid++) . '</b></td>
								<td><b>' . $skpd['nama'] . '</b></td>
								<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
								<td></td><td></td><td></td><td></td>
							</tr>
						';
                            $sql_program = mysqli_query($db,"select distinct(program) from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and status_update='0'");
                            $jum3 = 0;
                            $jum4 = 0;
                            $jum5 = 0;
                            $jum6 = 0;
                            $jum7 = 0;
                            $jum8 = 0;
                            $jum9 = 0;
                            $jum10 = 0;
                            $jum11 = 0;
                            $jum12 = 0;
                            $jum13 = 0;
                            $jum15 = 0;
                            while ($row_program = mysqli_fetch_array($sql_program)) {
                                echo '
                                    <tr>
                                            <td>&nbsp;</td><td>&nbsp;</td>
                                            <td><b>' . $row_program['program'] . '</b></td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                    </tr>
                                ';
                                if (isset($_SESSION['id_skpd'])) {
                                    $sqlKegiatan = mysqli_query($db,"select * from t_dub where id_skpd='" . $_SESSION['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and program='" . $row_program['program'] . "' and status_update='0'");
                                } else {
                                    $sqlKegiatan = mysqli_query($db,"select * from t_dub where id_skpd='" . $rowBid['id_skpd'] . "' and tahun='" . $_SESSION['tahun'] . "' and program='" . $row_program['program'] . "' and status_update='0'");
                                }
                                $no = 1;

                                while ($row = mysqli_fetch_array($sqlKegiatan)) {
                                    $jum3 = $jum3 + $row['anggaran_dub'];
                                    $jum4 = $jum4 + $row['anggaran_pendamping'];
                                    $jum5 = $jum5 + $row['anggaran_jumlah'];
                                    $jum6 = $jum6 + $row['panjar_dub'];
                                    $jum7 = $jum7 + $row['panjar_pendamping'];
                                    $jum8 = $jum8 + $row['panjar_jumlah'];
                                    $jum9 = $jum9 + $row['realisasi_dub'];
                                    $jum10 = $jum10 + $row['realisasi_pendamping'];
                                    $jum11 = $jum11 + $row['realisasi_jumlah'];
                                    $jum12 = $jum12 + $row['progres_target'];
                                    $jum13 = $jum13 + $row['progres_real'];
                                    $jum15 = $jum15 + $row['anggaran_kas'];
                                    $anggaran_dub = ($row['anggaran_dub'] == 0) ? "-" : number_format($row['anggaran_dub'], 0, ",", ".");
                                    $anggaran_pendamping = ($row['anggaran_pendamping'] == 0) ? "-" : number_format($row['anggaran_pendamping'], 0, ",", ".");
                                    $anggaran_jumlah = ($row['anggaran_jumlah'] == 0) ? "-" : number_format($row['anggaran_jumlah'], 0, ",", ".");
                                    $panjar_dub = ($row['panjar_dub'] == 0) ? "-" : number_format($row['panjar_dub'], 0, ",", ".");
                                    $panjar_pendamping = ($row['panjar_pendamping'] == 0) ? "-" : number_format($row['panjar_pendamping'], 0, ",", ".");
                                    $panjar_jumlah = ($row['panjar_jumlah'] == 0) ? "-" : number_format($row['panjar_jumlah'], 0, ",", ".");
                                    $realisasi_dub = ($row['realisasi_dub'] == 0) ? "-" : number_format($row['realisasi_dub'], 0, ",", ".");
                                    $realisasi_pendamping = ($row['realisasi_pendamping'] == 0) ? "-" : number_format($row['realisasi_pendamping'], 0, ",", ".");
                                    $realisasi_jumlah = ($row['realisasi_jumlah'] == 0) ? "-" : number_format($row['realisasi_jumlah'], 0, ",", ".");
                                    $progres_target = ($row['progres_target'] == 0) ? "-" : number_format($row['progres_target'], 0, ",", ".");
                                    $progres_real = ($row['progres_real'] == 0) ? "-" : number_format($row['progres_real'], 0, ",", ".");
                                    $anggaran_kas = ($row['anggaran_kas'] == 0) ? "-" : number_format($row['anggaran_kas'], 0, ",", ".");

                                    if ($_SESSION['level'] == "skpd") {
                                        if ($row['final'] == 0) {
                                            $icon = 'pencil';
                                            $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dub-ok&id=' . $row['id'] . '" onclick="return confirm(\'Apakah yakin sudah final? Klik Tombol OK untuk melanjutkan proses. Untuk merubah data yang sudah final, silahkan hubungi administrator. Terima kasih\')">
													<span class="icon-ok"></span> Tandai Final
													</a></li>';
                                        } else {
                                            $icon = 'ok';
                                            $tombol = '<li><a href="#"><i class="icon-ban-circle"></i> Hubungi administrator untuk ubah</a></li>';
                                        }
                                    } else
                                    if ($_SESSION['level'] == "administrator") {
                                        if ($row['final'] == 0) {
                                            $icon = 'pencil';
                                            $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>';
                                        } else {
                                            $icon = 'ok';
                                            $tombol = '<li><a href="index.php?val=laporan-dub-edit&id=' . $row['id'] . '" onclick="return confirm(\'Data ingin diubah?\')">Edit</a></li>
													<li><a href="index.php?val=laporan-dub-delete&id=' . $row['id'] . '" onclick="return confirm(\'Yakin ingin dihapus?\')">Hapus</a></li>
													<li class="divider"></li>
													<li><a href="index.php?val=laporan-dub-revisi&id=' . $row['id'] . '" onclick="return confirm(\'Data ini akan direvisi?\')">
													<span class="icon-retweet"></span> Revisi Data Laporan
													</a></li>';
                                        }
                                    }
                                    $masalah = ($row['permasalahan']) ? $row['permasalahan'] : "&nbsp;";
                                    $lokasi = ($row['lokasi']) ? " (" . $row['lokasi'] . ")" : "";
                                    echo '
								<tr>
									<td>
										<div class="btn-group">
                                            &nbsp;<button class="btn btn-link dropdown-toggle" data-toggle="dropdown"><span class="icon-' . $icon . '"></span></button>&nbsp;
                                            <ul class="dropdown-menu">
                                                ' . $tombol . '
                                            </ul>
                                        </div>
									</td>
									<td align="center">' . $no++ . '</td>
									<td>' . $row['kegiatan'] . $lokasi . '</td>
									<td align="right">' . $anggaran_dub . '</td>
									<td align="right">' . $anggaran_pendamping . '</td>
									<td align="right">' . $anggaran_jumlah . '</td>
									<td align="right">' . $panjar_dub . '</td>
									<td align="right">' . $panjar_pendamping . '</td>
									<td align="right">' . $panjar_jumlah . '</td>
									<td align="right">' . $realisasi_dub . '</td>
									<td align="right">' . $realisasi_pendamping . '</td>
									<td align="right">' . $realisasi_jumlah . '</td>
									<td align="right">' . $progres_target . '</td>
									<td align="right">' . $progres_real . '</td>
									<td>' . $masalah . '</td>							
									<td align="right">' . $anggaran_kas . '</td>
								</tr>
								';
                                }
                            }

                            $jum3 = ($jum3 == 0) ? "-" : number_format($jum3, 0, ",", ".");
                            $jum4 = ($jum4 == 0) ? "-" : number_format($jum4, 0, ",", ".");
                            $jum5 = ($jum5 == 0) ? "-" : number_format($jum5, 0, ",", ".");
                            $jum6 = ($jum6 == 0) ? "-" : number_format($jum6, 0, ",", ".");
                            $jum7 = ($jum7 == 0) ? "-" : number_format($jum7, 0, ",", ".");
                            $jum8 = ($jum8 == 0) ? "-" : number_format($jum8, 0, ",", ".");
                            $jum9 = ($jum9 == 0) ? "-" : number_format($jum9, 0, ",", ".");
                            $jum10 = ($jum10 == 0) ? "-" : number_format($jum10, 0, ",", ".");
                            $jum11 = ($jum11 == 0) ? "-" : number_format($jum11, 0, ",", ".");
                            $jum12 = ($jum12 == 0) ? "-" : number_format($jum12, 0, ",", ".");
                            $jum13 = ($jum13 == 0) ? "-" : number_format($jum13, 0, ",", ".");
                            $jum15 = ($jum15 == 0) ? "-" : number_format($jum15, 0, ",", ".");
                            echo '
							<tr>
								<td></td><td></td>
								<td align="center"><b>Jumlah</b></td>
								<td align="right"><b>' . $jum3 . '</b></td><td align="right"><b>' . $jum4 . '</b></td>
								<td align="right"><b>' . $jum5 . '</b></td><td align="right"><b>' . $jum6 . '</b></td>
								<td align="right"><b>' . $jum7 . '</b></td><td align="right"><b>' . $jum8 . '</b></td>
								<td align="right"><b>' . $jum9 . '</b></td><td align="right"><b>' . $jum10 . '</b></td>
								<td align="right"><b>' . $jum11 . '</b></td><td align="right"><b>' . $jum12 . '</b></td>
								<td align="right"><b>' . $jum13 . '</b></td><td align="right"></td>
								<td align="right"><b>' . $jum15 . '</b></td>
							</tr>
						';
                        }
                        ?>
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>