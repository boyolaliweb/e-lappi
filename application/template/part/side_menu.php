<div class="sidebar bordered">

    <div class="top">

        <p align="center">

            <img src="media/img/logo_.png" width="50px"/>

            <img src="media/img/logox.png"/><br>

        </p>

    </div>

    <div class="nContainer">

        <ul class="navigation bordered">

            <li class=""><a href="dashboard" class="blblue">Dashboard</a></li>

            <?php if ((isset($_SESSION['level']) && isset($_SESSION['username']) && $_SESSION['level'] == "administrator" && $_SESSION['username'] == "admin") || $_SESSION['level'] == "skpd") { ?>

                <li class="active1">

                    <a href="#" class="blgreen">Entry Lap. Kegiatan</a>



                    <div class="open"></div>

                    <ul>

                        <li><a href="laporan-dak-form">APBN - DAK</a></li>

                        <li><a href="laporan-dbhcht-form">APBN - Dana DBHCHT</a></li>

                        <li><a href="laporan-bankeu_kab-form">APBD Prov - Bankeu Kota/Kab</a></li>

                        <li><a href="laporan-apbd-form">APBD Kabupaten - APBD</a></li>

                        <li><a href="laporan-dekon-form">APBN - Dana Dekonsentrasi</a></li>

                        <li><a href="laporan-tp-form">APBN - Dana Tugas Pembantuan</a></li>

                        <li><a href="laporan-dub-form">APBN - Dana Urusan Bersama</a></li>

                        <li><a href="laporan-bankeu_desa-form">APBD Prov - Bankeu Desa</a></li>

                    </ul>

                </li>

                 <li class="active1">

                    <a href="#" class="blorange">Entry Ang. Perubahan</a>

                    <div class="open"></div>

                    <ul>

                        <li><a href="laporan-dak-form-perubahan">APBN - DAK</a></li>

                        <li><a href="laporan-dbhcht-form-perubahan">APBN - Dana DBHCHT</a></li>

                        <li><a href="laporan-bankeu_kab-form-perubahan">APBD Prov - Bankeu Kota/Kab</a></li>

                        <li><a href="laporan-apbd-form-perubahan">APBD Kabupaten - APBD</a></li>

                        <li><a href="laporan-dekon-form-perubahan">APBN - Dana Dekonsentrasi</a></li>

                        <li><a href="laporan-tp-form-perubahan">APBN - Dana Tugas Pembantuan</a></li>

                        <li><a href="laporan-dub-form-perubahan">APBN - Dana Urusan Bersama</a></li>

                        <li><a href="laporan-bankeu_desa-form-perubahan">APBD Prov - Bankeu Desa</a></li>

                    </ul>

                </li>

            <?php } ?>

            <li class="active1">

                <a href="#" class="blblue">Lap. Kegiatan</a>

                <div class="open"></div>

                <ul>

                    <li><a href="laporan-dak">APBN - DAK</a></li>

                    <li><a href="laporan-dbhcht">APBN - Dana DBHCHT</a></li>

                    <li><a href="laporan-bankeu_kab">APBD Prov - Bankeu Kota/Kab</a></li>

                    <li><a href="laporan-apbd">APBD Kabupaten - APBD</a></li>

                    <li><a href="laporan-dekon">APBN - Dana Dekonsentrasi</a></li>

                    <li><a href="laporan-tp">APBN - Dana Tugas Pembantuan</a></li>

                    <li><a href="laporan-dub">APBN - Dana Urusan Bersama</a></li>

                    <li><a href="laporan-bankeu_desa">APBD Prov - Bankeu Desa</a></li>

                </ul>

            </li>

            <!-- start menu laporan kas -->

            <li class="active1">

                <a href="#" class="blgreen">Lap. Anggaran Kas</a>



                <div class="open"></div>

                <ul>

                    <li><a href="kas-laporan-dak">APBN - DAK</a></li>

                    <li><a href="kas-laporan-dbhcht">APBN - Dana DBHCHT</a></li>

                    <li><a href="kas-laporan-bankeu_kab">APBD Prov - Bankeu Kota/Kab</a></li>

                    <li><a href="kas-laporan-apbd">APBD Kabupaten - APBD</a></li>



                    <li><a href="kas-laporan-dekon">APBN - Dana Dekonsentrasi</a></li>

                    <li><a href="kas-laporan-tp">APBN - Dana Tugas Pembantuan</a></li>

                    <li><a href="kas-laporan-dub">APBN - Dana Urusan Bersama</a></li>

                    <li><a href="kas-laporan-bankeu_desa">APBD Prov - Bankeu Desa</a></li>

                </ul>

            </li>

            <?php if (isset($_SESSION['level']) && ($_SESSION['level'] == "administrator" || $_SESSION['level'] == "view")) { ?>

                <li class="active1">

                    <a href="#" class="blorange">Rekapitulasi Per SKPD</a>



                    <div class="open"></div>

                    <ul>

                        <li><a href="rekap-dak">APBN - DAK</a></li>

                        <li><a href="rekap-dbhcht">APBN - Dana DBHCHT</a></li>

                        <li><a href="rekap-bankeu_kab">APBD Prov - Bankeu Kota/Kab</a></li>

                        <li><a href="rekap-apbd">APBD Kabupaten - APBD</a></li>

                        <li><a href="rekap-dekon">APBN - Dana Dekonsentrasi</a></li>

                        <li><a href="rekap-tp">APBN - Dana Tugas Pembantuan</a></li>

                        <li><a href="rekap-dub">APBN - Dana Urusan Bersama</a></li>

                        <li><a href="rekap-bankeu_desa">APBD Prov - Bankeu Desa</a></li>

                    </ul>

                </li>

                <li class="active1">

                    <a href="#" class="blyellow">Rekapitulasi APBN DAN APBD PROVINSI</a>



                    <div class="open"></div>

                    <ul>

                        <li><a href="rekap-apbn-apbd">Rekapitulasi APBN & APBD</a></li>

                    </ul>

                </li>

                <li class="active1">

                    <a href="#" class="blred">Rekapitulasi Kegiatan</a>



                    <div class="open"></div>

                    <ul>

                        <li><a href="rekap-apbd-kab">Rekapitulasi APBD Kabupaten</a></li>

                    </ul>

                </li>





                <?php if ($_SESSION['level'] == "administrator") { ?>

                    <li class="active1">

                        <a href="#" class="blpurple">Data Master</a>



                        <div class="open"></div>

                        <ul>

                            <li><a href="master-skpd">Data SKPD</a></li>

                            <!--<li><a href="master-bidang">Master Bidang</a></li>-->

                            <li><a href="master-kegiatan">Master Kegiatan</a></li>

                            <!--<li><a href="master-satuan">Master Satuan</a></li>-->

                            <li><a href="master-tahun">Master Tahun Anggaran</a></li>

                            <!--<li><a href="master-bagian">Master Bagian</a></li>-->

                            <li><a href="master-entry">Status Entry Data</a></li>

                        </ul>

                    </li>

                <?php } ?>

            <?php } ?>

            <li class="active1">

                <a href="#" class="blyellow">Pengguna</a>



                <div class="open"></div>

                <ul>

                    <!--<li><a href="user">Daftar Pengguna</a></li>-->

                    <!--<li><a href="user-akses">Hak Akses Pengguna</a></li>-->

                    <li><a href="user-password">Ubah Password</a></li>

                    <!--<li><a href="user-form">Tambah Pengguna</a></li>-->

                </ul>

            </li>

            <li>

                <a href="logout.php" class="blred">Keluar (Logout)</a>

            </li>

        </ul>

        <a class="close">

            <span class="ico-remove"></span>

        </a>

    </div>

    <div class="widget">

        <div class="datepicker"></div>

    </div>

    <div class="widget" align="center" style="padding:20px 5px 30px 5px;margin-top:20px;">

        <span>Status Entry: <?php echo $STATUS;?></span><br><br>

        <img src="media/img/firefox.gif"><br><br>

        <span>E-Laporan 2.0</span>

    </div>

</div>