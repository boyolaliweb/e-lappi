<ul class="navigation">
    <li>
        <a href="dashboard" class="button box-shadow">
            <div class="icon">
                <span class="ico-monitor"></span>
            </div>                    
            <div class="name">Dashboard</div>
        </a>                
    </li>
    <?php if ((isset($_SESSION['level']) && isset($_SESSION['username']) && $_SESSION['level'] == "administrator" && $_SESSION['username'] == "admin") || $_SESSION['level'] == "skpd") { ?>
        <li>
            <a href="#" class="button yellow box-shadow">
                <div class="arrow"></div>
                <div class="icon">
                    <span class="ico-download"></span>
                </div>                    
                <div class="name">Entry Laporan</div>
            </a>          
            <ul class="sub">
                <li><a href="laporan-dak-form">APBN - DAK</a></li>
                <li><a href="laporan-dbhcht-form">APBN - Dana DBHCHT</a></li>
                <li><a href="laporan-bankeu_kab-form">APBD Prov - Bankeu Kota/Kab</a></li>
                <li><a href="laporan-apbd-form">APBD Kabupaten - APBD</a></li>
                <li><a href="laporan-dekon-form">APBN - Dana Dekon</a></li>
                <li><a href="laporan-tp-form">APBN - Dana Tugas Pembantuan</a></li>
                <li><a href="laporan-dub-form">APBN - Dana Urusan Bersama</a></li>
                <li><a href="laporan-bankeu_desa-form">APBD Prov - Bankeu Desa</a></li>
            </ul>
        </li>  
    <?php } ?>	
    <li>
        <a href="#" class="button green box-shadow">
            <div class="arrow"></div>
            <div class="icon">
                <span class="ico-upload"></span>
            </div>                    
            <div class="name">Lap. Kegiatan</div>
        </a>                
        <ul class="sub">
            <li><a href="laporan-dak">APBN - DAK</a></li>
            <li><a href="laporan-dbhcht">APBN - Dana DBHCHT</a></li>
            <li><a href="laporan-bankeu_kab">APBD Prov - Bankeu Kota/Kab</a></li>
            <li><a href="laporan-apbd">APBD Kabupaten - APBD</a></li>
            <li><a href="laporan-dekon">APBN - Dana Dekon</a></li>
            <li><a href="laporan-tp">APBN - Dana Tugas Pembantuan</a></li>
            <li><a href="laporan-dub">APBN - Dana Urusan Bersama</a></li>
            <li><a href="laporan-bankeu_desa">APBD Prov - Bankeu Desa</a></li>
        </ul>                    
    </li>                        
    <?php if (isset($_SESSION['level']) && ($_SESSION['level'] == "administrator" || $_SESSION['level'] == "view")) { ?>
        <li>
            <a href="#" class="button dblue box-shadow">
                <div class="arrow"></div>
                <div class="icon">
                    <span class="ico-layout-7"></span>
                </div>                    
                <div class="name">Rekapitulasi</div>
            </a> 
            <ul class="sub">
                <li><a href="rekap-dak">DAK</a></li>
                <li><a href="rekap-dbhcht">Dana DBHCHT</a></li>
                <li><a href="rekap-bankeu_kab">Bankeu Kota/Kab</a></li>
                <li><a href="rekap-apbd">APBD Kabupaten</a></li>
                <li><a href="rekap-dekon">Dekon</a></li>
                <li><a href="rekap-tp">TP</a></li>
                <li><a href="rekap-dub">DUB</a></li>
                <li><a href="rekap-bankeu_desa">Bankeu Desa</a></li>
            </ul>                                        
        </li>
        <?php if ($_SESSION['level'] == "administrator") { ?>
            <li>
                <a href="#" class="button purple box-shadow">
                    <div class="arrow"></div>
                    <div class="icon">
                        <span class="ico-cloud"></span>
                    </div>                    
                    <div class="name">Data SKPD</div>
                </a>                
                <ul class="sub">
                    <li><a href="master-skpd">Data SKPD</a></li>
                </ul>                                        
            </li>  
            <li>
                <a href="#" class="button orange box-shadow">
                    <div class="arrow"></div>
                    <div class="icon">
                        <span class="ico-cloud"></span>
                    </div>                    
                    <div class="name">Data Master</div>
                </a>                
                <ul class="sub">
                    <li><a href="master-entry">Status Entry Data</a></li>
                    <li><a href="master-bidang">Master Bidang</a></li>
                    <li><a href="master-program">Master Program</a></li>
                    <li><a href="master-kegiatan">Master Kegiatan</a></li>
                    <li><a href="master-tahun">Master Tahun Anggaran</a></li>
                    <li><a href="master-satuan">Master Satuan</a></li> 
                    <li><a href="master-bagian">Master Bagian</a></li>
                </ul>                                        
            </li>   
        <?php } ?> 
    <?php } ?>
    <li>
        <a href="#" class="button red box-shadow">
            <div class="arrow"></div>
            <div class="icon">
                <span class="ico-box"></span>
            </div>                    
            <div class="name">Pengguna</div>
        </a>                
        <ul class="sub">
            <!--<li><a href="nodata">Daftar Pengguna</a></li>-->
            <!--<li><a href="nodata">Hak Akses Pengguna</a></li>-->
            <li><a href="user-password">Ubah Password</a></li>
            <!--<li><a href="nodata">Tambah Pengguna</a></li>-->
        </ul>                                        
    </li>
    <li class="hidden-phone">
        <div class="sbutton user">
            <img src="media/img/user.jpg" align="left" class="box-shadow"/>
            <a href="#" class="name ">
                <span><?php echo $_SESSION['username']; ?></span>
                <span class="sm"><?php echo $_SESSION['level']; ?></span>
            </a>
            <a href="#"><span class="ico-cog"></span></a>   
            <div class="popup">
                <div class="arrow"></div>
                <div class="row-fluid">
                    <div class="row-form">
                        <div class="span12"><strong>SISTEM</strong></div>
                    </div>                
                    <div class="row-form">
                        <!--
                                                <div class="span4">
                            <table border="0">
                                <tr>
                                    <td>
                                        <a href="nodata" class="buttonx yellow">
                                            <div class="icon">
                                                <span class="ico-lock"></span>
                                            </div>                    
                                            <div class="name">Hak Akses</div>
                                        </a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="nodata" class="buttonx purple top_10">
                                            <div class="icon">
                                                <span class="ico-book"></span>
                                            </div>                    
                                            <div class="name">Log Activity</div>
                                        </a> 
                                    </td>
                                </tr>
                            </table>
                        </div> 
                        -->
                        <div class="span4">
                            <a href="user-password" class="button orange box-shadow">
                                <div class="icon">
                                    <span class="ico-unlock"></span>
                                </div>                    
                                <div class="name">Ubah Password</div>
                            </a> 
                            <br>
                        </div> 
                        <div class="span8">
                            <a href="logout.php" class="button green box-shadow" style="width:90%;">
                                <div class="icon">
                                    <span class="ico-signout"></span>
                                </div>                    
                                <div class="name">Logout</div>
                            </a> 
                            <br>
                        </div> 
                    </div>

                </div>
            </div>                        
        </div>
    </li>                
</ul>