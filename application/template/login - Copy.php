<!DOCTYPE html>
<html lang="en">
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />    
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <!--[if gt IE 8]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
        <![endif]-->                
        <title>Login E-Laporan | Kabupaten Boyolali</title>
        <link rel="icon" type="image/ico" href="media/img/favicon.ico"/>
        <link href="media/css/boot.css" rel="stylesheet" type="text/css" />
        <link href="media/css/steel.css" rel="stylesheet" type="text/css" />
        <link href="media/css/grafik.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 10]>      
            <link href="media/css/ie.css" rel="stylesheet" type="text/css" />
        <![endif]-->           
        <!--[if lte IE 7]>
            <script type='text/javascript' src='media/js/plugins/other/lte-ie7.js'></script>
        <![endif]-->    
        <script type='text/javascript' src='media/js/jquery.js'></script>

        <script type='text/javascript' src='media/js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='media/js/plugins/bootstrap/bootstrap.min.js'></script>


        <script src="media/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="media/amcharts/pie.js" type="text/javascript"></script>
        <script src="media/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="media/amcharts/serial.js" type="text/javascript"></script>
        <script src="media/amcharts/dataloader.js" type="text/javascript"></script>

    </head>
    <body>    
        <header class="header_area">
            <!-- start header top area -->
            <div class="header_top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden-xs">
                            <div class="left_header_top">
                                <ul>
                                    <li>&nbsp;</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                            <div class="left_header_top pull-right">
                                <ul>
                                    <li><a href="../">Website Bappeda Kabupaten Boyolali</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start header bottom area -->
            <div class="header_bottom_area">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="logo">
                                <a href="login.php">
                                    <img src="media/img/byl.png" alt="Bappeda Kab Boyolali" class="img img-responsive" style="width:790px !important;height:102px !important;">                                
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-5 col-sm-6 col-md-9">
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container" id="mainContainer">
            <!-- start category area -->
            <div class="category_area">

                <div class="header_category">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="title_category" style="padding:0px !important">
                                <p align="center">
                                    <img src="media/img/logox.png" alt="Bappeda Kab Boyolali" class="img img-responsive" style="height:60px;">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if (isset($_POST['submit_login'])) {
                    if ($_POST['tipe'] == "skpd") {
                        $sql = $db->query("select * from m_skpd where username='" . $_POST['username'] . "' and password='" . $_POST['password'] . "' limit 1");
                        $sql->data_seek(0);
                        $sqlCount = $sql->num_rows;
                        if ($sqlCount > 0) {
                            $rw = $sql->fetch_assoc();
                            //save login data to session
                            $_SESSION['id_skpd'] = $rw['id'];
                            $_SESSION['username'] = $rw['username'];
                            $_SESSION['login'] = "OK";
                            $_SESSION['level'] = "skpd";
                            $_SESSION['tahun'] = $_POST['tahun'];
                            $log = $db->query("insert into t_log set user='" . $rw['username'] . "', 
                                log='Login into sistem',
                                date='" . (date("Y-m-d H:i")) . "'");
                            redirect("index.php");
                        } else {
                            echo '<div class="alert alert-danger"><b>Maaf!! </b>Login gagal</div>';
                        }
                    } else {
                        $sql = $db->query("select * from m_user where username='" . $_POST['username'] . "' and password='" . $_POST['password'] . "' limit 1");
                        $sql->data_seek(0);
                        $sqlCount = $sql->num_rows;
                        if ($sqlCount > 0) {
                            $rw = $sql->fetch_assoc();
                            //save login data to session
                            $_SESSION['username'] = $rw['username'];
                            $_SESSION['login'] = "OK";
                            $_SESSION['level'] = $rw['level'];
                            $_SESSION['tahun'] = $_POST['tahun'];
                            redirect("index.php");
                        } else {
                            echo '<div class="alert alert-danger"><b>Maaf!! </b>Login gagal</div>';
                        }
                    }
                }
                ?>
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="category_products">
                            <div class="row">

                                <div class="pgl-sidebar sidebar-left col-md-3">
                                    <h3 class="widget-title"><span>MEMBER AREA</span></h3>

                                    <div class="subtotal_box">
                                        <legend style="font-size:14px;">Masuk E-Laporan</legend>
                                        <form id="validate" method="POST" action="">
                                            <div class="form-group">
                                                <div class="span12">
                                                    <input type="text" name="username" placeholder="user ID" class="validate[required] form-control" autocomplete="off" required="true"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="span12">
                                                    <input type="password" name="password" placeholder="password" id="password" autocomplete="off" required="true" class="form-control"/>
                                                </div>  
                                            </div>
                                            <div class="form-group">
                                                <div class="span12">
                                                    <select name="tipe" class="form-control">
                                                        <option value="skpd" selected>SKPD</option>
                                                        <option value="administrator">Administrator</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="span12">
                                                    <select name="tahun" class="form-control">
                                                        <?php
//get year of "anggaran"
                                                        $sqlTahun = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
                                                        $sqlTahun->data_seek(0);
                                                        while ($rowTahun = $sqlTahun->fetch_assoc()) {
                                                            $select = ($rowTahun['tahun'] == date("Y")) ? "selected" : "";
                                                            echo '<option value="' . $rowTahun['tahun'] . '" ' . $select . '>Tahun Anggaran ' . $rowTahun['tahun'] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="span12">
                                                    <button class="btn pull-right btn-success" type="submit" name="submit_login">Click to Login&nbsp;&nbsp; <span class="icon-arrow-next icon-white"></span></button>
                                                </div> 
                                            </div>
                                            <div class="form-group">
                                                &nbsp;
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <section class="col-md-9 pgl-main-left-sidebar clearfix" id="pgl-main-content">
                                    <div class="main-content-inner">

                                        <h3 class="widget-title"><span>PUBLIC AREA</span></h3>
                                        <div class="col-lg-12 well">
											<h4 id="nav-pills" align="center">Lihat Grafik Rekapitulasi Dana</h4>
											<div class="bs-component">
											  <ul class="nav nav-pills">
												<li><a href="#APBD" id="bt_apbd">APBD KAB</a></li>
												<li><a href="#DAK" id="bt_dak">DAK</a></li>
												<li><a href="#TP" id="bt_tp">TP</a></li>											
												<li><a href="#BANKEU-KAB" id="bt_bankeu_kab">BANKEU KAB</a></li>
												<li><a href="#BANKEU-DESA" id="bt_bankeu_desa">BANKEU DESA</a></li>
												<li><a href="#DBHCHT" id="bt_dbhcht">DBHCHT</a></li>
												<li><a href="#DUB" id="bt_dub">DUB</a></li>
												<li><a href="#DEKON" id="bt_dekon">DEKON</a></li>
											  </ul>
											</div>
										</div>									
             
            
                                        <!-- ##### APBD KABUPATEN ##### -->
										<div class="panel panel-default panel_apbd">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA APBD KABUPATEN BOYOLALI</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahunx" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv2"><div id="chartdiv2"></div></div>   
														<div id="loadApbd"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv4"><div id="chartdiv4"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_apbd">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana APBD Kabupaten silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END APBD KABUPATEN ##### -->
										
										<!-- ##### DAK - DANA ALOKASI KHUSUS ##### -->
										<div class="panel panel-default panel_dak">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA ALOKASI KHUSUS (DAK)</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_dak" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_dak" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_dak"><div id="chartdiv_dak"></div></div>   
														<div id="loadDak"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_dak"><div id="chartdiv_pie_dak"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_dak">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana alokasi khusus (DAK) silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END DAK ##### -->
										
										<!-- ##### TUGAS PEMBANTUAN - TP ##### -->
										<div class="panel panel-default panel_tp">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA TUGAS PEMBANTUAN (TP)</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_tp" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_tp" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_tp"><div id="chartdiv_tp"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_tp"><div id="chartdiv_pie_tp"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_tp">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana pembantuan (TP) silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END TP ##### -->
										
										<!-- ##### BANKEU KAB - KOTA ##### -->
										<div class="panel panel-default  panel_bankeu_kab">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA BANKEU KAB/KOTA</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_bankeu_kab" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_bankeu_kab" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_bankeu_kab"><div id="chartdiv_bankeu_kab"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_bankeu_kab"><div id="chartdiv_pie_bankeu_kab"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_bankeu_kab">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana bankeu kepada kabupaten silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END BANKEU KOTA ##### -->
										
										<!-- ##### BANKEU DESA ##### -->
										<div class="panel panel-default panel_bankeu_desa">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA BANKEU DESA</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_bankeu_desa" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_bankeu_desa" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_bankeu_desa"><div id="chartdiv_bankeu_desa"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_bankeu_desa"><div id="chartdiv_pie_bankeu_desa"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_bankeu_desa">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana bankeu kepada desa silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END BANKEU DESA ##### -->
										
										<!-- ##### DBHCHT ##### -->
										<div class="panel panel-default panel_dbhcht">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA DBHCHT</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_dbhcht" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_dbhcht" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_dbhcht"><div id="chartdiv_dbhcht"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_dbhcht"><div id="chartdiv_pie_dbhcht"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_dbhcht">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana DBHCHT silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END DBHCHT ##### -->
										
										<!-- ##### DANA URUSAN BERSAMA - DUB ##### -->
										<div class="panel panel-default panel_dub">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA URUSAN BERSAMA (DUB)</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_dub" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_dub" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_dub"><div id="chartdiv_dub"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_dub"><div id="chartdiv_pie_dub"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_dub">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana urusan bersama (DUB) silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END DUB ##### -->
										
										<!-- ##### DANA DEKONSENTRASI ##### -->
										<div class="panel panel-default panel_dekon">
											<div class="panel-heading">
												<h2 class="panel-title">REKAPITULASI DANA DEKONSENTRASI</h2>
											</div>
											<div class="panel-body">
												<div>
													<table>
														<tr>
															<td>
																Tahun&nbsp;&nbsp;
															<td>
																<select name="tahun" id="tahun_dekon" class="form-control">
																	<?php
																	$sqlTahunx = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
																	$sqlTahunx->data_seek(0);
																	echo '<option value="0">-- Pilih Tahun --</option>';
																	while ($rowTahunx = $sqlTahunx->fetch_assoc()) {
																		//$select = ($rowTahunx['tahun'] == (date("Y"))) ? "selected" : "";
																		echo '<option value="' . $rowTahunx['tahun'] . '">' . $rowTahunx['tahun'] . '</option>';
																	}
																	?>
																</select>
															</td>
															<td>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																Bulan&nbsp;&nbsp;
															</td>
															<td>
																<select name="bulan" id="bulan_dekon" class="form-control">>
																</select>
															</td>
															
														</tr>
													</table>                                                  
												</div>
												<p><hr></p>
												<div class="row-fluid">
													<div class="col-lg-7">
														<div id="outchartdiv_dekon"><div id="chartdiv_dekon"></div></div>   
														<div id="loadTp"></div>
													</div>
													<div class="col-lg-5">
														<!-- Chart code -->
														<div id="outchartdiv_pie_dekon"><div id="chartdiv_pie_dekon"></div></div>   
													</div>
												</div>
												<div class="label label-default" id="alert_dekon">
													<strong>Pemberitahuan: </strong>Untuk melihat rekapitulasi dana Dekonsentrasi silahkan terlebih dahulu pilih tahun dan bulan.
												</div>
											</div>
										</div>
										<!-- ##### END DEKON ##### -->
										
										<!-- APBN -->
                                        <div class="row-fluid panel_apbn">
                                            <div class="col-lg-12 well"> 											
                                                <div class="row-fluid">
                                                 
                                                        <?php
                                                        $id_apbn = 12;
                                                        if (isset($_GET['years']) && $_GET['years'] != "") {
                                                            $tahun = $_GET['years']; //tahun by variabel years di URL
                                                        } else {
                                                            $tahun = date("Y"); //tahun grafik aktif    
                                                        }
                                                        ?>
                                                        <div style="text-align:center;">
                                                            <h5>REKAPITULASI DANA APBN DAN APBD PROVINSI<br>KABUPATEN BOYOLALI TAHUN ANGGARAN <?php echo $tahun; ?></h5>
                                                        </div>
                                                        <br>

                                                    <div>
                                                        <select name="tahunan" id="tahunan" class="form-control" onchange="if (this.value)
                                                                    window.location.href = this.value">
                                                                    <?php
                                                                    $sqlTahun = $db->query("select * from m_tahun_anggaran where status='1' order by id desc");
                                                                    $sqlTahun->data_seek(0);
                                                                    while ($rowTahun = $sqlTahun->fetch_assoc()) {
                                                                        $select = ($rowTahun['tahun'] == $tahun) ? "selected" : "";
                                                                        echo '<option value="?years=' . $rowTahun['tahun'] . '" ' . $select . '>Laporan Tahun Anggaran ' . $rowTahun['tahun'] . '</option>';
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>                                    

                                                </div>
                                            </div>

                                            
                                            <div id="chartdiv"></div>     
                                            <p>&nbsp;</p>
                                            <div class="row-fluid">
                                                <div class="col-lg-6">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Keterangan</div>
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li>
                                                                    <img src="media/img/yellow.jpg"> &nbsp;&nbsp;Jumlah Anggaran
                                                                </li>
                                                                <li>
                                                                    <img src="media/img/blue.jpg"> &nbsp;&nbsp;Jumlah Realisasi Anggaran
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">DAK</h5>
                                                            <p class="list-group-item-text">Dana ALokasi Khusus</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">DBHCHT</h5>
                                                            <p class="list-group-item-text">Dana Bagi Hasil Cukai Hasil Tembakau</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">Bankeu Kab</h5>
                                                            <p class="list-group-item-text">Dana Bantuan Keuangan Untuk Kabupaten & Kota</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">Dekon</h5>
                                                            <p class="list-group-item-text">Dana Dekonsentrasi</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">TP</h5>
                                                            <p class="list-group-item-text">Dana Tugas Pembantuan</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">DUB</h5>
                                                            <p class="list-group-item-text">Dana Urusan Bersama</p>
                                                        </a>
                                                        <a href="#" class="list-group-item list-group-item-action">
                                                            <h5 class="list-group-item-heading">Bankeu Desa</h5>
                                                            <p class="list-group-item-text">Dana Bantuan Keuangan Untuk Desa</p>
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- END APBN -->
                                </section>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="footer_copyright_area">
                <div class="row">
                    <center><p><small>Copyright © <?php echo date("Y"); ?> <a href="#">Bappeda Kabupaten Boyolali</a> - All Rights Reseved.</small></p></center>
                </div>
            </div>
        </footer>
        <script src="media/js/loadingoverlay.min.js"></script>
        <script>
			$(document).ajaxSend(function (event, jqxhr, settings) {
				$.LoadingOverlay("show");
			});
			$(document).ajaxComplete(function (event, jqxhr, settings) {
				$.LoadingOverlay("hide");
			});
			function number_format(number, decimals, dec_point, thousands_sep) {
                // Strip all characters but numerical ones.
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                        s = '',
                        toFixedFix = function (n, prec) {
                            var k = Math.pow(10, prec);
                            return '' + Math.round(n * k) / k;
                        };
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            }
        </script>

        <script type="text/javascript">
            (function ($) {
                //awal create chart
                // ##### APBD KABUPATEN ###### //
                $('#tahunx').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/apbd/item_bulan.php';
                    $.post(urls, {tahun: $('#tahunx').val()},
                            function (data) {
                                $('#bulan').html(data);
                            }
                    );
                });
                $('#bulan').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/apbd/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv2", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/apbd/item_laporan.php?th=" + $('#tahunx').val() + "&bln=" + $('#bulan').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_apbd').hide();
                                $('#outchartdiv2').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/apbd/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv4", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/apbd/item_laporan_pie.php?th=" + $('#tahunx').val() + "&bln=" + $('#bulan').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv4').show();
                            }
                    );
                });
                
                // ##### DAK - DANA ALOKASI KHUSUS ###### //
                $('#tahun_dak').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dak/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_dak').val()},
                            function (data) {
                                $('#bulan_dak').html(data);
                            }
                    );
                });
                $('#bulan_dak').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dak/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_dak').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_dak", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/dak/item_laporan.php?th=" + $('#tahun_dak').val() + "&bln=" + $('#bulan_dak').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_dak').hide();
                                $('#outchartdiv_dak').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/dak/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_dak').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_dak", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/dak/item_laporan_pie.php?th=" + $('#tahun_dak').val() + "&bln=" + $('#bulan_dak').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_dak').show();
                            }
                    );
                });
                // ##### END DAK ##### //
                
                // ##### TP - TUGAS PEMBANTUAN ###### //
                $('#tahun_tp').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/tp/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_tp').val()},
                            function (data) {
                                $('#bulan_tp').html(data);
                            }
                    );
                });
                $('#bulan_tp').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/tp/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_tp').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_tp", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/tp/item_laporan.php?th=" + $('#tahun_tp').val() + "&bln=" + $('#bulan_tp').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_tp').hide();
                                $('#outchartdiv_tp').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/tp/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_tp').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_tp", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/tp/item_laporan_pie.php?th=" + $('#tahun_tp').val() + "&bln=" + $('#bulan_tp').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_tp').show();
                            }
                    );
                });
                // ##### END TP ##### //
                
                // ##### BANKEU KAB ###### //
                $('#tahun_bankeu_kab').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/bankeu_kab/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_bankeu_kab').val()},
                            function (data) {
                                $('#bulan_bankeu_kab').html(data);
                            }
                    );
                });
                $('#bulan_bankeu_kab').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/bankeu_kab/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_bankeu_kab').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_bankeu_kab", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/bankeu_kab/item_laporan.php?th=" + $('#tahun_bankeu_kab').val() + "&bln=" + $('#bulan_bankeu_kab').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_bankeu_kab').hide();
                                $('#outchartdiv_bankeu_kab').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/bankeu_kab/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_bankeu_kab').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_bankeu_kab", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/bankeu_kab/item_laporan_pie.php?th=" + $('#tahun_bankeu_kab').val() + "&bln=" + $('#bulan_bankeu_kab').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_bankeu_kab').show();
                            }
                    );
                });
                // ##### END BANKEU KAB ##### //
                
                 // ##### BANKEU DESA ###### //
                $('#tahun_bankeu_desa').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/bankeu_desa/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_bankeu_desa').val()},
                            function (data) {
                                $('#bulan_bankeu_desa').html(data);
                            }
                    );
                });
                $('#bulan_bankeu_desa').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/bankeu_desa/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_bankeu_desa').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_bankeu_desa", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/bankeu_desa/item_laporan.php?th=" + $('#tahun_bankeu_desa').val() + "&bln=" + $('#bulan_bankeu_desa').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_bankeu_desa').hide();
                                $('#outchartdiv_bankeu_desa').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/bankeu_desa/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_bankeu_desa').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_bankeu_desa", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/bankeu_desa/item_laporan_pie.php?th=" + $('#tahun_bankeu_desa').val() + "&bln=" + $('#bulan_bankeu_desa').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_bankeu_desa').show();
                            }
                    );
                });
                // ##### END BANKEU DESA ##### //
                
                 // ##### DBHCHT ###### //
                $('#tahun_dbhcht').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dbhcht/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_dbhcht').val()},
                            function (data) {
                                $('#bulan_dbhcht').html(data);
                            }
                    );
                });
                $('#bulan_dbhcht').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dbhcht/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_dbhcht').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_dbhcht", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/dbhcht/item_laporan.php?th=" + $('#tahun_dbhcht').val() + "&bln=" + $('#bulan_dbhcht').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_dbhcht').hide();
                                $('#outchartdiv_dbhcht').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/dbhcht/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_dbhcht').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_dbhcht", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/dbhcht/item_laporan_pie.php?th=" + $('#tahun_dbhcht').val() + "&bln=" + $('#bulan_dbhcht').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_dbhcht').show();
                            }
                    );
                });
                // ##### END DBHCHT ##### //
                
                // ##### DUB ###### //
                $('#tahun_dub').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dub/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_dub').val()},
                            function (data) {
                                $('#bulan_dub').html(data);
                            }
                    );
                });
                $('#bulan_dub').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dub/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_dub').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_dub", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/dub/item_laporan.php?th=" + $('#tahun_dub').val() + "&bln=" + $('#bulan_dub').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_dub').hide();
                                $('#outchartdiv_dub').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/dub/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_dub').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_dub", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/dub/item_laporan_pie.php?th=" + $('#tahun_dub').val() + "&bln=" + $('#bulan_dub').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_dub').show();
                            }
                    );
                });
                // ##### END DUB ##### //
                
                // ##### DEKON ###### //
                $('#tahun_dekon').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dub/item_bulan.php';
                    $.post(urls, {tahun: $('#tahun_dekon').val()},
                            function (data) {
                                $('#bulan_dekon').html(data);
                            }
                    );
                });
                $('#bulan_dekon').change(function (event) {
                    //alert($('#tahunx').val());
                    urls = 'ajax/dekon/item_laporan.php';
                    $.post(urls, {tahun: $('#bulan_dekon').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_dekon", {
                                    "theme": "light",
                                    "type": "serial",
                                    "dataLoader": {
                                        "url": "ajax/dekon/item_laporan.php?th=" + $('#tahun_dekon').val() + "&bln=" + $('#bulan_dekon').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueAxes": [{
                                            "stackType": "3d",
                                            "unit": "",
                                            "position": "left",
                                            "title": "Jumlah"
                                        }],
                                    "startDuration": 1,
                                    "graphs": [
                                        {
                                            //"balloonText": "[[category]]: <b>[[value]]</b>",
                                            "balloonText": "[[value]]",
                                            "balloonFunction": function (graphDataItem, graph) {
                                                var value = graphDataItem.values.value;
                                                return "<b>" + number_format(value, 2, ',', '.') + "</b>";
                                            },
                                            "fillAlphas": 0.9,
                                            "lineAlpha": 0.2,
                                            "fillColorsField": "color",
                                            "title": "anggaran",
                                            "type": "column",
                                            "valueField": "anggaran"
                                        }
                                    ],
                                    "chartCursor": {
                                        "pan": true,
                                        "valueLineEnabled": true,
                                        "valueLineBalloonEnabled": true,
                                        "cursorAlpha": 1,
                                        "cursorColor": "#258cbb",
                                        "valueLineAlpha": 0.2
                                    },
                                    "plotAreaFillAlphas": 0.1,
                                    "depth3D": 60,
                                    "angle": 30,
                                    "categoryField": "dana",
                                    "categoryAxis": {
                                        "gridPosition"
                                                : "start"
                                    }
                                });
                                $('#alert_dekon').hide();
                                $('#outchartdiv_dekon').show();
                            }
                    );


                    //pie chart fisik
                    urlx = 'ajax/dekon/item_laporan_pie.php';
                    $.post(urls, {tahun: $('#bulan_dekon').val()},
                            function (data) {
                                //$('#loadApbd').html(data);
                                //alert(data);
                                var chart = AmCharts.makeChart("chartdiv_pie_dekon", {
                                    "theme": "light",
                                    "type": "pie",
                                    "legend": {
                                        "position": "bottom",
                                        "marginBottom": 70,
                                        "marginLeft": 70,
                                        "autoMargins": false
                                    },
                                    "labelRadius": -35,
                                    "labelText": "[[percents]]%",
                                    //labelsEnabled: false,
                                    autoMargins: false,
                                    marginTop: 0,
                                    marginBottom: 0,
                                    marginLeft: 0,
                                    marginRight: 0,
                                    pullOutRadius: 0,
                                    "dataLoader": {
                                        "url": "ajax/dekon/item_laporan_pie.php?th=" + $('#tahun_dekon').val() + "&bln=" + $('#bulan_dekon').val(),
                                        "showErrors": true,
                                        "complete": function () {
                                            console.log("Loading complete");
                                        },
                                        "load": function (options) {
                                            console.log("File loaded: ", options.url);
                                        },
                                        "error": function (options) {
                                            console.log("Error occured loading file: ", options.url);
                                        }
                                    },
                                    "valueField": "value",
                                    "titleField": "country",
                                    "outlineAlpha": 0.4,
                                    "depth3D": 15,
                                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                                    "angle": 30,
                                    "export": {
                                        "enabled": true
                                    }
                                });
                                $('#outchartdiv_pie_dekon').show();
                            }
                    );
                });
                // ##### END DEKON ##### //
                
                //akhir dari create chart
            })(jQuery);
            
            $(document).ready(function(){
			   $('#bt_apbd').click(function(){
				  $('.panel_apbd').show();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_dbhcht').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').show();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_dak').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').show();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_tp').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').show();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_bankeu_kab').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').show();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_bankeu_desa').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').show();$('.panel_dub').hide();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_dub').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').show();
				  $('.panel_dekon').hide();
			   }); 
			   
			   $('#bt_dekon').click(function(){
				  $('.panel_apbd').hide();$('.panel_dak').hide();$('.panel_tp').hide();
				  $('.panel_dbhcht').hide();$('.panel_bankeu_kab').hide();$('.panel_bankeu_desa').hide();$('.panel_dub').hide();
				  $('.panel_dekon').show();
			   }); 
			});
        </script>
	
	<!-- Chart code -->
	<?php
		//hitung jumlah kegiatan
		$q_hitung_dak = mysqli_query($db, "select id from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_dak = $q_hitung_dak->num_rows;

		$q_hitung_dbhct = mysqli_query($db, "select id from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_dbhcht = $q_hitung_dbhct->num_rows;

		$q_hitung_bankeu_kab = mysqli_query($db, "select id from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_bankeu_kab = $q_hitung_bankeu_kab->num_rows;

		$q_hitung_bankeu_desa = mysqli_query($db, "select id from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_bankeu_desa = $q_hitung_bankeu_desa->num_rows;

		$q_hitung_dekon = mysqli_query($db, "select id from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_dekon = $q_hitung_dekon->num_rows;

		$q_hitung_tp = mysqli_query($db, "select id from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_tp = $q_hitung_tp->num_rows;

		$q_hitung_dub = mysqli_query($db, "select id from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'");
		$hitung_dub = $q_hitung_bankeu_desa->num_rows;

		$perda_1 = $hitung_dak + $hitung_dbhcht;
		$perda_2 = $hitung_bankeu_kab;

		$non_1 = $hitung_dekon + $hitung_tp + $hitung_dub;
		$non_2 = $hitung_bankeu_desa;

		$sum_perda = $perda_1 + $perda_2;
		$sum_non = $non_1 + $non_2;
		$no_target = 0;
		$no_real = 0;
		//kolom jml kegiatan beres
		//anggaran
		$anggaran_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dak) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dbhcht) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dekon) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_tp) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_dub) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$anggaran_perda_apbn = $anggaran_dak['total'] + $anggaran_dbhcht['total'];
		$anggaran_perda_apbd = $anggaran_bankeu_kab['total'];

		$anggaran_non_apbn = $anggaran_dekon['total'] + $anggaran_tp['total'] + $anggaran_dub['total'];
		$anggaran_non_apbd = $anggaran_bankeu_desa['total'];

		$sum_anggaran_perda = $anggaran_perda_apbn + $anggaran_perda_apbd;
		$sum_anggaran_non = $anggaran_non_apbn + $anggaran_non_apbd;
		//anggaran beres
		//pendamping
		$pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_tp = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(anggaran_pendamping) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$pendamping_perda_apbn = $pendamping_dak['total'] + $pendamping_dbhcht['total'];
		$pendamping_perda_apbd = $pendamping_bankeu_kab['total'];

		$pendamping_non_apbn = $pendamping_dekon['total'] + $pendamping_tp['total'] + $pendamping_dub['total'];
		$pendamping_non_apbd = $pendamping_bankeu_desa['total'];

		$sum_pendamping_perda = $pendamping_perda_apbn + $pendamping_perda_apbd;
		$sum_pendamping_non = $pendamping_non_apbn + $pendamping_non_apbd;
		//pendamping beres
		//JUMLAH
		$jumlah_dak = $anggaran_dak['total'] + $pendamping_dak['total'];
		$jumlah_dbhcht = $anggaran_dbhcht['total'] + $pendamping_dbhcht['total'];
		$jumlah_bankeu_kab = $anggaran_bankeu_kab['total'] + $pendamping_bankeu_kab['total'];
		$jumlah_bankeu_desa = $anggaran_bankeu_desa['total'] + $pendamping_bankeu_desa['total'];
		$jumlah_dekon = $anggaran_dekon['total'] + $pendamping_dekon['total'];
		$jumlah_tp = $anggaran_tp['total'] + $pendamping_tp['total'];
		$jumlah_dub = $anggaran_dub['total'] + $pendamping_dub['total'];

		$jumlah_perda_apbn = $anggaran_perda_apbn + $pendamping_perda_apbn;
		$jumlah_perda_apbd = $anggaran_perda_apbd + $pendamping_perda_apbd;
		$jumlah_non_apbn = $anggaran_non_apbn + $pendamping_non_apbn;
		$jumlah_non_apbd = $anggaran_non_apbd + $pendamping_non_apbd;
		$jumlah_sum_perda = $sum_anggaran_perda + $sum_pendamping_perda;
		$jumlah_sum_non = $sum_anggaran_non + $sum_pendamping_non;
		//JUMLAH beres
		//realisasi spj
		$realisasi_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dak) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dbhcht) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_kab) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_bankeu_desa) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dekon) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_tp) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_dub) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_perda_apbn = $realisasi_dak['total'] + $realisasi_dbhcht['total'];
		$realisasi_perda_apbd = $realisasi_bankeu_kab['total'];

		$realisasi_non_apbn = $realisasi_dekon['total'] + $realisasi_tp['total'] + $realisasi_dub['total'];
		$realisasi_non_apbd = $realisasi_bankeu_desa['total'];

		$sum_realisasi_perda = $realisasi_perda_apbn + $realisasi_perda_apbd;
		$sum_realisasi_non = $realisasi_non_apbn + $realisasi_non_apbd;
		//realisasi spj beres
		//pendamping spj
		$realisasi_pendamping_dak = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendampingg_tp = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_dub = mysqli_fetch_array(mysqli_query($db, "select sum(realisasi_pendamping) as total from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$realisasi_pendamping_perda_apbn = $realisasi_pendamping_dak['total'] + $realisasi_pendamping_dbhcht['total'];
		$realisasi_pendamping_perda_apbd = $realisasi_pendamping_bankeu_kab['total'];

		$realisasi_pendamping_non_apbn = $realisasi_pendamping_dekon['total'] + $realisasi_pendampingg_tp['total'] + $realisasi_pendamping_dub['total'];
		$realisasi_pendamping_non_apbd = $realisasi_pendamping_bankeu_desa['total'];

		$sum_realisasi_pendamping_perda = $realisasi_pendamping_perda_apbn + $realisasi_pendamping_perda_apbd;
		$sum_realisasi_pendamping_non = $realisasi_pendamping_non_apbn + $realisasi_pendamping_non_apbd;
		//pendamping spj beres
		//JUMLAH SPJ
		$jumlah_realisasi_dak = $realisasi_dak['total'] + $realisasi_pendamping_dak['total'];
		$jumlah_realisasi_dbhcht = $realisasi_dbhcht['total'] + $realisasi_pendamping_dbhcht['total'];
		$jumlah_realisasi_bankeu_kab = $realisasi_bankeu_kab['total'] + $realisasi_pendamping_bankeu_kab['total'];
		$jumlah_realisasi_bankeu_desa = $realisasi_bankeu_desa['total'] + $realisasi_pendamping_bankeu_desa['total'];
		$jumlah_realisasi_dekon = $realisasi_dekon['total'] + $realisasi_pendamping_dekon['total'];
		$jumlah_realisasi_tp = $realisasi_tp['total'] + $realisasi_pendampingg_tp['total'];
		$jumlah_realisasi_dub = $realisasi_dub['total'] + $realisasi_pendamping_dub['total'];

		$jumlah_realisasi_perda_apbn = $realisasi_perda_apbn + $realisasi_pendamping_perda_apbn;
		$jumlah_realisasi_perda_apbd = $realisasi_perda_apbd + $realisasi_pendamping_perda_apbd;
		$jumlah_realisasi_non_apbn = $realisasi_non_apbn + $realisasi_pendamping_non_apbn;
		$jumlah_realisasi_non_apbd = $realisasi_non_apbd + $realisasi_pendamping_non_apbd;
		$jumlah_realisasi_sum_perda = $sum_realisasi_perda + $sum_realisasi_pendamping_perda;
		$jumlah_realisasi_sum_non = $sum_realisasi_non + $sum_realisasi_pendamping_non;
		//JUMLAH spj beres
		//Persen SPJ
		$persen_dak = ($jumlah_realisasi_dak != 0) ? $jumlah_realisasi_dak * 100 / $jumlah_dak : 0;
		$persen_dbhcht = ($jumlah_realisasi_dbhcht != 0) ? $jumlah_realisasi_dbhcht * 100 / $jumlah_dbhcht : 0;
		$persen_bankeu_kab = ($jumlah_realisasi_bankeu_kab != 0) ? $jumlah_realisasi_bankeu_kab * 100 / $jumlah_bankeu_kab : 0;
		$persen_bankeu_desa = ($jumlah_realisasi_bankeu_desa != 0) ? $jumlah_realisasi_bankeu_desa * 100 / $jumlah_bankeu_desa : 0;
		$persen_dekon = ($jumlah_realisasi_dekon != 0) ? $jumlah_realisasi_dekon * 100 / $jumlah_dekon : 0;
		$persen_tp = ($jumlah_realisasi_tp != 0) ? $jumlah_realisasi_tp * 100 / $jumlah_tp : 0;
		$persen_dub = ($jumlah_realisasi_dub != 0) ? $jumlah_realisasi_dub * 100 / $jumlah_dub : 0;

		$persen_perda_apbn = ($jumlah_realisasi_perda_apbn != 0) ? $jumlah_realisasi_perda_apbn * 100 / $jumlah_perda_apbn : 0;
		$persen_perda_apbd = ($jumlah_realisasi_perda_apbd != 0) ? $jumlah_realisasi_perda_apbd * 100 / $jumlah_perda_apbd : 0;
		$persen_non_apbn = ($jumlah_realisasi_non_apbn != 0) ? $jumlah_realisasi_non_apbn * 100 / $jumlah_non_apbn : 0;
		$persen_non_apbd = ($jumlah_realisasi_non_apbd != 0) ? $jumlah_realisasi_non_apbd * 100 / $jumlah_non_apbd : 0;
		$persen_sum_perda = ($jumlah_realisasi_sum_perda != 0) ? $jumlah_realisasi_sum_perda * 100 / $jumlah_sum_perda : 0;
		$persen_sum_non = ($jumlah_realisasi_sum_non != 0) ? $jumlah_realisasi_sum_non * 100 / $jumlah_sum_non : 0;
		//Persen spj beres
		//target & realisasi

		$target_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_dak = ($target_dak['target'] != 0 && $hitung_dak != 0) ? $target_dak['target'] / $hitung_dak : 0;
		$target_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_dbhcht = ($target_dbhcht['target'] != 0 && $hitung_dbhcht != 0) ? $target_dbhcht['target'] / $hitung_dbhcht : 0;
		$target_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_bankeu_kab = ($target_bankeu_kab['target'] != 0 && $hitung_bankeu_kab != 0) ? $target_bankeu_kab['target'] / $hitung_bankeu_kab : 0;
		$target_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_bankeu_desa = ($target_bankeu_desa['target'] != 0 && $hitung_bankeu_desa != 0) ? $target_bankeu_desa['target'] / $hitung_bankeu_desa : 0;
		$target_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_dub = ($target_dub['target'] != 0 && $hitung_dub != 0) ? $target_dub['target'] / $hitung_dub : 0;
		$target_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_tp = ($target_tp['target'] != 0 && $hitung_tp != 0) ? $target_tp['target'] / $hitung_tp : 0;
		$target_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_target) target from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$target_dekon = ($target_dekon['target'] != 0 && $hitung_dekon != 0) ? $target_dekon['target'] / $hitung_dekon : 0;

		//realisasi
		$real_dak = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dak where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_dak = ($real_dak['reali'] != 0) ? $real_dak['reali'] / $hitung_dak : 0;
		$real_dbhcht = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dbhcht where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_dbhcht = ($real_dbhcht['reali'] != 0) ? $real_dbhcht['reali'] / $hitung_dbhcht : 0;
		$real_bankeu_kab = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_kab where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_bankeu_kab = ($real_bankeu_kab['reali'] != 0) ? $real_bankeu_kab['reali'] / $hitung_bankeu_kab : 0;
		$real_bankeu_desa = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_bankeu_desa where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_bankeu_desa = ($real_bankeu_desa['reali'] != 0) ? $real_bankeu_desa['reali'] / $hitung_bankeu_desa : 0;

		$real_dub = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dub where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_dub = ($real_dub['reali'] != 0 && $hitung_dub != 0) ? $real_dub['reali'] / $hitung_dub : 0;
		$real_tp = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_tp where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_tp = ($real_tp['reali'] != 0 && $hitung_tp != 0) ? $real_tp['reali'] / $hitung_tp : 0;
		$real_dekon = mysqli_fetch_array(mysqli_query($db, "select sum(progres_real) reali from t_dekon where bulan='" . $id_apbn . "' and tahun='" . $tahun . "' and status_update='0'"));
		$real_dekon = ($real_dekon['reali'] != 0 && $hitung_dekon != 0) ? $real_dekon['reali'] / $hitung_dekon : 0;
		?>
	<script>
		AmCharts.makeChart("chartdiv", {
			"theme": "light",
			"type": "serial",
			"dataProvider": [
				{
					"dana": "DAK",
					"anggaran": <?php echo $jumlah_dak; ?>,
					"spj": <?php echo $jumlah_realisasi_dak; ?>
				},
				{
					"dana": "DBHCHT",
					"anggaran": <?php echo $jumlah_dbhcht; ?>,
					"spj": <?php echo $jumlah_realisasi_dbhcht; ?>
				},
				{
					"dana": "Bankeu Kab",
					"anggaran": <?php echo $jumlah_bankeu_kab; ?>,
					"spj": <?php echo $jumlah_realisasi_bankeu_kab; ?>
				},
				{
					"dana": "Dekon",
					"anggaran": <?php echo $jumlah_dekon; ?>,
					"spj": <?php echo $jumlah_realisasi_dekon; ?>
				},
				{
					"dana": "TP",
					"anggaran": <?php echo $jumlah_tp; ?>,
					"spj": <?php echo $jumlah_realisasi_tp; ?>
				},
				{
					"dana": "DUB",
					"anggaran": <?php echo $jumlah_dub; ?>,
					"spj": <?php echo $jumlah_realisasi_dub; ?>
				},
				{
					"dana": "Bankeu Desa",
					"anggaran": <?php echo $jumlah_bankeu_desa; ?>,
					"spj": <?php echo $jumlah_realisasi_bankeu_desa; ?>
				}
			],
			"valueAxes": [{
					"stackType": "3d",
					"unit": "",
					"position": "left",
					"title": "Jumlah",
				}],
			"startDuration": 1,
			"graphs": [
				{
					//"balloonText": "Realisasi Anggaran [[category]]: <b>[[value]]</b>",
					"balloonText": "SPJ [[category]]: [[value]]",
					"balloonFunction": function (graphDataItem, graph) {
						var value2 = graphDataItem.values.value;
						return "SPJ: <b>Rp " + number_format(value2, 2, ',', '.') + "</b>";
					},
					"fillAlphas": 0.9,
					"lineAlpha": 0.2,
					"title": "spj",
					"type": "column",
					"valueField": "spj"
				},
				{
					//"balloonText": "Anggaran [[category]]: <b>[[value]]</b>",
					"balloonText": "Anggaran [[category]]: [[value]]",
					"balloonFunction": function (graphDataItem, graph) {
						var value3 = graphDataItem.values.value;
						return "Anggaran: <b>Rp " + number_format(value3, 2, ',', '.') + "</b>";
					},
					"fillAlphas": 0.9,
					"lineAlpha": 0.2,
					"title": "anggaran",
					"type": "column",
					"valueField": "anggaran"
				}
			],
			"chartCursor": {
				"pan": true,
				"valueLineEnabled": true,
				"valueLineBalloonEnabled": true,
				"cursorAlpha": 1,
				"cursorColor": "#258cbb",
				"valueLineAlpha": 0.2
			},
			"plotAreaFillAlphas": 0.1,
			"depth3D": 60,
			"angle": 30,
			"categoryField": "dana",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"export": {
				"enabled": true
			}
		});
	</script>
    </body>
</html>

